#include <higgsOffshellDecayCombinations/RooDensityRatio.h>
#include <higgsOffshellDecayCombinations/RooTreeDataStoreStar.h>
#include <higgsOffshellDecayCombinations/RooDataSetStar.h>
#include <higgsOffshellDecayCombinations/RooNLLVarStar.h>

DECLARE_COMPONENT(RooDensityRatio)
DECLARE_COMPONENT(RooTreeDataStoreStar)
DECLARE_COMPONENT(RooDataSetStar)
DECLARE_COMPONENT(RooNLLVarStar)