#include "higgsOffshellDecayCombinations/RooDataSetStar.h"

ClassImp(RooDataSetStar)

RooDataSetStar::RooDataSetStar(RooStringView name, RooStringView title, const RooArgSet& vars, std::unique_ptr<RooTreeDataStoreStar> store, const char* wgtVarName):
    RooDataSet(name, title, vars){
    TRACE_CREATE;

    // Assign the weight variable
    _wgtVar = (RooRealVar*)vars.find(wgtVarName);
    _vars.remove(*(vars.find(wgtVarName)));

    // Point to the tree from the RooTreeDataStore
    _dstore = std::move(store);
    
}

RooDataSetStar::~RooDataSetStar(){
    TRACE_DESTROY;
}

const RooArgSet* RooDataSetStar::get(Int_t index) const {
    // _dstore->CheckInit();
    // std::cout << "RooDataSetStar.cxx - get() - index: " << index << std::endl;
    const RooArgSet* test = _dstore->get(index);
    // std::cout << "RooDataSetStar.cxx - get() - value of row " << index << " of var_ATLAS_LUMI_ratio_EWSBI10_up: " << ((RooRealVar*)test->find("var_ATLAS_LUMI_ratio_EWSBI10_up"))->getVal() << std::endl;
    return test; //_dstore->get(index);
}