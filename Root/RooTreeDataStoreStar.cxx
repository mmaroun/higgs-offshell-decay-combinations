#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"

ClassImp(RooTreeDataStoreStar)

// Constructor with a filename to the original tree
RooTreeDataStoreStar::RooTreeDataStoreStar(const char* name, const char* title, TTree* tree, const RooArgSet& vars, const char* weightVarName)
    : RooTreeDataStore(tree, vars, weightVarName) {

    if (!m_tree){
        m_tree = new TTree(name, title);
        m_tree = tree;
    }

}

RooTreeDataStoreStar::RooTreeDataStoreStar(const RooTreeDataStoreStar& other, const char* newName)
    : RooTreeDataStore(other, newName){

}

// Standard Destructor
RooTreeDataStoreStar::~RooTreeDataStoreStar(){

}
