#include "higgsOffshellDecayCombinations/HandleSystematics.h"

// Constructor with Setting NPs and GOs from systematics list
HandleSystematics::HandleSystematics(std::vector<std::string> sysList){

    this->SetSystematicsList(sysList);

}

// Standard Destructor
HandleSystematics::~HandleSystematics(){

}

// Add the list of systematics list separately
void HandleSystematics::SetSystematicsList(std::vector<std::string> sysList){

     for (auto sys : sysList){
        m_nSys++;
        std::string npTitle = "alpha_"+sys;
        std::string goTitle = "nom_"+sys;
        std::string gaussTitle = "gauss_"+sys;
        RooRealVar* theNP = new RooRealVar(npTitle.c_str(), npTitle.c_str(), 0.0, -5., 5.);
        m_nps.add(*theNP);
        RooRealVar* theGO = new RooRealVar(goTitle.c_str(), goTitle.c_str(), 0.0, -INF, INF);
        theGO->setConstant(true);
        m_gos.add(*theGO);
        m_gaussians.add(*(new RooGaussian(gaussTitle.c_str(), gaussTitle.c_str(), *theGO, *theNP, RooRealConstant::value(1.0))));
    }
}

// Conpute and return the constraint PDF for the system
RooProdPdf* HandleSystematics::ComputeConstraintPdf(const char* name, const char* title){

    RooProdPdf* output = new RooProdPdf(name, title, m_gaussians, 0.0);

    return output;
}