#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"

TextToPoissonParams::TextToPoissonParams(const char* crFileName, const char* treeFileName, RooArgList nps, std::vector<std::string> processList,
    bool runOnData)
    :m_crFileName(crFileName), m_varTreeFileName(treeFileName), m_nps(nps), m_processList(processList), m_runOnData(runOnData){

    if (runOnData) {
        this->readCRNobsFromTree();
    }
    this->readVariationsTree();
    this->setInterpCodeVector();
}

void TextToPoissonParams::readCRNobsFromTree(){

    TFile* myRootFile(TFile::Open(m_crFileName.c_str()));
    if ((!myRootFile) || myRootFile->IsZombie()){
        delete myRootFile;
        throw std::runtime_error("ROOT File not found or is a zombie, exiting.");
    }

    TTree* nominal_CR = myRootFile->Get<TTree>("nominal_sys_var_CR");
    if (!nominal_CR){
        throw std::runtime_error("TTree with name \"nominal_sys_var_CR\" not found, exiting.");
    }

    const TObjArray* listOfBranches = nominal_CR->GetListOfBranches();
    TBranch* yieldBranch = (TBranch*)listOfBranches->operator()("yield_observed");
    if (!yieldBranch){
        throw std::runtime_error("Branch with name \"yield_observed\" not found, exiting.");
    }

    Long64_t numCRs = nominal_CR->GetEntries();
    Double_t branchVal;
    yieldBranch->SetAddress(&branchVal);
    for (auto i=0ll; i<numCRs; i++){
        yieldBranch->GetEntry(i);
        m_NobsCRs.push_back(branchVal);
    }
}

void TextToPoissonParams::readTextFile(){

    std::ifstream crFile(m_crFileName, std::ifstream::in);
    if (crFile.fail() || crFile.bad()){
        throw std::runtime_error("Unable to open file, exiting.");
    }

    std::string line;
    std::map<std::string, Double_t> sectionDict;
    std::string sectionName;
    std::string keyFirstStr, keyName;
    std::string arrayData;
    std::string firstNumberStr, secondNumberStr, thirdNumberStr;
    std::string secondAndThirdNumberStr;
    char keyDelim = '\'';
    char arrayStart = '[';
    char arrayEnd = ']';
    char arrayDelim = ',';

    while (std::getline(crFile, line)){
        boost::algorithm::trim(line);
        if (line.empty()) continue;
        if (line[0]=='{'){
            sectionName = std::string(line.begin()+1, line.end());
        } else {
            sectionName = line;
        }
        std::size_t keyDelimPos = line.find(keyDelim);
        if (keyDelimPos != std::string::npos){
            keyFirstStr = line.substr(keyDelimPos+1, line.size()-keyDelimPos);
            std::size_t keyEndDelimPos = keyFirstStr.find(keyDelim);
            if (keyEndDelimPos != std::string::npos){
                keyName =  std::string(keyFirstStr.begin(), keyFirstStr.begin()+keyEndDelimPos);
            }
        }
        std::size_t arrayStartPos = line.find(arrayStart);
        std::size_t arrayEndPos = line.find(arrayEnd);
        if (arrayStartPos != std::string::npos && arrayEndPos != std::string::npos){
            arrayData = std::string(line.begin()+arrayStartPos+1, line.begin()+arrayEndPos);
            std::size_t firstNumberStrPos = arrayData.find(arrayDelim);
            if (firstNumberStrPos != std::string::npos){
                firstNumberStr = arrayData.substr(0, firstNumberStrPos);
            }
            secondAndThirdNumberStr = arrayData.substr(firstNumberStrPos+1, arrayData.size()-firstNumberStrPos);
            std::size_t secondNumberStrPos = secondAndThirdNumberStr.find(arrayDelim);
            if (secondNumberStrPos != std::string::npos){
                secondNumberStr = secondAndThirdNumberStr.substr(0, secondNumberStrPos);
                thirdNumberStr = secondAndThirdNumberStr.substr(secondNumberStrPos+1, secondAndThirdNumberStr.size()-secondNumberStrPos);
            }
            Double_t firstNumber = std::stod(firstNumberStr.c_str(), nullptr);
            Double_t secondNumber = std::stod(secondNumberStr.c_str(), nullptr);
            Double_t thirdNumber = std::stod(thirdNumberStr.c_str(), nullptr);
            sectionDict["n_jets_CR_bin0"] = firstNumber;
            sectionDict["n_jets_CR_bin1"] = secondNumber;
            sectionDict["n_jets_CR_bin2"] = thirdNumber;
        }
        m_crDict[keyName] = sectionDict;
    }

    crFile.close();
}

void TextToPoissonParams::readVariationsTree(){

    TFile* myRootFile(TFile::Open(m_varTreeFileName.c_str()));
    if ((!myRootFile) || myRootFile->IsZombie()){
        delete myRootFile;
        throw std::runtime_error("ROOT File not found or is a zombie, exiting.");
    }

    TTree* eventYieldTree = myRootFile->Get<TTree>("nominal_sys_var_CR");
    if (!eventYieldTree){
        throw std::runtime_error("TTree with name \"nominal_sys_var_CR\" not found, exiting.");
    }

    const TObjArray* listOfBranches = eventYieldTree->GetListOfBranches();
    Long64_t numCRs = eventYieldTree->GetEntries();
    Double_t branchValUp;
    Double_t branchValDown;
    Double_t branchValNominal;
    for (auto processName : m_processList){
        std::string branchNameNominal = "yield_CR_"+processName;
        TBranch* bNom = (TBranch*)listOfBranches->operator()(branchNameNominal.c_str());
        if (!bNom){
            throw std::runtime_error("Branch with name "+branchNameNominal+" not found, exiting.");
        }
        bNom->SetAddress(&branchValNominal);
        for (auto np: m_nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string branchNameUp = "var_"+sysName+"_CRbins_"+processName+"_up";
            std::string branchNameDown = "var_"+sysName+"_CRbins_"+processName+"_down";
            TBranch* bUp = (TBranch*)listOfBranches->operator()(branchNameUp.c_str());
            if (!bUp){
                throw std::runtime_error("Branch with name "+branchNameUp+" not found, exiting.");
            }
            TBranch* bDown = (TBranch*)listOfBranches->operator()(branchNameDown.c_str());
            if (!bDown){
                throw std::runtime_error("Branch with name "+branchNameDown+" not found, exiting.");
            }
            bUp->SetAddress(&branchValUp);
            bDown->SetAddress(&branchValDown);
            for (auto i = 0ll; i<numCRs; i++){
                std::string nominalCRbin = "n_jets_CR_bin"+std::to_string(i);
                bUp->GetEntry(i);
                bDown->GetEntry(i);
                bNom->GetEntry(i);
                m_eventYieldsInfo["up"][processName][i].push_back(branchValUp);
                m_eventYieldsInfo["down"][processName][i].push_back(branchValDown);
                m_crDict[processName][nominalCRbin] = branchValNominal;
            }
        }
    }

}

void TextToPoissonParams::setInterpCode(int code){
    m_code = code;
    this->setInterpCodeVector();
}

void TextToPoissonParams::setInterpCodeVector(){

    for (int i=0; i<m_nps.getSize(); i++){
        m_interpCodes.push_back(m_code);
    }
}

TextToPoissonParams::~TextToPoissonParams(){

}

void TextToPoissonParams::printCRDict(){

    std::cout << "Elements of m_crDict: " << std::endl;
    for (const auto& [key, value] : m_crDict){
        for (const auto& [valKey, valValue] : value){
            std::cout << "m_crDict[" << key << "][" << valKey << "] = " << valValue << std::endl;
        }
    }

}

void TextToPoissonParams::printNobsCRs(){

    std::cout << "The Observed Yields of the Control Regions: " << std::endl;
    for (auto Nobs : m_NobsCRs){
        std::cout << Nobs << std::endl;
    }
}

void TextToPoissonParams::printVarYieldsDict(){

    std::cout << "Elements of Yields Var Dict: " << std::endl;
    for (const auto& [key, value] :  m_eventYieldsInfo){
        for (const auto& [valKey, valValue] : value){
            for (const auto& [valValueKey, valValueValue] : valValue){
                std::cout << "m_eventYieldsInfo[" << key << "][" << valKey << "][Region " << valValueKey <<"][";
                for (auto vecVal : valValueValue){
                    std::cout << vecVal << " ";
                }
                std::cout << "]" << std::endl;
            }
        }
    }
}

std::map<std::string, RooAddPdf*> TextToPoissonParams::generateCRpdfs(RooArgList multipliers, RooRealVar* ATLAS_LUMI, RooRealVar* weightVar){

    std::map<std::string, RooAddPdf*> outputMap;
    Double_t qqZZValue;
    RooRealVar* qqZZ_formula;
    // assert(m_crDict["S"].size() == 3);
    for (int i=0; i<3; i++){

        // Set Names for Map and Values
        std::string dictKey = "CR"+std::to_string(i);
        std::string nJet = "n_jets_CR_bin"+std::to_string(i);
        std::string pdfNameTitle = "pdf_CR"+std::to_string(i);

        // Set the Observable Name
        std::string obsName = "obs_CR"+std::to_string(i);

        // Set the Dataset Name
        std::string datasetName = "data_CR"+std::to_string(i);
        std::string weightVarName = "N_Obs_CR"+std::to_string(i);

        // Set Names of the FlexibleInterpVars per process
        std::string sFactName       = "interpFactor_CR"+std::to_string(i)+"_S";
        std::string sbiFactName     = "interpFactor_CR"+std::to_string(i)+"_SBI";
        std::string bFactName       = "interpFactor_CR"+std::to_string(i)+"_B";
        std::string ewbFactName     = "interpFactor_CR"+std::to_string(i)+"_EWB";
        std::string ewsbiFactName   = "interpFactor_CR"+std::to_string(i)+"_EWSBI";
        std::string ewsbi10FactName = "interpFactor_CR"+std::to_string(i)+"_EWSBI10";
        std::string qqZZFactName    = "interpFactor_CR"+std::to_string(i)+"_qqZZ";
        std::string ttVFactName     = "interpFactor_CR"+std::to_string(i)+"_ttV";

        // Set the names of the nominal yields
        std::string nSName       = "n_CR"+std::to_string(i)+"_S";
        std::string nSBIName     = "n_CR"+std::to_string(i)+"_SBI";
        std::string nBName       = "n_CR"+std::to_string(i)+"_B";
        std::string nEWBName     = "n_CR"+std::to_string(i)+"_EWB";
        std::string nEWSBIName   = "n_CR"+std::to_string(i)+"_EWSBI";
        std::string nEWSBI10Name = "n_CR"+std::to_string(i)+"_EWSBI10";
        std::string nqqZZName    = "n_CR"+std::to_string(i)+"_qqZZ";
        std::string nttVName     = "n_CR"+std::to_string(i)+"_ttV";

        // Set the names of the yields
        std::string nuSName       = "nu_CR"+std::to_string(i)+"_S";
        std::string nuSBIName     = "nu_CR"+std::to_string(i)+"_SBI";
        std::string nuBName       = "nu_CR"+std::to_string(i)+"_B";
        std::string nuEWBName     = "nu_CR"+std::to_string(i)+"_EWB";
        std::string nuEWSBIName   = "nu_CR"+std::to_string(i)+"_EWSBI";
        std::string nuEWSBI10Name = "nu_CR"+std::to_string(i)+"_EWSBI10";
        std::string nuqqZZName    = "nu_CR"+std::to_string(i)+"_qqZZ";
        std::string nuttVName     = "nu_CR"+std::to_string(i)+"_ttV";

        // Set the names of the per-process pdfs
        std::string sPdfName       = "pdf_S_CR"+std::to_string(i);
        std::string sbiPdfName     = "pdf_SBI_CR"+std::to_string(i);
        std::string bPdfName       = "pdf_B_CR"+std::to_string(i);
        std::string ewbPdfName     = "pdf_EWB_CR"+std::to_string(i);
        std::string ewsbiPdfName   = "pdf_EWSBI_CR"+std::to_string(i);
        std::string ewsbi10PdfName = "pdf_EWSBI10_CR"+std::to_string(i);
        std::string qqzzPdfName    = "pdf_qqZZ_CR"+std::to_string(i);
        std::string ttvPdfName     = "pdf_ttV_CR"+std::to_string(i);

        // Calculate Expected Events for Each Process, put into Varaibles
        if (i==0){
            qqZZValue = m_crDict.at("qqZZ_0").at(nJet);
            qqZZ_formula = (RooRealVar*)multipliers.find("f_qqZZ_0");
        }
        else if (i==1) {
            qqZZValue = m_crDict.at("qqZZ_1").at(nJet);
            qqZZ_formula = (RooRealVar*)multipliers.find("f_qqZZ_1");
        }
        else {
            qqZZValue = m_crDict.at("qqZZ_2").at(nJet);
            qqZZ_formula = (RooRealVar*)multipliers.find("f_qqZZ_2");
        }
        RooFormulaVar* sYield       = new RooFormulaVar(nSName.c_str(),       "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_s")),       RooRealConstant::value(m_crDict.at("S").at(nJet))));
        RooFormulaVar* sbiYield     = new RooFormulaVar(nSBIName.c_str(),     "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_sbi")),     RooRealConstant::value(m_crDict.at("SBI").at(nJet))));
        RooFormulaVar* bYield       = new RooFormulaVar(nBName.c_str(),       "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_b")),       RooRealConstant::value(m_crDict.at("B").at(nJet))));
        RooFormulaVar* ewbYield     = new RooFormulaVar(nEWBName.c_str(),     "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_ewb")),     RooRealConstant::value(m_crDict.at("EWB").at(nJet))));
        RooFormulaVar* ewsbiYield   = new RooFormulaVar(nEWSBIName.c_str(),   "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_ewsbi")),   RooRealConstant::value(m_crDict.at("EWSBI").at(nJet))));
        RooFormulaVar* ewsbi10Yield = new RooFormulaVar(nEWSBI10Name.c_str(), "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_ewsbi10")), RooRealConstant::value(m_crDict.at("EWSBI10").at(nJet))));
        RooFormulaVar* ttVYield     = new RooFormulaVar(nttVName.c_str(),     "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find("f_ttV")),     RooRealConstant::value(m_crDict.at("ttV").at(nJet))));
        RooFormulaVar* qqZZYield    = new RooFormulaVar(nqqZZName.c_str(),    "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI,  *qqZZ_formula,                                RooRealConstant::value(qqZZValue)));

        // Generate FlexibleInterpVars from the NP list and variations up and down for each np
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_S = new RooStats::HistFactory::FlexibleInterpVar(sFactName.c_str(), sFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("S").at(i), m_eventYieldsInfo.at("up").at("S").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_SBI = new RooStats::HistFactory::FlexibleInterpVar(sbiFactName.c_str(), sbiFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("SBI").at(i), m_eventYieldsInfo.at("up").at("SBI").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_B = new RooStats::HistFactory::FlexibleInterpVar(bFactName.c_str(), bFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("B").at(i), m_eventYieldsInfo.at("up").at("B").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_EWB = new RooStats::HistFactory::FlexibleInterpVar(ewbFactName.c_str(), ewbFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("EWB").at(i), m_eventYieldsInfo.at("up").at("EWB").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_EWSBI = new RooStats::HistFactory::FlexibleInterpVar(ewsbiFactName.c_str(), ewsbiFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("EWSBI").at(i), m_eventYieldsInfo.at("up").at("EWSBI").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_EWSBI10 = new RooStats::HistFactory::FlexibleInterpVar(ewsbi10FactName.c_str(), ewsbi10FactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("EWSBI10").at(i), m_eventYieldsInfo.at("up").at("EWSBI10").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_ttV = new RooStats::HistFactory::FlexibleInterpVar(ttVFactName.c_str(), ttVFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("ttV").at(i), m_eventYieldsInfo.at("up").at("ttV").at(i),
                                    m_interpCodes);
        RooStats::HistFactory::FlexibleInterpVar* factor_CR_qqZZ = new RooStats::HistFactory::FlexibleInterpVar(qqZZFactName.c_str(), qqZZFactName.c_str(),
                                    m_nps, 1.0, m_eventYieldsInfo.at("down").at("qqZZ_"+std::to_string(i)).at(i), m_eventYieldsInfo.at("up").at("qqZZ_"+std::to_string(i)).at(i),
                                    m_interpCodes);

        // Create Variables from the expected events per process and their FlexibleInterpVars
        RooProduct* nu_CR_S       = new RooProduct(nuSName.c_str(),       nuSName.c_str(),       RooArgList(*sYield,       *factor_CR_S));
        RooProduct* nu_CR_SBI     = new RooProduct(nuSBIName.c_str(),     nuSBIName.c_str(),     RooArgList(*sbiYield,     *factor_CR_SBI));
        RooProduct* nu_CR_B       = new RooProduct(nuBName.c_str(),       nuBName.c_str(),       RooArgList(*bYield,       *factor_CR_B));
        RooProduct* nu_CR_EWB     = new RooProduct(nuEWBName.c_str(),     nuEWBName.c_str(),     RooArgList(*ewbYield,     *factor_CR_EWB));
        RooProduct* nu_CR_EWSBI   = new RooProduct(nuEWSBIName.c_str(),   nuEWSBIName.c_str(),   RooArgList(*ewsbiYield,   *factor_CR_EWSBI));
        RooProduct* nu_CR_EWSBI10 = new RooProduct(nuEWSBI10Name.c_str(), nuEWSBI10Name.c_str(), RooArgList(*ewsbi10Yield, *factor_CR_EWSBI10));
        RooProduct* nu_CR_qqZZ    = new RooProduct(nuqqZZName.c_str(),    nuqqZZName.c_str(),    RooArgList(*qqZZYield,    *factor_CR_qqZZ));
        RooProduct* nu_CR_ttV     = new RooProduct(nuttVName.c_str(),     nuttVName.c_str(),     RooArgList(*ttVYield,     *factor_CR_ttV));

        // Create RooUniforms for Each Process
        RooRealVar* obs_CR = new RooRealVar(obsName.c_str(), obsName.c_str(), 0.0, 1.0);
        m_crObservables[dictKey] = obs_CR;

        RooUniform* pdfUnifS       = new RooUniform( sPdfName.c_str(),       sPdfName.c_str(),       RooArgSet(*obs_CR));
        RooUniform* pdfUnifSBI     = new RooUniform( sbiPdfName.c_str(),     sbiPdfName.c_str(),     RooArgSet(*obs_CR));
        RooUniform* pdfUnifB       = new RooUniform( bPdfName.c_str(),       bPdfName.c_str(),       RooArgSet(*obs_CR));
        RooUniform* pdfUnifEWB     = new RooUniform( ewbPdfName.c_str(),     ewbPdfName.c_str(),     RooArgSet(*obs_CR));
        RooUniform* pdfUnifEWSBI   = new RooUniform( ewsbiPdfName.c_str(),   ewsbiPdfName.c_str(),   RooArgSet(*obs_CR));
        RooUniform* pdfUnifEWSBI10 = new RooUniform( ewsbi10PdfName.c_str(), ewsbi10PdfName.c_str(), RooArgSet(*obs_CR));
        RooUniform* pdfUnifqqZZ    = new RooUniform( qqzzPdfName.c_str(),    qqzzPdfName.c_str(),    RooArgSet(*obs_CR));
        RooUniform* pdfUnifttV     = new RooUniform( ttvPdfName.c_str(),     ttvPdfName.c_str(),     RooArgSet(*obs_CR));

        // Create the RooAddPdf and Store in the Output Map
        RooAddPdf* theAddPdf = new RooAddPdf(pdfNameTitle.c_str(), pdfNameTitle.c_str(), 
                                            RooArgList(*pdfUnifS, *pdfUnifSBI, *pdfUnifB, *pdfUnifEWB, *pdfUnifEWSBI, *pdfUnifEWSBI10, *pdfUnifqqZZ, *pdfUnifttV),
                                            RooArgList(*nu_CR_S,  *nu_CR_SBI,  *nu_CR_B,  *nu_CR_EWB,  *nu_CR_EWSBI,  *nu_CR_EWSBI10,  *nu_CR_qqZZ,  *nu_CR_ttV),
                                            false);
        outputMap[dictKey] = theAddPdf;

        // Add the Pdf info to the CR Dataset
        RooArgSet* argSetCR = new RooArgSet(*obs_CR);
        double weightValue;
        if (m_runOnData){
            weightValue = m_NobsCRs[i];
        } else {
            weightValue = theAddPdf->expectedEvents(argSetCR);
        }
        weightVar->setVal(weightValue);
        RooDataSet* theDataSet = new RooDataSet(datasetName.c_str(), datasetName.c_str(), RooArgSet(*obs_CR, *weightVar), RooFit::WeightVar(*weightVar));
        theDataSet->add(RooArgSet(*obs_CR, *weightVar), weightValue, 0.0);
        m_crDatasets[dictKey] = theDataSet;
    }

    return outputMap; 
}
