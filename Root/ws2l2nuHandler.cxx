#include "higgsOffshellDecayCombinations/ws2l2nuHandler.h"

Ws2l2nuHandler::Ws2l2nuHandler(const char* fileName2l2nu, RooArgList nps, std::vector<std::string> processList, 
    std::vector<std::string> binNames) : m_inputFileName(fileName2l2nu), m_nps(nps), m_processList(processList),
        m_binNames(binNames){

    this->readTree();
    this->setInterpCodeVector();

}

void Ws2l2nuHandler::readTree(){

    TFile* myRootFile(TFile::Open(m_inputFileName.c_str()));
    if ((!myRootFile) || myRootFile->IsZombie()){
        delete myRootFile;
        throw std::runtime_error("ROOT File not found or is a zombie, exiting.");
    }

    TTree* eventYieldTree = myRootFile->Get<TTree>("nominal_sys_var_2l2nu");
    if (!eventYieldTree){
        throw std::runtime_error("TTree with name \"nominal_sys_var_2l2nu\" not found, exiting.");
    }
    m_inputTreeName = eventYieldTree->GetName();

    const TObjArray* listOfBranches = eventYieldTree->GetListOfBranches();
    Long64_t numCRs = eventYieldTree->GetEntries();
    Double_t branchValUp;
    Double_t branchValDown;
    Double_t branchValNominal;
    Double_t branchValObs;
    TBranch* bObs = (TBranch*)listOfBranches->operator()("yield_observed_REALDATA");
    if (!bObs){
        throw std::runtime_error("Branch with name \"yield_observed_REALDATA\" not found, exiting.");
    }
    bObs->SetAddress(&branchValObs);
    for (auto processName : m_processList){
        std::string branchNameNominal = "yield_"+processName;
        TBranch* bNom = (TBranch*)listOfBranches->operator()(branchNameNominal.c_str());
        if (!bNom){
            throw std::runtime_error("Branch with name \""+branchNameNominal+"\" not found, exiting.");
        }
        bNom->SetAddress(&branchValNominal);
        for (auto np: m_nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string branchNameUp = "var_up_"+sysName+"_"+processName;
            std::string branchNameDown = "var_down_"+sysName+"_"+processName;
            TBranch* bUp = (TBranch*)listOfBranches->operator()(branchNameUp.c_str());
            if (!bUp){
                throw std::runtime_error("Branch with name \""+branchNameUp+"\" not found, exiting.");
            }
            TBranch* bDown = (TBranch*)listOfBranches->operator()(branchNameDown.c_str());
            if (!bDown){
                throw std::runtime_error("Branch with name \""+branchNameDown+"\" not found, exiting.");
            }
            bUp->SetAddress(&branchValUp);
            bDown->SetAddress(&branchValDown);
            for (auto i = 0ll; i<numCRs; i++){
                bUp->GetEntry(i);
                bDown->GetEntry(i);
                bNom->GetEntry(i);
                bObs->GetEntry(i);
                m_variationsInfo["up"][processName][m_binNames[i]].push_back(branchValUp);
                m_variationsInfo["down"][processName][m_binNames[i]].push_back(branchValDown);
                m_AsimovNominalYields[processName][m_binNames[i]] = branchValNominal;
                m_NobsPerRegion[m_binNames[i]] = branchValObs;
            }
        }
    }

}

void Ws2l2nuHandler::setInterpCode(int code){
    m_code = code;
    this->setInterpCodeVector();
}


void Ws2l2nuHandler::setInterpCodeVector(){

    for (int i=0; i<m_nps.getSize(); i++){
        m_interpCodes.push_back(m_code);
    }
}

Ws2l2nuHandler::~Ws2l2nuHandler(){

}

void Ws2l2nuHandler::PrintVariationsDict(){

    std::cout << "Elements of Yields Variations Dict: " << std::endl;
    for (const auto& [key, value] :  m_variationsInfo){
        for (const auto& [valKey, valValue] : value){
            for (const auto& [valValueKey, valValueValue] : valValue){
                std::cout << "m_variationsInfo[" << key << "][" << valKey << "][" << valValueKey <<"][";
                for (auto vecVal : valValueValue){
                    std::cout << vecVal << " ";
                }
                std::cout << "]" << std::endl;
            }
        }
    }
}

void Ws2l2nuHandler::PrintAsimovNominalDict(){

    std::cout << "Elements of Asimov Nominal Yields Dict: " << std::endl;
    for (const auto& [key, value] : m_AsimovNominalYields){
        for (const auto& [valKey, valValue] : value){
            std::cout << "m_AsimovNominalYields[" << key << "][" << valKey << "] = " << valValue << std::endl;
        }
    }
}

void Ws2l2nuHandler::PrintObsYieldsDict(){

    std::cout << "The Observed Yields of the Control Regions: " << std::endl;
    for (const auto& [key, value] : m_NobsPerRegion){
        std::cout << "m_NobsPerRegion[" << key << "] = " << value << std::endl;
    }
}

std::map<std::string, RooAddPdf*> Ws2l2nuHandler::generatePdfsAndDataSets(RooArgList multipliers, RooRealVar* ATLAS_LUMI, RooRealVar* weightVar){

    std::map<std::string, RooAddPdf*> outputMap;
    for (auto binName : m_binNames){
        std::cout << "ws2l2nuHandler.cxx - generatePdfsAndDataSets() - looking at bin " << binName << std::endl;
        std::string pdfNameTitle = "pdf_"+binName;
        std::string obsName = "obs_"+binName;
        std::string dsAsimovName = "asimovDS_"+binName;
        std::string dsDataName = "dataDS_"+binName;

        RooArgList listOfUniformsPerProcess;
        RooArgList listOfCoefficientsPerProcess;
        RooRealVar* obs = new RooRealVar(obsName.c_str(), obsName.c_str(), 0.0, 1.0);
        m_wsObservables.add(*obs);
        for (auto process : m_processList){
            std::string factorName = "interpFactor_"+binName+"_"+process;
            std::string nominalYieldName = "n_"+binName+"_"+process;
            std::string nuYieldName = "nu_"+binName+"_"+process;
            std::string unifPdfName = "pdf_"+binName+"_"+process;

            RooFormulaVar* nominalYield = new RooFormulaVar(nominalYieldName.c_str(), "@0*@1*@2/140.1", RooArgList(*ATLAS_LUMI, (*(RooRealVar*)multipliers.find(std::string("f_"+process).c_str())), RooRealConstant::value(m_AsimovNominalYields.at(process).at(binName))));
            RooStats::HistFactory::FlexibleInterpVar* interpFactor = new RooStats::HistFactory::FlexibleInterpVar(factorName.c_str(), factorName.c_str(),
                                    m_nps, 1.0, m_variationsInfo.at("down").at(process).at(binName), m_variationsInfo.at("up").at(process).at(binName),
                                    m_interpCodes);
            RooProduct* nuYield = new RooProduct(nuYieldName.c_str(), nuYieldName.c_str(), RooArgList(*nominalYield, *interpFactor));
            RooUniform* pdfUnif = new RooUniform(unifPdfName.c_str(), unifPdfName.c_str(), RooArgSet(*obs));

            listOfCoefficientsPerProcess.add(*nuYield);
            listOfUniformsPerProcess.add(*pdfUnif);
        }

        RooAddPdf* theAddPdf = new RooAddPdf(pdfNameTitle.c_str(), pdfNameTitle.c_str(), listOfUniformsPerProcess, listOfCoefficientsPerProcess, false);
        outputMap[binName] = theAddPdf;

        RooArgSet* argSetObs = new RooArgSet(*obs);
        double weightValueAsimov;
        weightValueAsimov = theAddPdf->expectedEvents(argSetObs);
        std::cout << "ws2l2nuHandler.cxx - generatePdfsAndDataSets() - Asimov Weight Value is " << weightValueAsimov << std::endl;
        weightVar->setVal(weightValueAsimov);
        RooDataSet* dsAsimov = new RooDataSet(dsAsimovName.c_str(), dsAsimovName.c_str(), RooArgSet(*obs, *weightVar), RooFit::WeightVar(*weightVar));
        dsAsimov->add(RooArgSet(*obs, *weightVar), weightValueAsimov, 0.0);
        m_asimovDSMap[binName] = dsAsimov;

        double weightValueData;
        weightValueData = m_NobsPerRegion.at(binName);
        std::cout << "ws2l2nuHandler.cxx - generatePdfsAndDataSets() - Data Weight Value is " << weightValueData << std::endl;
        weightVar->setVal(weightValueData);
        RooDataSet* dsRealData = new RooDataSet(dsDataName.c_str(), dsDataName.c_str(), RooArgSet(*obs, *weightVar), RooFit::WeightVar(*weightVar));
        dsRealData->add(RooArgSet(*obs, *weightVar), weightValueData, 0.0);
        m_realDSMap[binName] = dsRealData;
    }

    return outputMap;

}