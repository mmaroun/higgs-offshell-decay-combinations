#include "higgsOffshellDecayCombinations/ReadBinary.h"

ReadBinary::ReadBinary(const char* fileName, RooArgList nps, std::vector<std::string> processList):m_fileName(fileName), 
    m_nps(nps), m_processList(processList){

    TFile* myRootFile(TFile::Open(fileName));
    if ((!myRootFile) || myRootFile->IsZombie()){
        delete myRootFile;
        throw std::runtime_error("ROOT File not found or is a zombie, exiting.");
    }

    // Ratios and Weight Variable Storage
    m_paramWeightTree = myRootFile->Get<TTree>("nominal");
    if (!m_paramWeightTree) {
        m_paramWeightTree = myRootFile->Get<TTree>("nominal_sys_var_per_event");
        if (!m_paramWeightTree){
            m_missingWeightsFlag = true;
        }
    }

    // Event Yields Storage
    m_eventYieldTree = myRootFile->Get<TTree>("yields");
    if (!m_eventYieldTree){
        m_eventYieldTree = myRootFile->Get<TTree>("sys_var_yield");
        if (!m_eventYieldTree){
            m_missingYieldsFlag = true;
        }
    }

    if (m_missingWeightsFlag && m_missingYieldsFlag){
        throw std::runtime_error("Unable to retrieve either params/weights or yields trees; expected tree name 'nominal' or 'sys_var_per_event' and 'yields' or 'sys_var_yield'");
    }

    if (m_paramWeightTree){
        m_listOfRatiosAndWeights = m_paramWeightTree->GetListOfBranches();
        m_nBranches = m_listOfRatiosAndWeights->GetEntries();
        m_allBranches.reserve(m_nBranches);
        for (auto i = 0ll; i<m_nBranches; i++){
            TBranch* b = m_paramWeightTree->GetBranch(m_listOfRatiosAndWeights->At(i)->GetName());
            m_ratiosAndWeights.add(*(new RooRealVar(b->GetName(), b->GetName(), -INF, INF)));
            if (std::regex_match(b->GetName(), std::regex("(.*)(weight)(.*)"))) {m_weightName = b->GetName();}
        }
    }

    if (m_eventYieldTree){
        // assert(m_eventYieldTree->GetEntries() == 1);
        m_listOfProcesses = m_eventYieldTree->GetListOfBranches();
        m_nProcesses = m_listOfProcesses->GetEntries();
        m_allProcesses.reserve(m_nProcesses);
        for (auto i = 0ll; i<m_nProcesses; i++){
            TBranch* b = m_eventYieldTree->GetBranch(m_listOfProcesses->At(i)->GetName());
            m_allProcesses.push_back(b);
        }

        this->SetEventYieldsNamesAndValues();
        this->setYieldsInterpMap(m_nps, m_processList);
        this->setInterpCodeVector();
    }
    
}

ReadBinary::~ReadBinary(){
    if (m_paramWeightTree) delete m_paramWeightTree;
    if (m_eventYieldTree) delete m_eventYieldTree;
}

void ReadBinary::setInterpCode(int code){
    m_code = code;
    this->setInterpCodeVector();
}

void ReadBinary::setInterpCodeVector(){

    for (int i=0; i<m_nps.getSize(); i++){
        m_interpCodes.push_back(m_code);
    }

}

void ReadBinary::SetEventYieldsNamesAndValues(){

    if (!m_eventYieldTree){
        throw std::runtime_error("File read does not have event yields in a tree. Exiting");
    }

    m_eventYieldsNameAndValues = new std::map<std::string, Double_t>;
    Double_t branchVal;
    for (auto j = 0ll; j<m_nProcesses; j++){
        TBranch* b = m_allProcesses[j];
        b->SetAddress(&branchVal);
        b->GetEntry(0);
        (*m_eventYieldsNameAndValues)[std::string(b->GetName())] = branchVal;
    }

}

std::vector<std::string> ReadBinary::GetEventYieldsNames(){

    std::vector<std::string> output;
    for (const auto& [key, value] : *m_eventYieldsNameAndValues){
        output.push_back(key);
    }

    return output;
}

void ReadBinary::setYieldsInterpMap(RooArgList nps, std::vector<std::string> processList){

    if (!m_eventYieldTree){
        throw std::runtime_error("File read does not have event yields in a tree. Exiting");
    }

    for (auto processName : processList){
        m_eventYieldsInfo["nominal"][processName].push_back(m_eventYieldsNameAndValues->at("yield_tot_"+processName));
        for (auto np : nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string branchName = "yield_var_"+sysName+"_ratio_"+processName;
            m_eventYieldsInfo["down"][processName].push_back(m_eventYieldsNameAndValues->at(branchName+"_down")); 
            m_eventYieldsInfo["up"][processName].push_back(m_eventYieldsNameAndValues->at(branchName+"_up")); 
        }
    }

}

// This method is not recommended for Asimov or MC, only for real data or real-data-sized datasets
RooDataSet* ReadBinary::createRooDataSet(const char* name, const char* title){

    if (!m_paramWeightTree){
        throw std::runtime_error("File read does not have event ratios and weight values in a tree. Exiting");
    }

    // RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* output = new RooDataSet(name, title, m_ratiosAndWeights, RooFit::Import(*m_paramWeightTree), RooFit::WeightVar(m_weightName, true));

    return output;
}

// Debug method to create a 1-event Dataset for NLL value validation
RooDataSet* ReadBinary::createRooDataSet1Event(const char* name, const char* title){

    if (!m_paramWeightTree){
        throw std::runtime_error("File read does not have event ratios and weight values in a tree. Exiting");
    }

    TTree* oneEventTree = m_paramWeightTree->CopyTree("","",2,0); // Number of rows, index of first row
    RooDataSet* output = new RooDataSet(name, title, m_ratiosAndWeights, RooFit::Import(*oneEventTree), RooFit::WeightVar(m_weightName, true));
    delete oneEventTree;
    
    return output;

}

RooArgList ReadBinary::createNYieldsList(RooRealVar* ATLAS_LUMI){

    RooArgList output;
    for (auto processName : m_processList){
        std::string yieldNameN = "n_SR_"+processName;
        double value = m_eventYieldsInfo.at("nominal").at(processName)[0];
        output.add(*(new RooFormulaVar(yieldNameN.c_str(), "@0*@1/140.1", RooArgList(*ATLAS_LUMI, RooRealConstant::value(value)))));
    }

    return output;
}

RooArgList ReadBinary::createNuYieldsList(RooRealVar* ATLAS_LUMI){

    RooArgList output;
    for (auto processName : m_processList){
        std::string nominalName = "nominal_yield_SR_"+processName;
        std::string yieldNameNu = "nu_SR_"+processName;
        std::string interpName = "mod_"+processName;
        RooStats::HistFactory::FlexibleInterpVar* var = new RooStats::HistFactory::FlexibleInterpVar(interpName.c_str(), interpName.c_str(),
                                        m_nps, 1.0, m_eventYieldsInfo.at("down").at(processName), m_eventYieldsInfo.at("up").at(processName),
                                        m_interpCodes);
        RooFormulaVar* value = new RooFormulaVar(nominalName.c_str(), "@0*@1/140.1", RooArgList(RooRealConstant::value(m_eventYieldsInfo.at("nominal").at(processName)[0]), *ATLAS_LUMI));
        output.add(*(new RooProduct(yieldNameNu.c_str(), yieldNameNu.c_str(), RooArgList(*value, *var))));
    }
    return output;
}

RooArgList ReadBinary::createGArgList(std::string upDown){

    if (!m_paramWeightTree){
        throw std::runtime_error("File read does not have event ratios and weight values in a tree. Exiting");
    }

    if (upDown.compare("up") && upDown.compare("down")){
        throw std::runtime_error("First argument must be \"up\" or \"down\"");
    }

    RooArgList output;
    for (auto processName : m_processList){
        for (auto np : m_nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string branchName = "var_"+sysName+"_ratio_"+processName+"_"+upDown;
            output.add(*(RooRealVar*)m_ratiosAndWeights.find(branchName.c_str()));
        }
    }

    return output;
}

RooArgList ReadBinary::createPiecewiseInterpsforGs(){

    if (!m_paramWeightTree){
        throw std::runtime_error("File read does not have event ratios and weight values in a tree. Exiting");
    }

    RooArgList output;
    for (auto processName : m_processList){
        RooArgList upVars;
        RooArgList downVars;
        for (auto np : m_nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string varNameUp = "var_"+sysName+"_ratio_"+processName+"_up";
            std::string varNameDown = "var_"+sysName+"_ratio_"+processName+"_down";
            upVars.add(*(RooRealVar*)m_ratiosAndWeights.find(varNameUp.c_str()));
            downVars.add(*(RooRealVar*)m_ratiosAndWeights.find(varNameDown.c_str()));
        }
        std::string interpName = "piecewise_interp_"+processName;
        PiecewiseInterpolation* interpVar = new PiecewiseInterpolation(interpName.c_str(), interpName.c_str(), RooRealConstant::value(1.0),
                                        downVars, upVars, m_nps);
        interpVar->setAllInterpCodes(5);
        output.add(*interpVar);
    }

    return output;
}

std::map<std::string, RooDataSet*> ReadBinary::createProcessDataSets(){

    RooDataSet* mainData = new RooDataSet("DS", "DS", m_ratiosAndWeights, RooFit::Import(*m_paramWeightTree), RooFit::WeightVar(m_weightName, true));

    std::map<std::string, RooDataSet*> output;
    for (auto processName : m_processList){
        RooArgSet processVars;
        processVars.add(*(RooRealVar*)m_ratiosAndWeights.find(std::string("r_"+processName).c_str()));
        for (auto np : m_nps){
            std::string sysName = std::string(np->GetName()).erase(0,6); // erase "alpha_" from the np name
            std::string branchNameUp = "var_"+sysName+"_ratio_"+processName+"_up";
            std::string branchNameDown = "var_"+sysName+"_ratio_"+processName+"_down";
            processVars.add(*(RooRealVar*)m_ratiosAndWeights.find(branchNameUp.c_str()));
            processVars.add(*(RooRealVar*)m_ratiosAndWeights.find(branchNameDown.c_str()));
        }
        std::string processDSName = "dataSet_SR_"+processName;
        RooDataSet* processDataSet = (RooDataSet*)mainData->reduce(RooFit::Name(processDSName.c_str()), RooFit::Title(processDSName.c_str()), RooFit::SelectVars(processVars));
        output[processName] = processDataSet;
    }

    return output;
}