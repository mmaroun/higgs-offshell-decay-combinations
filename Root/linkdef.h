#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class RooDensityRatio+;
#pragma link C++ class RooTreeDataStoreStar+;
#pragma link C++ class RooDataSetStar+;
#pragma link C++ class RooNLLVarStar+;

#pragma link C++ namespace RooFit;

#endif