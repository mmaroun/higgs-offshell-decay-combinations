#include "higgsOffshellDecayCombinations/RooDensityRatio.h"

ClassImp(RooDensityRatio)

// Constructor with process ratios
RooDensityRatio::RooDensityRatio(const char* name, const char* title, RooArgList& procRatios):
    RooAbsPdf(name, title),
    m_nps("m_nps", "m_nps", this),
    m_eventYieldsN("m_eventYieldsN", "m_eventYieldsN", this),
    m_procRatios("m_procRatios", "m_procRatios", this),
    m_multipliers("m_multipliers", "m_multipliers", this),
    m_gUp("m_gUp", "m_gUp", this), m_gDown("m_gDown","m_gDown",this),
    m_expEvntSave("m_expEvntSave", "m_expEvntSave", this){

    this->addProcessRatios(procRatios);
    
}

// Constructor with POIs, nominal event yields, and process ratios
RooDensityRatio::RooDensityRatio(const char* name, const char* title, RooArgList& eventYieldsN, RooArgList& procRatios):
    RooAbsPdf(name, title),
    m_nps("m_nps", "m_nps", this),
    m_eventYieldsN("m_eventYieldsN", "m_eventYieldsN", this),
    m_procRatios("m_procRatios", "m_procRatios", this),
    m_multipliers("m_multipliers", "m_multipliers", this),
    m_gUp("m_gUp", "m_gUp", this), m_gDown("m_gDown","m_gDown",this),
    m_expEvntSave("m_expEvntSave", "m_expEvntSave", this){

    this->addEventYieldsN(eventYieldsN);
    this->addProcessRatios(procRatios);

}

// Constructor with POIs, nominal event yields, process ratios, and multipliers
RooDensityRatio::RooDensityRatio(const char* name, const char* title, RooArgList& eventYieldsN, RooArgList& procRatios, RooArgList& multipliers):
    RooAbsPdf(name, title),
    m_nps("m_nps", "m_nps", this),
    m_eventYieldsN("m_eventYieldsN", "m_eventYieldsN", this),
    m_procRatios("m_procRatios", "m_procRatios", this),
    m_multipliers("m_multipliers", "m_multipliers", this),
    m_gUp("m_gUp", "m_gUp", this), m_gDown("m_gDown","m_gDown",this),
    m_expEvntSave("m_expEvntSave", "m_expEvntSave", this){

    this->addEventYieldsN(eventYieldsN);
    this->addProcessRatios(procRatios);
    this->addMultipliers(multipliers);

}

// Full Constructor
RooDensityRatio::RooDensityRatio(const char* name, const char* title, RooArgList& nps, RooArgList& eventYieldsN,
                                     RooArgList& procRatios, RooArgList& multipliers, RooArgList& gDown, RooArgList& gUp, RooAbsReal& expEvntSave):
    RooAbsPdf(name, title),
    m_nps("m_nps", "m_nps", this),
    m_eventYieldsN("m_eventYieldsN", "m_eventYieldsN", this),
    m_procRatios("m_procRatios", "m_procRatios", this),
    m_multipliers("m_multipliers", "m_multipliers", this),
    m_gUp("m_gUp", "m_gUp", this), m_gDown("m_gDown","m_gDown",this),
    m_expEvntSave("m_expEvntSave", "m_expEvntSave", this, expEvntSave){
    
    this->addNPs(nps);
    this->addEventYieldsN(eventYieldsN);
    this->addProcessRatios(procRatios);
    this->addMultipliers(multipliers);
    this->addGDown(gDown);
    this->addGUp(gUp);

    m_nNPs = nps.getSize();

}

// Constructor from Other RooDensityRatio
RooDensityRatio::RooDensityRatio(const RooDensityRatio& other, const char* name) :
    RooAbsPdf(other,name),
    m_nps("m_nps", this, other.m_nps),
    m_eventYieldsN("m_eventYieldsN", this, other.m_eventYieldsN),
    m_procRatios("m_procRatios", this, other.m_procRatios),
    m_multipliers("m_multipliers", this, other.m_multipliers),
    m_gUp("m_gUp", this, other.m_gUp), m_gDown("m_gDown", this, other.m_gDown),
    m_expEvntSave("m_expEvntSave", this, other.m_expEvntSave){

    m_nNPs = other.m_nNPs;
    m_code = other.m_code;

}

// Standard Destructor (void)
RooDensityRatio::~RooDensityRatio(){

}

// Add NPs
void RooDensityRatio::addNPs(RooArgList& nps){
    
    this->addRooAbsCollection(nps, this->m_nps);
}

void RooDensityRatio::addEventYieldsN(RooArgList& eventYieldsN){
    
    this->addRooAbsCollection(eventYieldsN, this->m_eventYieldsN);
}

void RooDensityRatio::addProcessRatios(RooArgList& procRatios){
    
    this->addRooAbsCollection(procRatios, this->m_procRatios);
}

void RooDensityRatio::addMultipliers(RooArgList& multipliers){
  
    this->addRooAbsCollection(multipliers, this->m_multipliers);
}

void RooDensityRatio::addGUp(RooArgList& gUp){

    this->addRooAbsCollection(gUp, this->m_gUp);
}

void RooDensityRatio::addGDown(RooArgList& gDown){

    this->addRooAbsCollection(gDown, this->m_gDown);
}

void RooDensityRatio::addRooAbsCollection(RooAbsCollection& collection, RooAbsCollection& memberCollection){

    memberCollection.add(collection);
}

void RooDensityRatio::setExpEventsVal(RooAbsReal& expEvntSave){

    m_expEvntSave.setArg(expEvntSave);
}

void RooDensityRatio::setInterpCode(Int_t code){

    m_code = code;
}

inline double RooDensityRatio::flexibleInterpSingle(unsigned int code, double low, double high, double boundary, double nominal, double paramVal, double res) const {

    if (code == 0) {
        // piece-wise linear
        if (paramVal > 0) {
            return paramVal * (high - nominal);
        } else {
            return paramVal * (nominal - low);
        }
    } else if (code == 1) {
        // piece-wise log
        if (paramVal >= 0) {
            return res * (std::pow(high / nominal, +paramVal) - 1);
        } else {
            return res * (std::pow(low / nominal, -paramVal) - 1);
        }
    } else if (code == 2) {
        // parabolic with linear
        double a = 0.5 * (high + low) - nominal;
        double b = 0.5 * (high - low);
        double c = 0;
        if (paramVal > 1) {
            return (2 * a + b) * (paramVal - 1) + high - nominal;
        } else if (paramVal < -1) {
            return -1 * (2 * a - b) * (paramVal + 1) + low - nominal;
        } else {
            return a * std::pow(paramVal, 2) + b * paramVal + c;
        }
    } else if (code == 3) {
        // parabolic version of log-normal
        double a = 0.5 * (high + low) - nominal;
        double b = 0.5 * (high - low);
        double c = 0;
        if (paramVal > 1) {
            return (2 * a + b) * (paramVal - 1) + high - nominal;
        } else if (paramVal < -1) {
            return -1 * (2 * a - b) * (paramVal + 1) + low - nominal;
        } else {
            return a * std::pow(paramVal, 2) + b * paramVal + c;
        }
    } else if (code == 4) {
        double x = paramVal;
        if (x >= boundary) {
            return x * (high - nominal);
        } else if (x <= -boundary) {
            return x * (nominal - low);
        }
    
        // interpolate 6th degree
        double t = x / boundary;
        double eps_plus = high - nominal;
        double eps_minus = nominal - low;
        double S = 0.5 * (eps_plus + eps_minus);
        double A = 0.0625 * (eps_plus - eps_minus);
    
        return x * (S + t * A * (15 + t * t * (-10 + t * t * 3)));
    } else if (code == 5) {
        double x = paramVal;
        double mod = 1.0;
        if (x >= boundary) {
            mod = std::pow(high / nominal, +paramVal);
        } else if (x <= -boundary) {
            mod = std::pow(low / nominal, -paramVal);
        } else {
            // interpolate 6th degree exp
            double x0 = boundary;
    
            // GHL: Swagato's suggestions
            double powUp = std::pow(high / nominal, x0);
            double powDown = std::pow(low / nominal, x0);
            double logHi = std::log(high);
            double logLo = std::log(low);
            double powUpLog = high <= 0.0 ? 0.0 : powUp * logHi;
            double powDownLog = low <= 0.0 ? 0.0 : -powDown * logLo;
            double powUpLog2 = high <= 0.0 ? 0.0 : powUpLog * logHi;
            double powDownLog2 = low <= 0.0 ? 0.0 : -powDownLog * logLo;
    
            double S0 = 0.5 * (powUp + powDown);
            double A0 = 0.5 * (powUp - powDown);
            double S1 = 0.5 * (powUpLog + powDownLog);
            double A1 = 0.5 * (powUpLog - powDownLog);
            double S2 = 0.5 * (powUpLog2 + powDownLog2);
            double A2 = 0.5 * (powUpLog2 - powDownLog2);
    
            // fcns+der+2nd_der are eq at bd
            double a = 1. / (8 * x0) * (15 * A0 - 7 * x0 * S1 + x0 * x0 * A2);
            double b = 1. / (8 * x0 * x0) * (-24 + 24 * S0 - 9 * x0 * A1 + x0 * x0 * S2);
            double c = 1. / (4 * std::pow(x0, 3)) * (-5 * A0 + 5 * x0 * S1 - x0 * x0 * A2);
            double d = 1. / (4 * std::pow(x0, 4)) * (12 - 12 * S0 + 7 * x0 * A1 - x0 * x0 * S2);
            double e = 1. / (8 * std::pow(x0, 5)) * (+3 * A0 - 3 * x0 * S1 + x0 * x0 * A2);
            double f = 1. / (8 * std::pow(x0, 6)) * (-8 + 8 * S0 - 5 * x0 * A1 + x0 * x0 * S2);
    
            // evaluate the 6-th degree polynomial using Horner's method
            double value = 1. + x * (a + x * (b + x * (c + x * (d + x * (e + x * f)))));
            mod = value;
        }
        return res * (mod - 1.0);
    }
    return 0.0;
}

double RooDensityRatio::expectedEvents(const RooArgSet* /*nset*/) const {

    return m_expEvntSave.arg().getVal();
}

std::unique_ptr<RooAbsReal> RooDensityRatio::createExpectedEventsFunc(const RooArgSet* /*nset*/) const {

    std::unique_ptr<RooAbsReal> output = std::unique_ptr<RooAbsReal>(new RooRealVar("expEventsFunc", "expEventsFunc", m_expEvntSave.arg().getVal()));

    return output;
}

Double_t RooDensityRatio::evaluate() const {

    Double_t densityRatio = 0.0;
    for (int i=0; i<m_procRatios.getSize(); i++){
        Double_t gFactor = 1.0;
        if (!((std::abs(((RooRealVar*)m_multipliers.at(i))->getVal()) < 1e-15) || (((RooRealVar*)m_procRatios.at(i))->getVal() == 0))){
            for (auto j=0u; j<m_nNPs; j++){
                int gIndex = i*m_nNPs+j;
                gFactor += this->flexibleInterpSingle(m_code, ((RooRealVar*)m_gDown.at(gIndex))->getVal(), 
                                                        ((RooRealVar*)m_gUp.at(gIndex))->getVal(), 1.0, 1.0,
                                                        ((RooRealVar*)m_nps.at(j))->getVal(), gFactor);
            }
        }
        densityRatio += ((RooRealVar*)m_eventYieldsN.at(i))->getVal() * ((RooRealVar*)m_multipliers.at(i))->getVal() * gFactor * ((RooRealVar*)m_procRatios.at(i))->getVal();
    }
    densityRatio /= this->expectedEvents(nullptr);

    return densityRatio;
}