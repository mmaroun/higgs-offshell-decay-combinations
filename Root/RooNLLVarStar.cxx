#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"

// #include <RooNaNPacker.h>
// #include <RooBatchComputeTypes.h>

#include <TError.h>

#include <limits>
#include <cstdint>
#include <cmath>
#include <cassert>
#include <numeric>
#include <cstring>

struct RooNaNPacker {
   double _payload;

   // In double-valued NaNs, on can abuse the lowest 51 bit for payloads.
   // We use this to pack a float into the lowest 32 bits, which leaves
   // 19-bit to include a magic tag to tell NaNs with payload from ordinary
   // NaNs:
   static constexpr uint64_t magicTagMask = 0x3ffff00000000;
   static constexpr uint64_t magicTag = 0x321ab00000000;

   constexpr RooNaNPacker() : _payload(0.) {}

   /// Create NaN with a packed floating point number.
   explicit RooNaNPacker(float value) : _payload(packFloatIntoNaN(value)) {}

   /// Pack float into mantissa of NaN.
   void setPayload(float payload)
   {
      _payload = packFloatIntoNaN(payload);

      if (!std::isnan(_payload)) {
         // Big-endian machine or other. Just return NaN.
         warn();
         _payload = std::numeric_limits<double>::quiet_NaN();
      }
   }

   /// Accumulate a packed float from another NaN into `this`.
   void accumulate(double val) { *this += unpackNaN(val); }

   /// Unpack floats from NaNs, and sum the packed values.
   template <class It_t>
   static double accumulatePayloads(It_t begin, It_t end)
   {
      double sum = std::accumulate(begin, end, 0.f, [](float acc, double val) { return acc += unpackNaN(val); });

      return packFloatIntoNaN(sum);
   }

   /// Add to the packed float.
   RooNaNPacker &operator+=(float val)
   {
      setPayload(getPayload() + val);
      return *this;
   }

   /// Multiply the packed float.
   RooNaNPacker &operator*=(float val)
   {
      setPayload(getPayload() * val);
      return *this;
   }

   /// Retrieve packed float. Returns zero if number is not NaN
   /// or if float wasn't packed by this class.
   float getPayload() const { return isNaNWithPayload(_payload) ? unpackNaN(_payload) : 0.; }

   /// Retrieve a NaN with the current float payload packed into the mantissa.
   double getNaNWithPayload() const { return _payload; }

   /// Test if this struct has a float packed into its mantissa.
   bool isNaNWithPayload() const { return isNaNWithPayload(_payload); }

   /// Test if `val` has a float packed into its mantissa.
   static bool isNaNWithPayload(double val)
   {
      uint64_t tmp;
      std::memcpy(&tmp, &val, sizeof(uint64_t));
      return std::isnan(val) && (tmp & magicTagMask) == magicTag;
   }

   /// Pack float into mantissa of a NaN. Adds a tag to the
   /// upper bits of the mantissa, so a "normal" NaN can be
   /// differentiated from a NaN with a payload.
   static double packFloatIntoNaN(float payload)
   {
      double result = std::numeric_limits<double>::quiet_NaN();
      uint64_t tmp;
      std::memcpy(&tmp, &result, sizeof(uint64_t));
      tmp |= magicTag;
      std::memcpy(&tmp, &payload, sizeof(float));
      std::memcpy(&result, &tmp, sizeof(uint64_t));
      return result;
   }

   /// If `val` is NaN and a this NaN has been tagged as containing
   /// a payload, unpack the float from the mantissa.
   /// Return 0 otherwise.
   static float unpackNaN(double val)
   {
      float tmp;
      std::memcpy(&tmp, &val, sizeof(float));
      return isNaNWithPayload(val) ? tmp : 0.;
   }

   /// Warn that packing only works on little-endian machines.
   static void warn()
   {
      static bool haveWarned = false;
      if (!haveWarned) {
         Warning("RooNaNPacker",
                 "Fast recovery from undefined function values only implemented for little-endian machines."
                 " If necessary, request an extension of functionality on https://root.cern");
      }
      haveWarned = true;
   }
};

ClassImp(RooNLLVarStar)

RooNLLVarStar::RooNLLVarStar(const char* name, const char* title, RooAbsPdf* pdf, RooAbsData* data, bool extended, RooAbsTestStatistic::Configuration const& cfg)
    : RooAbsReal(name, title), m_data(data), m_pdf(pdf), m_extended(extended){
    
    std::cout << "RooNLLVarStar.cxx - constructor() - executing the one line..." << std::endl;
    m_nCPU = cfg.nCPU;
    m_normSet = new RooArgSet;
    data->get()->snapshot(*m_normSet, false);
    // std::cout << "RooNLLVarStar.cxx - constructor() - Printing the m_data: " << std::endl;
    // m_data->Print("v");
    std::cout << "RooNLLVarStar.cxx - constructor() - the address of the input dataset: " << data << std::endl;
    std::cout << "RooNLLVarStar.cxx - constructor() - the address of the member dataset: " << m_data << std::endl;
    std::cout << "RooNLLVarStar.cxx - constructor() - finished successfully" << std::endl;


}

RooNLLVarStar::RooNLLVarStar(const RooNLLVarStar& other, const char* name)
    : RooAbsReal(other, name), m_data(other.m_data), m_pdf(other.m_pdf), m_extended(other.m_extended){

    m_nCPU = other.m_nCPU;
    m_normSet = new RooArgSet;
    other.m_data->get()->snapshot(*m_normSet, false);
}

RooNLLVarStar::~RooNLLVarStar(){

}

bool RooNLLVarStar::setDataSlave(RooAbsData& /*data*/, bool /*cloneData*/, bool /*ownNewDataAnyway*/){

    /* I'm not sure what to put here. We aren't cloning the data, and the data set that 
    will be passed into this object isn't a unique pointer, so we don't have to worry
    about ownership, really. */

    return true;

}

void RooNLLVarStar::applyWeightSquared(bool flag){

    if (m_gofOpMode == Slave){
        if (flag != m_weightSq){
            m_weightSq = flag; 
            std::swap(m_offset, m_offsetSaveW2);
        }
        setValueDirty();
    } else if (m_gofOpMode == MPMaster){
        for (int i=0; i<m_nCPU; i++){
            m_mpfeArray[i]->applyNLLWeightSquared(flag);
        }
    } else if (m_gofOpMode == SimMaster){
        for (auto& gof : m_gofArray){
            static_cast<RooNLLVar&>(*gof).applyWeightSquared(flag); // This line might be an issue with combination
        }
    }

}

double RooNLLVarStar::evaluate() const {

    std::size_t firstEvent = 0;
    std::size_t stepSize = 1;
    std::size_t lastEvent = m_data->numEntries();

    return this->evaluatePartition(firstEvent, lastEvent, stepSize);
}

double RooNLLVarStar::evaluatePartition(std::size_t firstEvent, std::size_t lastEvent, std::size_t stepSize) const {
    
    std::cout << "RooNLLVarStar.cxx - evaluatePartition() - entering..." << std::endl;
    ROOT::Math::KahanSum<double> result{0.0};
    double sumWeight{0.0};

    std::cout << "RooNLLVarStar.cxx - evaluatePartition() - calling computeScalar..." << std::endl;
    std::tie(result, sumWeight) = computeScalar(stepSize, firstEvent, lastEvent);
    std::cout << "RooNLLVarStar.cxx - evaluatePartition() - computeScalar done" << std::endl;

    if (m_extended && m_simCount >1){
        std::cout << "RooNLLVarStar.cxx - evaluatePartition() - adding combinatorics term..." << std::endl;
        result += sumWeight * std::log(static_cast<double>(m_simCount));
    }

    if (m_first) {
        std::cout << "RooNLLVarStar.cxx - evaluatePartition() - first if clause entered" << std::endl;
        m_first = false;
        m_pdf->wireAllCaches();
        std::cout << "RooNLLVarStar.cxx - evaluatePartition() - pdf->wireAllCaches() ran" << std::endl;
    }

    m_evalCarry = result.Carry();
    return result.Sum();
    std::cout << "RooNLLVarStar.cxx - evaluatePartition() - finished, returning " << result.Sum() << std::endl;
}

RooNLLVar::ComputeResult RooNLLVarStar::computeScalar(std::size_t stepSize, std::size_t firstEvent, std::size_t lastEvent) const {

    std::cout << "RooNLLVarStar.cxx - computeScalar() - calling  computeScalarFunc()..." << std::endl;
    return computeScalarFunc(m_pdf, m_data, m_normSet, m_weightSq, stepSize, firstEvent, lastEvent, m_offsetPdf);
    std::cout << "RooNLLVarStar.cxx - computeScalar() - finished" << std::endl;
}

RooNLLVar::ComputeResult RooNLLVarStar::computeScalarFunc(RooAbsPdf* pdf, RooAbsData* data, RooArgSet* normSet, bool weightSq, 
                    std::size_t stepSize, std::size_t firstEvent, std::size_t lastEvent, RooAbsPdf* offsetPdf) const {
    
    std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - entering..." << std::endl;
    ROOT::Math::KahanSum<double> kahanWeight;
    ROOT::Math::KahanSum<double> kahanProb;
    RooNaNPacker packedNaN(0.f);

    std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - firstEvent: " << firstEvent << std::endl;
    std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - lastEvent: " << lastEvent << std::endl;
    std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - stepSize: " << stepSize << std::endl;

    std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - entering loop..." << std::endl;
    for (auto i=firstEvent; i<lastEvent; i+=stepSize){

        if (i%100000 == 0) std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - i = " << i << std::endl;
        // std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - i = " << i << std::endl;
        data->get(i);
        if (data->get(i) == nullptr){
            std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - nullptr was retrieved by data->get(i), exiting." << std::endl;
        }
        // if (i%100000 == 0) std::cout << "           data(" << i << ") got " << std::endl;
        // if (i%100000 == 0) std::cout << "           Printing Data: " << std::endl;
        // if (i%100000 == 0) data->get(i)->Print("v");
        // std::cout << "           data(" << i << ") got " << std::endl;
        // std::cout << "           this is what test gives: " << std::endl;
        // test->Print("v");
        // std::cout << "           Printing Data: " << std::endl;
        // data->get(i)->Print("v");
        // const RooArgSet* test = data->get(i);
        // std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - value of row " << i << " of var_ATLAS_LUMI_ratio_EWSBI10_up: " << ((RooRealVar*)test->find("var_ATLAS_LUMI_ratio_EWSBI10_up"))->getVal() << std::endl;

        double weight{0.0};
        if (weightSq){
            if (i%100000 == 0) std::cout << "           weightSquared if clause entered " << std::endl;
            weight = data->weightSquared();
        } else { 
            if (i%100000 == 0) std::cout << "           weightSquared else clause entered " << std::endl;
            weight = data->weight();
            if (weight * weight == 0){
                continue;
            }
        }
        // if (i%100000 == 0) std::cout << "           weight value: " << weight << std::endl;
        // std::cout << "           weight value: " << weight << std::endl;

        // std::cout << "           using the following NormSet: " << std::endl;
        // normSet->Print("v");
        if (i%100000 == 0) std::cout << "           getting log value..." << std::endl;
        double logProb = pdf->getLogVal(normSet);
        if (i%100000 == 0) std::cout << "           log value got and is: " << logProb << std::endl;

        if (offsetPdf){
            if (i%100000 == 0) std::cout << "           entered offsetPdf if clause" << std::endl;
            logProb -= offsetPdf->getLogVal(normSet);
            if (i%100000 == 0) std::cout << "           log val is now" << logProb << std::endl;
        }

        if (i%100000 == 0) std::cout << "           making the term with the weight and log val..." << std::endl;
        const double term = -weight * logProb;
        if (i%100000 == 0) std::cout << "           term made and is " << term << std::endl;

        if (i%100000 == 0) std::cout << "           updating kahanWeight..." << std::endl;
        kahanWeight.Add(weight);
        if (i%100000 == 0) std::cout << "           done. updating kahanProb..." << std::endl;
        // std::cout << "              kahanWeight is now " << kahanWeight.Sum() << std::endl;
        kahanProb.Add(term);
        // std::cout << "              kahanProb sum is now " << kahanProb.Sum() << std::endl;
        // std::cout << "              kahanProb result is now " << kahanProb.Result() << std::endl;
        if (i%100000 == 0) std::cout << "           done. updating packedNaN..." << std::endl;
        packedNaN.accumulate(term);
        if (i%100000 == 0) std::cout << "           done." << std::endl;

    }

    if (packedNaN.getPayload() != 0.){
        std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - packedNaN.getPayload() is not 0, entered clause " << std::endl;
        return {ROOT::Math::KahanSum<double>{packedNaN.getNaNWithPayload()}, kahanWeight.Sum()};
        std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - done from nan clause" << std::endl;
    }

    return {kahanProb, kahanWeight.Sum()};
    // std::cout << "RooNLLVarStar.cxx - computeScalarFunc() - done" << std::endl;

}