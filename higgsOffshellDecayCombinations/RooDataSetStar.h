#ifndef __ROODATASETSTAR_H__
#define __ROODATASETSTAR_H__

#include <TTree.h>
#include <RooDataSet.h>
#include <RooStringView.h>
#include <RooArgSet.h>
#include <RooRealVar.h>
#include <RooTrace.h>
#include "RooTreeDataStoreStar.h"

class RooDataSet;

class RooDataSetStar : public RooDataSet 
{
    public: 

        // Constructor with pointer to RooTreeDataStoreStar
        RooDataSetStar(RooStringView name, RooStringView title, const RooArgSet& vars, std::unique_ptr<RooTreeDataStoreStar> store, const char* wgtVarName);

        // Standard Destructor
        ~RooDataSetStar();

        const RooArgSet* get(Int_t index) const override;

    protected:


    private:
        ClassDefOverride(RooDataSetStar, 1) // RooDataSet that interacts directly with pointer to Tree from RooTreeDataStoreStar

};

#endif