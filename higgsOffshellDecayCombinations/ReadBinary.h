#ifndef __HODC_READBINARY_H__
#define __HODC_READBINARY_H__

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <RooArgList.h>
#include <RooAbsArg.h>
#include <RooRealVar.h>
#include <RooConstVar.h>
#include <RooDataSet.h>
#include <RooArgSet.h>
#include <RooAbsData.h>
#include <RooFormulaVar.h>
#include <RooRealConstant.h>
#include <RooProduct.h>
#include <RooStats/HistFactory/FlexibleInterpVar.h> 
#include <RooStats/HistFactory/PiecewiseInterpolation.h>

#include <string>
#include <regex>
#include <limits>

#define INF std::numeric_limits<float>::infinity()

class RooAbsReal;

class ReadBinary {

public:

    // Constructor with filename we want to read from
    ReadBinary(const char* fileName, RooArgList nps, std::vector<std::string> processList);

    // Standard constructor (not sure if necessary)
    ReadBinary();

    // Standard Destructor
    virtual ~ReadBinary();

    // Get the filename that we're reading
    const std::string& GetInputName(){return m_fileName;}

    // Get all the event yields names and values that we're reading
    std::map<std::string, Double_t>* GetEventYieldsNamesAndValues(){return m_eventYieldsNameAndValues;}

    // Get all the event yields names that we're reading
    std::vector<std::string> GetEventYieldsNames();

    // Return the RooArgSet of Observables for RooDensityRatio
    RooArgSet GetObservables(){return m_ratiosAndWeights;}

    // Return the pointer to the tree of weights and ratios for the datastore
    TTree* GetRatiosWeightTTree(){return m_paramWeightTree;}

    // Return the map of yields and vectors of systematics yields variation ratios
    std::map<std::string, std::map<std::string, std::vector<double>>> GetEventYieldsMap(){return m_eventYieldsInfo;}

    // Return the vector of interpolation codes used to create the Nu yields objects
    std::vector<int> GetInterpCodes(){return m_interpCodes;}

    // Return the interpolation code assigned to each Nu yield
    int GetInterpCode(){return m_code;}

    // Set the interpolation code assigned to each Nu yield to a different value
    void setInterpCode(int code);

    // Create a dataset of all the info parsed by the class to be passed into RooDensityRatio
    RooDataSet* createRooDataSet(const char* name, const char* title);

    RooDataSet* createRooDataSet1Event(const char* name, const char* title);

    RooArgList createNYieldsList(RooRealVar* ATLAS_LUMI);

    RooArgList createNuYieldsList(RooRealVar* ATLAS_LUMI);

    // Create the RooArgLists of g_up and g_down for systematics ratios
    RooArgList createGArgList(std::string upDown);

    // Create PiecewiseInterpolations of g_up and g_down values for a more traditional PDF construction
    RooArgList createPiecewiseInterpsforGs();

    // Create a map of the individual Process DataSets from the main tree of information
    std::map<std::string, RooDataSet*> createProcessDataSets();

private:

    // Set the names and values of the event yields from the ROOT file
    void SetEventYieldsNamesAndValues();

    // Set the names of the event yields from the ROOT file
    void SetEventYieldsNames();

    // Create the Vectors to create the flexible interp variables low, nominal, and high values
    void setYieldsInterpMap(RooArgList nps, std::vector<std::string> processList);

    // Assign the interpolation code to apply to each variation per systematic in the Nu yields
    void setInterpCodeVector();

    // The name of the root file from which to extract values for RooDensityRatio
    std::string m_fileName;

    // The proxy variable of the NPs list
    RooArgList m_nps;

    // The tree from the root file from which to extract parameters and weights and store into a RooDataSet
    TTree* m_paramWeightTree = nullptr;

    // The tree from the root file from which to extract the event yields and store into a RooArgSet
    TTree* m_eventYieldTree = nullptr;

    // The RooArgSet of Branch Names from TTree
    RooArgSet m_ratiosAndWeights;

    // List of all branches in tree of input root file
    std::vector<TBranch*> m_allBranches;

    // List of all processes for event yield in tree of input root file
    std::vector<TBranch*> m_allProcesses;

    // List of all event yields saved in root file
    std::map<std::string, Double_t>* m_eventYieldsNameAndValues;

    // The map of Event yields, organized by nominal and vectors of up/down variations
    std::map<std::string, std::map<std::string, std::vector<double>>> m_eventYieldsInfo;

    // List of all objects saved in root file to be passed into RooDensityRatio
    const TObjArray* m_listOfRatiosAndWeights;

    // List of all yields/channels in root file
    const TObjArray* m_listOfProcesses;

    // The String of the name of the weight variable
    const char* m_weightName;

    // The vector of strings corresponding to process names
    std::vector<std::string> m_processList;

    // The vector of interpolation codes for the FlexibleInterpVars of Nu Yields
    std::vector<int> m_interpCodes;

    // Number of branches saved in root file 
    Long64_t m_nBranches{0};

    // Number of channels for event yields saved in root file
    Long64_t m_nProcesses{0};

    // Number of parameters saved in root file to be passed into RooDensityRatio
    Long64_t m_nParameters{0};

    // Number of weights saved in root file to be passed into RooDensityRatio
    Long64_t m_nWeights{0};

    // The interpolation code for the FlexibleInterpVars
    int m_code{4};

    // A flag to see if the ratios and weight tree exists
    bool m_missingWeightsFlag{false};

    // A flag to see if the yields tree exists
    bool m_missingYieldsFlag{false};
     
};

#endif
