#ifndef __HODC_HANDLESYSTEMATICS_H__
#define __HODC_HANDLESYSTEMATICS_H__

#include <RooArgList.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooRealConstant.h>

#include <string>
#include <limits>

#define INF std::numeric_limits<float>::infinity()

class RooAbsReal;

class HandleSystematics {

public:

    // Standard Constructor
    HandleSystematics();

    // Constructor with systematics list
    HandleSystematics(std::vector<std::string> sysList);

    // Standard Destructor
    virtual ~HandleSystematics();

    // The function to set the list of systematics
    void SetSystematicsList(std::vector<std::string> sysList);

    // The function to retrieve the Nuisance Parameters
    RooArgList GetNuisanceParams() {return m_nps;}

    // The function to retrieve the Global Observables
    RooArgList GetGlobalObservables() {return m_gos;}

    // The function to retrieve the Gaussians
    RooArgList GetGaussianPdfs() {return m_gaussians;}

    // The function to retrieve the number of systematics
    Int_t GetNumberOfSystematics() {return m_nSys;}

    // The function to set and return the Constraint PDF of the system
    RooProdPdf* ComputeConstraintPdf(const char* name, const char* title);


private:

    // The RooArgList (? or set?) of Nuisance Parameters
    RooArgList m_nps;

    // The RooArgList (? or set?) of Global Observables
    RooArgList m_gos;

    // The map of RooArgLists of RooGaussians that contain each NP and GO per process
    RooArgList m_gaussians;

    // The number of systematics
    Int_t m_nSys{0};

};

#endif