#ifndef __ROOPARAMETERIZEDHISTPDF_H__
#define __ROOPARAMETERIZEDHISTPDF_H__

#include <TObject.h>

#include <RooHistPdf.h>


class RooParameterizedHistPdf : public RooHistPdf 
{
    public:
        

    protected:


    private:
        ClassDefOverride(RooParameterizedHistPdf, 1) // Extension of Hist Pdf to call different shapes based on observed parameter value
};

#endif