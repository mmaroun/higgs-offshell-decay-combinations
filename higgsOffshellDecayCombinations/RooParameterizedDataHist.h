#ifndef __ROOPARAMETERIZEDDATAHIST_H__
#define __ROOPARAMETERIZEDDATAHIST_H__

#include <TObject.h>

#include <RooDataHist.h>


class RooParameterizedDataHist : public RooDataHist 
{
    public:
        

    protected:


    private:
        ClassDefOverride(RooParameterizedDataHist, 1) // Extension of Data Hist to call different datasets based on observed parameter value
};

#endif