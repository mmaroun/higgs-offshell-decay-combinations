#ifndef __ROOTREEDATASTORESTAR_H__
#define __ROOTREEDATASTORESTAR_H__

#include <TTree.h>
#include <RooAbsDataStore.h>
#include <RooTreeDataStore.h>
#include <RooStringView.h>
#include <RooArgSet.h>

class RooAbsArg;
class RooArgSet;
class RooRealVar;
class RooTreeDataStore;

class RooTreeDataStoreStar : public RooTreeDataStore
{
    public: 

        // Constructor with a tree
        RooTreeDataStoreStar(const char* name, const char* title, TTree* tree, const RooArgSet& vars, const char* weightVarName);

        // Copy constructor
        RooTreeDataStoreStar(const RooTreeDataStoreStar& other, const char* newName = nullptr);

        // Function to retrieve the tree pointer
        TTree* tree() {return m_tree;}

        // Standard Destructor
        ~RooTreeDataStoreStar();

    protected:

        // The Tree that will be used to store the data (RooTreeDataStore::_tree is private)
        TTree* m_tree = nullptr;

        // Possible overload of loadValues() (May be necessary for createNLL to be used properly)
        void loadValues();

    private:
        ClassDefOverride(RooTreeDataStoreStar, 1) // TTree-based Data Storage class for DataSets exceeding 40 GB in size
};

#endif