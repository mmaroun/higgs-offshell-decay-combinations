#ifndef __HODC_WS2L2NUHANDLER_H__
#define __HODC_WS2L2NUHANDLER_H__

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <RooArgSet.h>
#include <RooRealVar.h>
#include <RooFormulaVar.h>
#include <RooDataSet.h>
#include <RooAddPdf.h>
#include <RooUniform.h>
#include <RooArgList.h>
#include <RooProduct.h>
#include <RooRealConstant.h>
#include <RooStats/HistFactory/FlexibleInterpVar.h>

#include <string>

class Ws2l2nuHandler {

public:

    Ws2l2nuHandler(const char* fileName2l2nu, RooArgList nps, std::vector<std::string> processList, std::vector<std::string> binNames);

    Ws2l2nuHandler();

    virtual ~Ws2l2nuHandler();

    const std::string& GetInputFileName() {return m_inputFileName;}

    const std::string& GetInputTreeName() {return m_inputTreeName;}

    RooArgList GetNPs(){return m_nps;}

    std::vector<std::string> GetProcessList(){return m_processList;}

    std::vector<std::string> GetBinNames(){return m_binNames;}

    std::map<std::string, std::map<std::string, std::map<std::string, std::vector<Double_t>>>> GetVariationsDict(){return m_variationsInfo;}

    std::map<std::string, std::map<std::string, Double_t>> GetAsimovNominalDict(){return m_AsimovNominalYields;}

    std::map<std::string, Double_t> GetRealDataObservedYields(){return m_NobsPerRegion;}

    void PrintVariationsDict();

    void PrintAsimovNominalDict();

    void PrintObsYieldsDict();

    std::vector<int> GetInterpCodes(){return m_interpCodes;}

    // Return the interpolation code assigned to each Nu yield
    int GetInterpCode(){return m_code;}

    // Set the interpolation code assigned to each Nu yield to a different value
    void setInterpCode(int code);

    RooArgSet GetWSObservables(){return m_wsObservables;}

    // Generate Asimiov DSs for WS - Bin Name
    std::map<std::string, RooDataSet*> GetAsimovDataSets(){return m_asimovDSMap;}

    // Generate Real Data DSs for WS - Bin Name
    std::map<std::string, RooDataSet*> GetRealDataSets(){return m_realDSMap;}

    // Generate PDFs and DSs for WS - Bin Name
    std::map<std::string, RooAddPdf*> generatePdfsAndDataSets(RooArgList multipliers, RooRealVar* ATLAS_LUMI, RooRealVar* weightVar);

private:

    void readTree();

    void setInterpCodeVector();

    std::string m_inputFileName;

    std::string m_inputTreeName;

    // The proxy variable of the NPs list
    RooArgList m_nps;

    // The list of the processes in the analysis
    std::vector<std::string> m_processList;

    // The list of the bin names in the analysis
    std::vector<std::string> m_binNames;

    // The map that stores the N_obs for the Control Regions (real data) - Bin Name
    std::map<std::string, Double_t> m_NobsPerRegion;

    // The map that stores the N_exp for the Control Regions (asimov) - Channel, Bin Name
    std::map<std::string, std::map<std::string, Double_t>> m_AsimovNominalYields;

    // The map that stores the variations for the CRs - Up/Down, Channel, Bin Name (vector in systematics order)
    std::map<std::string, std::map<std::string, std::map<std::string, std::vector<Double_t>>>> m_variationsInfo;

    RooArgSet m_wsObservables;

    std::map<std::string, RooDataSet*> m_asimovDSMap;

    std::map<std::string, RooDataSet*> m_realDSMap;

    std::vector<int> m_interpCodes;

    int m_code{4};

};

#endif