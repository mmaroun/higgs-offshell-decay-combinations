#ifndef __HODC_TEXTTOPOISSONPARAMS_H__
#define __HODC_TEXTTOPOISSONPARAMS_H__

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <RooArgSet.h>
#include <RooRealVar.h>
#include <RooFormulaVar.h>
#include <RooDataSet.h>
#include <RooAddPdf.h>
#include <RooUniform.h>
#include <RooArgList.h>
#include <RooProduct.h>
#include <RooRealConstant.h>
#include <RooStats/HistFactory/FlexibleInterpVar.h>

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <limits>

#define INF std::numeric_limits<float>::infinity()

class TextToPoissonParams {

public: 

    // Constructor with CR filename we want to read from
    TextToPoissonParams(const char* crFileName, const char* varTreeFileName, RooArgList nps, std::vector<std::string> processList, bool runOnData=false);

    // Standard Constructor (not sure if necessary)
    TextToPoissonParams();

    // Standard Destructor
    virtual ~TextToPoissonParams();

    // Get the CR filename that we're reading
    const std::string& GetCRInputName(){return m_crFileName;}

    // Get the map of all values from each region
    std::map<std::string, std::map<std::string, Double_t>> GetCRDict(){return m_crDict;}

    // Get the map of all yield variations from each region
    std::map<std::string, std::map<std::string, std::map<Long64_t, std::vector<Double_t>>>> GetEventYieldsMap(){return m_eventYieldsInfo;}

    // Get the observables from the AddPdfs to set the Datasets accordingly
    std::map<std::string, RooRealVar*> GetCRObservables(){return m_crObservables;}

    // Get the datasets for each of the CR datasets
    std::map<std::string, RooDataSet*> GetCRDatasets(){return m_crDatasets;}

    // View the CR N_obs yields
    void printNobsCRs();

    // View the CR Dictionary
    void printCRDict();

    // View the CR Event Yields Variations
    void printVarYieldsDict();

    // Return the vector of interpolation codes used to create the Nu yields objects
    std::vector<int> GetInterpCodes(){return m_interpCodes;}

    // Return the interpolation code assigned to each Nu yield
    int GetInterpCode(){return m_code;}

    // Set the interpolation code assigned to each Nu yield to a different value
    void setInterpCode(int code);

    // Generate the Control Region Poisson Pdfs and return in a map by region
    std::map<std::string, RooAddPdf*> generateCRpdfs(RooArgList multipliers, RooRealVar* ATLAS_LUMI, RooRealVar* weightVar);

private:

    // Read the text file with the nominal CR yields
    void readTextFile();

    // Read the TTree with the N_obs_CR yields
    void readCRNobsFromTree();

    // Read the root file with the TTree of the CR yield variations
    void readVariationsTree();

    // Assign the interpolation code to apply to each variation per systematic in the Nu yields
    void setInterpCodeVector();

    // The name of the txt file from which to extract values for CR RooPoisson
    std::string m_crFileName;

    // The name of the root file from which to extract variations on the CR yields per process per np
    std::string m_varTreeFileName;

    // The map of CR Datasets associated with each CR pdf
    std::map<std::string, RooDataSet*> m_crDatasets;

    // The proxy variable of the NPs list
    RooArgList m_nps;

    // The vector that stores the N_obs for the Control Regions
    std::vector<Double_t> m_NobsCRs;

    // The list of the processes in the analysis
    std::vector<std::string> m_processList;

    // Map of all values read from text file for 4l control region
    std::map<std::string, std::map<std::string, Double_t>> m_crDict;

    // Map of all values read from the root file for the 4l control region with variations
    std::map<std::string, std::map<std::string, std::map<Long64_t, std::vector<Double_t>>>> m_eventYieldsInfo;

    // The map of the observables for the CR pdfs, to set their datasets accordingly
    std::map<std::string, RooRealVar*> m_crObservables;

    // The vector of interpolation codes for the FlexibleInterpVars of Nu Yields
    std::vector<int> m_interpCodes;

    // The interpolation code for the FlexibleInterpVars
    int m_code{4};

    // The Flag that says if the analysis is run on data or MC
    bool m_runOnData{false};

};

#endif