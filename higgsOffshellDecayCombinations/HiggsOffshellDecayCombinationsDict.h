#ifndef HIGGSOFFSHELLDECAYCOMBINATIONSDICT
#define HIGGSOFFSHELLDECAYCOMBINATIONSDICT

#include <higgsOffshellDecayCombinations/RooDensityRatio.h>
#include <higgsOffshellDecayCombinations/RooDataSetStar.h>
#include <higgsOffshellDecayCombinations/RooTreeDataStoreStar.h>
#include <higgsOffshellDecayCombinations/RooNLLVarStar.h>

#endif