#ifndef __ROODENSITYRATIO_H__
#define __ROODENSITYRATIO_H__

#include <TObject.h>

#include <RooAbsReal.h>
#include <RooRealVar.h>
#include <RooAbsPdf.h>
#include <RooAbsArg.h>
#include <RooSetProxy.h>
#include <RooListProxy.h>
#include <RooRealProxy.h>
#include <RooExpensiveObjectCache.h>
#include <RooAbsCollection.h>

class RooAbsReal;

class RooDensityRatio : public RooAbsPdf
{
    public:
        ///\brief the default constructor, void
        RooDensityRatio() {};

        ///\brief the standard constructor with only process ratios
        RooDensityRatio(const char* name, const char* title, RooArgList& procRatios);

        ///\brief the standard constructor with nominal event yields, and process ratios
        RooDensityRatio(const char* name, const char* title, RooArgList& eventYieldsN, RooArgList& procRatios);

        ///\brief the standard constructor with nominal event yields, process ratios, and multipliers (full stat-only constructor)
        RooDensityRatio(const char* name, const char* title, RooArgList& eventYieldsN, RooArgList& procRatios, RooArgList& multipliers);

        ///\brief the full constructor: NPs, nominal and systematics event yields, process ratios, multipliers, systematics ratios up and down
        RooDensityRatio(const char* name, const char* title, RooArgList& nps, RooArgList& eventYieldsN, RooArgList& procRatios, RooArgList& multipliers, RooArgList& gDown, RooArgList& gUp, RooAbsReal& expEvntSave);

        ///\brief the standard copy constructor from another RooDensityRatio
        RooDensityRatio(const RooDensityRatio& other, const char* name=nullptr);

        ///\brief the standard destructor
        ~RooDensityRatio();

        ///\brief inherited method from RooAbsPdf that must be overridden: clone class
        virtual TObject* clone(const char* newname) const override {return new RooDensityRatio(*this, newname);}

        ///\brief set the NPs in an argset
        void addNPs(RooArgList& nps);

        ///\brief set the arglist that has nominal event yields
        void addEventYieldsN(RooArgList& eventYieldsN);

        ///\brief set the arglist that has process ratios per event
        void addProcessRatios(RooArgList& procRatios); 

        ///\brief set the arglist that has the multipliers
        void addMultipliers(RooArgList& multipliers);

        ///\brief set the systematics multipiers up
        void addGUp(RooArgList& gUp);

        ///\brief set the systematics multipliers down
        void addGDown(RooArgList& gDown);

        ///\brief the function that sets the expected events value and stores it in a RooFormulaVar
        void setExpEventsVal(RooAbsReal& expEvntSave);

        ///\brief the function to change the interpolation code if necessary
        void setInterpCode(Int_t code);

        ///\brief override of the creation of expected events function to enable batch mode nll computation
        std::unique_ptr<RooAbsReal> createExpectedEventsFunc(const RooArgSet* nset) const;

        ///\brief the override of expectedEvents()
        double expectedEvents(const RooArgSet* nset) const override;

        ///\brief the override of extendMode() (can be extended or part of an extension)
        ExtendMode extendMode() const override {return CanBeExtended;}

        ///\brief the function to return the NPs passed into the pdf
        RooArgList GetNPs() {return (RooArgList)m_nps;}

        ///\brief the function to return the RooArgList of nominal eventYields
        RooArgList GetEventYieldsN() {return (RooArgList)m_eventYieldsN;}

        ///\brief the function to return the RooArgList of Process Ratios
        RooArgList GetProcRatios() {return (RooArgList)m_procRatios;}

        ///\brief the function to return the RooArgList of Multipliers
        RooArgList GetMultipliers() {return (RooArgList)m_multipliers;}

        ///\brief the function to return the systematics ratios up
        RooArgList GetGUp() {return (RooArgList)m_gUp;}

        ///\brief the function to return the systematics ratios down
        RooArgList GetGDown() {return (RooArgList)m_gDown;}

        ///\brief the function to return the flexible interp code for this class
        UInt_t GetInterpCode() {return m_code;}

        ///\brief the function to return the number of nuisance parameters passed into the class
        UInt_t GetNPCount() {return m_nNPs;}

    protected:

        ///\brief inherited method from RooAbsPdf that must be overridden
        Double_t evaluate() const override;

        ///\brief the override of getAnalyticalIntegral (not well defined for a density ratio)
        Int_t getAnalyticalIntegral(const RooArgSet& integSet, RooArgSet& anaIntSet) {(void)integSet; (void)anaIntSet; return 1;}

        ///\brief the override of analyticalIntegral (not well defined for a density ratio)
        double analyticalIntegral(Int_t code) {(void)code; return 1.0;}

        ///\brief the override of the selfNormalized() method (the RooDensityRatio is self-normalized)
        bool selfNormalized() const override {return true;}

        ///\brief the function to add a RooArgSet or RooArgList to a member arg set or arg list
        void addRooAbsCollection(RooAbsCollection& collection, RooAbsCollection& memberCollection);

        ///\brief the function that performs the flexibleInterp (copy from RooFit::Detail::MathFuncs)
        inline double flexibleInterpSingle(unsigned int code, double low, double high, double boundary, double nominal, double paramVal, double res) const;

        ///\brief the placeholder for the nps
        RooListProxy m_nps;

        ///\brief the arg list of event yields (scalars) for function evaluation - nominal
        RooListProxy m_eventYieldsN;

        ///\brief the arg list of density ratios for function evaluation
        RooListProxy m_procRatios;

        ///\brief the arg list of multipliers for process formulas
        RooListProxy m_multipliers;

        ///\brief the arg set of multipliers g for the nominal ratios 1 Std. Dev. up
        RooListProxy m_gUp;

        ///\brief the arg set of multipliers g for the nominal ratios 1 Std. Dev. down
        RooListProxy m_gDown;

        ///\brief the real proxy of the expected events formula variable
        RooRealProxy m_expEvntSave;

        ///\brief the code for flexible interp (5 is used for this analysis)
        UInt_t m_code{5};

        ///\brief the number of nuisance parameters passed into the class
        UInt_t m_nNPs{0};
 
    private:
        ClassDefOverride(RooDensityRatio, 1) // "Pdf" that performs the unbinned NSBI likelihood fit
}; 

#endif