#ifndef __ROONLLVARSTAR_H__
#define __ROONLLVARSTAR_H__

#include <RooAbsReal.h>
#include <RooAbsPdf.h>
#include <RooAbsData.h>
#include <RooAbsTestStatistic.h>
#include <RooNLLVar.h>
#include <RooArgSet.h>
#include <RooRealVar.h>
#include <RooRealMPFE.h>

#include <vector>

class RooRealMPFE;
typedef RooRealMPFE* pRooRealMPFE;


class RooNLLVarStar : public RooAbsReal 
{
    public:

        // Standatrd Constructor
        RooNLLVarStar() {};

        // Constructor with pdf, dataset as an AbsData, and RooAbsTestStatistic configuration
        RooNLLVarStar(const char* name, const char* title, RooAbsPdf* pdf, RooAbsData* data, bool extended, RooAbsTestStatistic::Configuration const& cfg = RooAbsTestStatistic::Configuration{});

        // Copy Object
        RooNLLVarStar(const RooNLLVarStar& other, const char* name=nullptr);

        // Clone Object
        TObject* clone(const char* newname) const override {return new RooNLLVarStar(*this, newname);}

        // Destructor
        ~RooNLLVarStar();

        // Set the weight squared boolean
        void applyWeightSquared(bool flag) override;

        // Get Val "Override"
        double getVal() const {return this->evaluate();}

        // Evaluate Partition "Override" (Unbinned-only)
        double evaluatePartition(std::size_t firstEvent, std::size_t lastEvent, std::size_t stepSize) const;

        // Compute Scalar "Override"
        RooNLLVar::ComputeResult computeScalar(std::size_t stepSize, std::size_t firstEvent, std::size_t lastEvent) const;

        // Compute Scalar Function "Override"
        RooNLLVar::ComputeResult computeScalarFunc(RooAbsPdf* pdf, RooAbsData* data, RooArgSet* normSet, bool weightSq, std::size_t stepSize, std::size_t firstEvent, std::size_t lastEvent, RooAbsPdf* offsetPdf = nullptr) const;

        // Define and retrieve the operation mode of the test statistic instance
        enum GOFOpMode {SimMaster, MPMaster, Slave};
        GOFOpMode operMode() const {return m_gofOpMode;}

    protected:

        // Set the owner of the data - may not be necessary for this object
        bool setDataSlave(RooAbsData& /*data*/, bool /*cloneData*/, bool /*ownNewDataAnyway*/);

        // The pointer to the data
        RooAbsData* m_data = nullptr;

        // The pointer to the pdf 
        RooAbsPdf* m_pdf = nullptr;

        // The boolean if the pdf is extended or not
        bool m_extended{false};

        // The boolean if weights squared get applied
        bool m_weightSq{false};

        // If this was the first evaluation, I think (likely wrong)
        mutable bool m_first{true};

        // An optional per-bin likelihood offset - I think useless for unbinned fits
        RooAbsPdf* m_offsetPdf = nullptr;

        // Pointer to set with observables used for normalization
        RooArgSet* m_normSet = nullptr;

        // Operation mode of test statistic instance
        GOFOpMode m_gofOpMode = Slave;

        // The number of CPUs to use in parallel calculation mode
        Int_t m_nCPU = 1;

        // Array of Parallel Execution front ends
        pRooRealMPFE* m_mpfeArray = nullptr;

        // Array of sub-contexts representing part of the combined test statistic (simultaneous mode data)
        std::vector<std::unique_ptr<RooAbsTestStatistic>> m_gofArray;  // This array declaration may be an issue when combining in a simpdf

        // Carry of Kahan Sum Evaluation in evaluatePartition
        mutable double m_evalCarry = 0.0;

        // The number of pdfs in the RooSimultaneous
        Int_t m_simCount = 1;

        // Offset as a Kahan Sum
        mutable ROOT::Math::KahanSum<double> m_offset{0.0};

        // Offset with Weights Squared applied
        ROOT::Math::KahanSum<double> m_offsetSaveW2{0.0}; 

        // Override of RooAbsReal to allow for object cloning and copying
        double evaluate() const override;

    private:
        ClassDefOverride(RooNLLVarStar, 1) // NLL Variable that does not copy data in its constructor for unbinned data (use caution when calling as a SimMaster)
};

#endif 