# RooDensityRatio Derived Class
## (And the Higgs Offshell Decay Combinations Workspace)

This codespace is currently a work in progress. It houses a class named RooDensityRatio, which is derived from RooAbsPdf, which implements in RooFit a per-event signal/background fit using Neural Simulation-Based Inference (NSBI) outputs. In addition, the fit is recreated using native RooFit methods. This code class is highly specific to the Offshell Higgs to ZZ to 4l search. The workspaces created by this codespace are specifically for that analysis. It is possible to extend this codespace (or to create a new one) that generalizes the creatiion of a probability model from NSBI outputs for any analysis - however, the ability to do so currently does not exist in this repository.

## Setting Up the Code in its Current State

A top-level `CMakeLists.topLevel.txt`, along with a "class-level" `CMakeLists.txt`, are added within this repository to compile the class and create a dictionary and shared library (thanks to Martina's help!). **When cloning this repo, it is advised to do the following to set up the workspace:**

1. Create your work area with `source/`, `run/`, and `build/` directories
2. Run `cd source/` then clone the repository here, in the `source/` directory
3. Run `ln -s higgs-offshell-decay-combinations/CMakeLists.topLevel.txt CMakeLists.txt` to move the top-level `CMakeLists.txt` into the `source/` directory (i.e. the top-level)
4. Run `cd ../build`. After running `asetup AnalysisBase ...`, run `cmake ../source && make && source x86../setup.sh`

I have been running scripts after setting up AnalysisBase 25.2.28 in my `build/` directory, which uses ROOT v6.32/02. Note that, with this version, `RooNLLVar` is a deprecated class; warnings will appear when compiling that it, as well as its base classes, will be removed from the next ROOT version.

If you wish to compile and run outside of the AnalysisBase build, then step 4 becomes:
4. Run `cd build`, then `cmake ../source/higgs-offshell-decay-combinations && make && make install && source x86../setup.sh`.

## Running the Code in its Current State

The most recent active executable as of writing (19/02/2025) is `create4lWorkspaceV2`, whose source file (`create4lWorkspaceV2.cpp`) housed in the `utils/` directory. It can be run on its own or with the shell script `create4lWorkspaceV2.sh`, housed within the `share/` directory. (The actual minimization and scan with this workspace should be carried out in a work area that has StatAnalysis installed for accuracy reasons related to the newest version of ROOT.)

`create4lWorkspaceV2.cpp` generates a per-event probability density ratio model from NSBI outputs using native RooFit methods of the HZZ->4l signal region and three control regions with real data and saves out the RooWorkspace. **It is highly recommended to use this executable to generate the HZZ->4l real data workspace due to its much faster fit times.**

After setting up the code and compiling, run the following command in your `run/` directory to see output:
`source ../source/higgs-offshell-decay-combinations/share/create4lWorkspaceV2.sh`. Alternatively, you can run the command on your own using the following syntax:

`create4lWorkspaceV2 <luminosity> <path to Real SR+CR data> <path to CR Yields data> <path to WS file>`

Note that datasets only exist for luminosities of 139.0 ifb or 140.1 ifb.

Another executable, `runOnData`, tests a basic implementation of the `RooDensityRatio` class. It performs a per-event, combined NSBI fit of the signal region and three control regions (which are binned parts) with real data, after which it performs a POI scan on the fit. Options to run Hesse and/or Minos uncertainties are included, too.

After setting up the code and compiling, run the following command in your `run/` directory to see output:
`source ../source/higgs-offshell-decay-combinations/share/runOnData.sh`. Alternatively, you can run the command on your own using the following syntax:

`runOnData <luminosity> <path to Real SR+CR data> <path to CR Yields data> <path to WS file> <path to scan graph file> data`

Note that datasets only exist for luminosities of 139.0 ifb or 140.1 ifb. 

If you run using the provided shell script, a logfile will be created that documents what the test script is doing automatically. It is assumed when running the shell script that it is run in the `run/` directory.

(*Note:* Skeleton files such as `testScript.py` along with their ATLAS-conventional workspace locations, have also been provided, but they may or may not ever be used.) 