#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooRandom.h"
#include "RooSimultaneous.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#define debug

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    const char* wsFileName = argv[1];

    std::cout << std::fixed;
    std::cout << std::setprecision(12);

    TFile* wsFile(TFile::Open(wsFileName));
    RooWorkspace* ws = static_cast<RooWorkspace*>(wsFile->FindObjectAny("combWS"));

    /***********************************************************************************/
    // Determine the number of entries and extenable PDFs in each region for NLL Shift //
    /***********************************************************************************/

    double pi = 3.14159265359;
    int numGOs4l = 127;

    double numEntries = ws->data("combData")->sumEntries();
    int numNPs = ws->set("ModelConfig_NuisParams")->getSize();
    int numGOs = ws->set("ModelConfig_GlobalObservables")->getSize();
    int numRegions = ws->cat("combCat")->size();

    std::cout << "Number of Entries: " << numEntries << std::endl;
    std::cout << "Number of NPs: " << numNPs << std::endl;
    std::cout << "Number of GOs: " << numGOs << std::endl;
    std::cout << "Number of Regions (i.e. Extendable PDFs): " << numRegions << std::endl;

    double nllShift = numEntries*std::log(numRegions) + (numGOs+numGOs4l)*std::log(std::sqrt(2*pi));
    std::cout << "Expected Shift between RooFit and JAX NLLs: " << nllShift << std::endl;


    /**********************************/
    // Set POI and NP values to Pulls //
    /**********************************/

    std::cout << std::endl;
    std::vector<double> poi_pulls = {1.05515586e+00, 1.0, 1.0, 1.11278701e+00,  8.51480180e-01,  8.91379268e-01,
                                1.04936778e+00,  9.23277464e-01,  7.50231664e-01,  8.93088395e-01, 1.07579694e+00};
    std::vector<std::string> poi_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2", "mu_3lep", "mu_3lep_1", "mu_3lep_2", "mu_Zjets", "mu_emu"};
    std::map<std::string, double> pullsPOIsMap;
    int kk=0;
    for (auto poiName : poi_list){
        pullsPOIsMap[poiName] = poi_pulls[kk];
        kk++;
    }
    for (const auto& [key, value] : pullsPOIsMap){
        std::cout << "Setting " << key << " to " << value << std::endl;
        ((RooRealVar*)ws->arg(key.c_str()))->setVal(value);
    }

    std::vector<double> np_pulls = {-2.29179944e-02, -3.98599784e-02,
        -1.69479630e-01, -1.19651979e-01,  1.33152839e-01,  8.62146319e-02,
        4.85883502e-02,  6.89084478e-02,  1.51341101e-01, -2.69191609e-02,
        -1.93222089e-02,  7.30546690e-02, -1.71481901e-01,  1.64799791e-01,
        -1.02626519e-01, -1.55396132e-02, -4.67613197e-02, -2.68400377e-02,
        -1.62117554e-01, -4.81442437e-02,  7.10303016e-02, -1.23164723e-01,
        -1.85357227e-01, -5.17553643e-02, -1.64671437e-01, -1.04531164e-01,
        1.97808804e-01,  3.18477831e-02, -7.77601672e-02,  1.54000641e-01,
        9.65441912e-02, -8.72307029e-02,  5.27325295e-02, -1.07953642e-01,
        -9.20219045e-02, -6.60482770e-02,  1.97201035e-02,  2.08019422e-01,
        -3.08886111e-02, -1.13994131e-02,  1.68634596e-02,  3.75317972e-02,
        7.32912549e-02, -2.87078663e-02, -7.26173126e-02,  1.30908228e-01,
        -8.38547116e-03, -2.74824259e-02,  2.56800655e-01, -2.11684851e-01,
        -6.40144883e-02, -3.99128194e-01, -6.95843623e-02, -1.44033394e-01,
        1.80928821e-01, -1.47806827e-01, -6.34380065e-01, -1.55865197e-01,
        -1.28674423e-01, -2.12304289e-02,  1.87258321e-01, -4.14088489e-01,
        -1.11358975e-01, -3.04937528e-01, -3.47302010e-01, -1.34545616e-01,
        -2.21724728e-01,  8.95447093e-02,  1.75076877e-02,  4.27454278e-01,
        -3.12102163e-01,  7.37164158e-02, -3.09708177e-02, -1.89043026e-01,
        -3.13460183e-02,  1.62105628e-02,  2.95826332e-02, -6.96113902e-03,
        1.16175891e-02,  8.93396870e-02,  9.13741171e-02,  4.11665180e-01,
        2.50575319e-01, -6.32898763e-01, -2.56197449e-01, -2.59203058e-01,
        2.54866327e-01, -3.21620536e-01,  6.19616732e-02,  1.04408968e-01,
        1.08911469e-01, -6.07725479e-02,  6.06156824e-01,  5.79681659e-02,
        4.37311255e-02,  6.69974831e-02, -6.06151896e-02, -1.99796204e-01,
        2.49182120e-01,  4.89910825e-02, -1.64173616e-02, -1.21286865e-01,
        -5.96760968e-02, -1.81530995e-02, -1.43048670e-01, -9.40512597e-02,
        -5.48192320e-02,  6.42037882e-02,  1.01906090e-01,  3.35978534e-01,
        8.83589312e-02, -2.11249126e-03,  2.07350567e-01,  4.26964289e-01,
        -3.75069356e-01,  9.91629078e-02,  3.28431587e-01,  6.16479939e-01,
        -2.85373720e-01, -1.72387313e-01, -7.05918463e-02, -1.81204234e-01,
        -9.01099720e-02, -4.40744815e-02, -5.02395715e-02,  1.02632563e-01,
        9.86604203e-02,  0.00000000e+00,  0.00000000e+00, -3.85502956e-03,
        -6.29536133e-03,  3.08907551e-02,  3.40160664e-02, -4.02614628e-02,
        -1.01164162e-02,  9.28103882e-03, -2.34489181e-03, -2.00261893e-04,
        5.40632839e-06,  1.12336296e-23,  1.12336296e-23,  1.12336296e-23,
        1.69352834e-02,  1.71129770e-03, -2.17818481e-03, -5.92623112e-05,
        1.96272919e-02, -5.74937101e-04,  1.09938720e-04,  1.48102649e-04,
        -1.41668833e-04, -4.22234560e-02,  1.15173761e-01, -1.66608195e-01,
        -1.67917946e-02,  1.92932344e-02,  5.95512098e-02, -8.40448722e-03,
        -4.30224233e-03, -5.39239545e-03,  5.18225294e-03, -4.69601072e-04,
        5.11703309e-03, -6.47641394e-05,  3.09096090e-02, -1.78387126e-01,
        5.66832080e-02,  2.16180770e-01,  7.76152098e-02,  6.32350484e-02,
        5.26300668e-02,  7.15170634e-02, -5.80053249e-02, -3.27983697e-02,
        -9.77668636e-03};
    std::vector<std::string> np_pulls_list = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2",
       "ATLAS_EG_SCALE_ALL", "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", "ATLAS_EL_EFF_ID_CorrUncertaintyNP9",
       "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS",
       "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", "ATLAS_JET_EffectiveNP_Modelling1",
       "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4",
       "ATLAS_JET_EffectiveNP_Statistical5", "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta",
       "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg",
       "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6",
       "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV",
       "ATLAS_JET_Pileup_PtTerm", "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape",
       "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape",
       "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm",
       "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_MUON_EFF_TrigStatUncertainty", "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", "ATLAS_FT_EFF_Eigen_B_1",
       "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8",
       "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", "ATLAS_FT_EFF_Eigen_Light_2",
       "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9",  "ATLAS_JET_JvtEfficiency",
       "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet","ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", "ATLAS_PS_VBF_RE",
       "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet",
       "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};
    std::map<std::string, double> pullsNPsMap;
    int ff=0;
    for (auto npName : np_pulls_list){
        pullsNPsMap[npName] = np_pulls[ff];
        ff++;
    }
    for (const auto& [key, value] : pullsNPsMap){
        std::cout << "Setting " << key << " to " << value << std::endl;
        ((RooRealVar*)ws->arg(key.c_str()))->setVal(value);
    }
    std::cout << std::endl;

    /****************************************/
    // Calculate the NLL and Compare to JAX //
    /****************************************/

    double roofitNLL = ws->pdf("combPdf")->createNLL(*(ws->data("combData")), 
                            RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
                            RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    std::cout << "RooFit NLL Val: " << 2*roofitNLL << std::endl;
    std::cout << "Shifted RooFit NLL: " << 2*(roofitNLL - nllShift) << std::endl;
    // double nll4lCR0 = ws->pdf("pdf_CR0_channel_4l")->createNLL(*(ws->data("combData")),
    //                         RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
    //                         RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    // std::cout << "nll4lCR0: " << 2*nll4lCR0 << std::endl;
    // double nll4lCR1 = ws->pdf("pdf_CR1_channel_4l")->createNLL(*(ws->data("combData")),
    //                         RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
    //                         RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    // std::cout << "nll4lCR1: " << 2*nll4lCR1 << std::endl;
    // double nll4lCR2 = ws->pdf("pdf_CR2_channel_4l")->createNLL(*(ws->data("combData")),
    //                         RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
    //                         RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    // std::cout << "nll4lCR2: " << 2*nll4lCR2 << std::endl;
    // double nll4lSR = ws->pdf("pdf_SR_channel_4l")->createNLL(*(ws->data("combData")),
    //                         RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
    //                         RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    // std::cout << "nll4lSR: " << 2*nll4lSR << std::endl;
    double nll2l2nuConstraint = ws->pdf("constraintPdf_channel_2l2nu")->createNLL(*(ws->data("combData")),
                            RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
                            RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    std::cout << "2l2nu Constraint NLL: " << nll2l2nuConstraint << std::endl;
    std::cout << "2*2l2nu Constraint NLL without GO Shift: " << 2*(nll2l2nuConstraint - numGOs*std::log(std::sqrt(2*pi))) << std::endl;
    double nll4lConstraint = ws->pdf("constraintPdf_channel_4l")->createNLL(*(ws->data("combData")),
                            RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
                            RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))))->getVal();
    std::cout << "4l Constraint NLL: " << nll4lConstraint << std::endl;
    std::cout << "2*4l Constraint NLL without GO Shift: " << 2*(nll4lConstraint - 127*std::log(std::sqrt(2*pi))) << std::endl;

    // std::cout << "4l NLL Contribution(?): " << 2*(nll4lCR0+nll4lCR1+nll4lCR2+nll4lSR+nll4lConstraint - 127*std::log(std::sqrt(2*pi))) << std::endl;

    std::cout << std::endl;
    std::cout << "Attempt at Minimization w/o Legacy Backend Eval:" << std::endl;
    RooMinimizer minim(*ws->pdf("combPdf")->createNLL(*(ws->data("combData")), 
                                            RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
                                            RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables")))));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
    minim.setEps(0.01);
    minim.optimizeConst(2);
    minim.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Now Attempting Minimization with Legacy Backend Evaluation:" << std::endl;
    RooMinimizer minim2(*ws->pdf("combPdf")->createNLL(*(ws->data("combData")), 
                                            RooFit::Constrain(*(ws->set("ModelConfig_NuisParams"))),
                                            RooFit::GlobalObservables(*(ws->set("ModelConfig_GlobalObservables"))), RooFit::EvalBackend("legacy")));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim2.setStrategy(0);
    minim2.setPrintLevel(1);
    minim2.setEps(0.01);
    minim2.optimizeConst(2);
    minim2.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    status = minim2.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;


    std::cout << "Done." << std::endl;
    return 0;
}