#include <TFile.h>
#include <TGraph.h>
#include <RooWorkspace.h>
#include <RooAbsPdf.h>
#include <RooRealVar.h>
#include <RooFit.h>
#include <iostream>

using namespace RooFit;

int main(int argc, const char* argv[]){

    (void)argc;
    const char* inputWSDirectory = argv[1];
    const char* graphFilename = argv[2];
 
    // Load each WS, compute NLL and scan point, and store in graph
    double muStep = 0.05;
    int numMus = 61;
    std::vector<std::string> muVals;
    std::vector<double> mus;
    for (int i=0; i<=numMus; i++){
        double muValueDouble = i*muStep;
        mus.push_back(muValueDouble);
        muVals.push_back(std::to_string(muValueDouble).replace(1,1,"_").substr(0,4));
    }

    int i=0;
    std::cout << "Making Scan Points..." << std::endl;
    std::vector<double> scanYpoints;
    for (auto muVal : muVals){
        TFile* wsMuValFile = TFile::Open((std::string(inputWSDirectory)+"/WS_HZZ_4l_12_11_2024_binnedAsimov_"+muVal+".root").c_str(), "read");
        RooWorkspace* wsMuVal = (RooWorkspace*)wsMuValFile->Get(std::string("WS_binnedAsimov_4l_mu_"+muVal).c_str());
        // Denominator
        wsMuVal->loadSnapshot("globalFitForThisMu");
        // wsMuVal->getSnapshot("globalFitForThisMu")->Print("v");
        // RooRealVar* mu = ((RooRealVar*)wsMuVal->arg("mu"));
        double nllDenom = 2*wsMuVal->pdf(std::string("pdf_model_mu_"+muVal).c_str())->createNLL(*wsMuVal->data(std::string("asimovData_mu_"+muVal).c_str()), 
                                        RooFit::Constrain(*(wsMuVal->set("nuisanceParameters"))), 
                                        RooFit::GlobalObservables(*(wsMuVal->set("globalObservables"))))->getVal();
        std::cout << "Global Fit NLL for mu = " << mus[i] << ": " << nllDenom << std::endl;
        // Numerator
        wsMuVal->loadSnapshot("scanPointForThisMu");
        // wsMuVal->getSnapshot("scanPointForThisMu")->Print("v");
        ((RooRealVar*)wsMuVal->arg("mu"))->setVal(mus[i]);
        double nllNumer = 2*wsMuVal->pdf(std::string("pdf_model_mu_"+muVal).c_str())->createNLL(*wsMuVal->data(std::string("asimovData_mu_"+muVal).c_str()), 
                                        RooFit::Constrain(*(wsMuVal->set("nuisanceParameters"))), 
                                        RooFit::GlobalObservables(*(wsMuVal->set("globalObservables"))))->getVal();
        RooRealVar* mu = ((RooRealVar*)wsMuVal->arg("mu"));
        std::cout << "Current mu value: " << mu->getVal() << std::endl;
        std::cout << "Scan Point NLL for mu = " << mus[i] << ": " << nllNumer << std::endl;
        wsMuValFile->Close();
        delete wsMuVal;

        double scanPoint = nllNumer - nllDenom;
        scanYpoints.push_back(scanPoint);
        std::cout << "mu = " << mus[i] << ", ScanPoint = " << scanPoint << std::endl;
        i++;
    }

    TGraph* poiScan = new TGraph();
    poiScan->SetName("nll_scan");
    poiScan->SetMarkerStyle(21);
    for (auto i=0lu; i<muVals.size(); i++){
        poiScan->AddPoint(mus[i], scanYpoints[i]);
    }

    // Save out graph
    std::cout << "Saving out graph now..." << std::endl;
    TFile* graphFile = new TFile(graphFilename, "recreate");
    poiScan->Write();
    graphFile->Close();

    std::cout << "Done." << std::endl;
    return 0;
}