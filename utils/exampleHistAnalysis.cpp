#include <TH1D.h>
#include <RooRealVar.h>
#include <RooAddPdf.h>
#include <RooArgList.h>
#include <RooArgSet.h>
#include <RooRealSumPdf.h>
#include <RooProduct.h>
#include <RooDataHist.h>
#include <RooHistPdf.h>
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooNLLVar.h>
#include <RooRealConstant.h>
#include <RooStats/HistFactory/FlexibleInterpVar.h>
#include <RooStats/HistFactory/PiecewiseInterpolation.h>
#include <iostream>

using namespace RooFit;

int main (int argc, const char* argv[]){

    // Deal with Arguments (there are none)
    (void)argc;
    (void)argv;

    // Build some fake histograms - signal, BG1, data for now, plus variations on signal and BG1 for 1 systematic
    TH1D* signal = new TH1D("signal", "signal", 3, 0., 100.);
    TH1D* signalUp = new TH1D("signalUp", "signalUp", 3, 0., 100.);
    TH1D* signalDown = new TH1D("signalDown", "signalDown", 3, 0., 100.);

    TH1D* background = new TH1D("background", "background", 3, 0., 100.);
    TH1D* backgroundUp = new TH1D("backgroundUp", "backgroundUp", 3, 0., 100.);
    TH1D* backgroundDown = new TH1D("backgroundDown", "backgroundDown", 3, 0., 100.);

    TH1D* data = new TH1D("data", "data", 3, 0., 100.);

    signal->SetBinContent(1, 4.1);
    signal->SetBinContent(2, 1.2);
    signal->SetBinContent(3, 2.5);

    signalUp->SetBinContent(1, 4.4);
    signalUp->SetBinContent(2, 1.3);
    signalUp->SetBinContent(3, 2.1);

    signalDown->SetBinContent(1, 4.0);
    signalDown->SetBinContent(2, 1.15);
    signalDown->SetBinContent(3, 2.5);


    background->SetBinContent(1, 6.5);
    background->SetBinContent(2, 0.3);
    background->SetBinContent(3, 10.1);

    backgroundUp->SetBinContent(1, 6.4);
    backgroundUp->SetBinContent(2, 0.4);
    backgroundUp->SetBinContent(3, 10.2);

    backgroundDown->SetBinContent(1, 6.0);
    backgroundDown->SetBinContent(2, 0.05);
    backgroundDown->SetBinContent(3, 12.1);

    data->SetBinContent(1, 10.2);
    data->SetBinContent(2, 1.7);
    data->SetBinContent(3, 13.0);


    // Build RooDataHists from the Signal and Background Histograms, Common Observable
    RooRealVar* obs = new RooRealVar("obs", "obs", 0.0, 0.0, 10.0);

    RooDataHist* signal_data = new RooDataHist("signal_data", "signal_data", RooArgList(*obs), (TH1*)signal);
    RooDataHist* signalUp_data = new RooDataHist("signalUp_data", "signalUp_data", RooArgList(*obs), (TH1*)signalUp);
    RooDataHist* signalDown_data = new RooDataHist("signalDown_data", "signalDown_data", RooArgList(*obs), (TH1*)signalDown);

    RooDataHist* background_data = new RooDataHist("background_data", "background_data", RooArgList(*obs), (TH1*)background);
    RooDataHist* backgroundUp_data = new RooDataHist("backgroundUp_data", "backgroundUp_data", RooArgList(*obs), (TH1*)backgroundUp);
    RooDataHist* backgroundDown_data = new RooDataHist("backgroundDown_data", "backgroundDown_data", RooArgList(*obs), (TH1*)backgroundDown);

    // Build a RooDataHist of the Data, too
    RooDataHist* data_data = new RooDataHist("data_data", "data_data", RooArgList(*obs), (TH1*)data);

    // Assign the values for the norms of the Signal and Histograms to Variables for Convenience
    RooRealVar* signal_norm = new RooRealVar("signal_norm", "signal_norm", signal_data->sumEntries());
    RooRealVar* signalUp_norm = new RooRealVar("signalUp_norm", "signalUp_norm", signalUp_data->sumEntries());
    RooRealVar* signalDown_norm = new RooRealVar("signalDown_norm", "signalDown_norm", signalDown_data->sumEntries());

    RooRealVar* background_norm = new RooRealVar("background_norm", "background_norm", background_data->sumEntries());
    RooRealVar* backgroundUp_norm = new RooRealVar("backgroundUp_norm", "backgroundUp_norm", backgroundUp_data->sumEntries());
    RooRealVar* backgroundDown_norm = new RooRealVar("backgroundDown_norm", "backgroundDown_norm", backgroundDown_data->sumEntries());

    // Create the RooHistPdfs from the signal and backgrounds - these are the shapes of the nominal and variations 
    RooHistPdf* signal_pdf = new RooHistPdf("signal_pdf", "signal_pdf", RooArgSet(*obs), *signal_data);
    RooHistPdf* signalUp_pdf = new RooHistPdf("signalUp_pdf", "signalUp_pdf", RooArgSet(*obs), *signalUp_data);
    RooHistPdf* signalDown_pdf = new RooHistPdf("signalDown_pdf", "signalDown_pdf", RooArgSet(*obs), *signalDown_data);

    RooHistPdf* background_pdf = new RooHistPdf("background_pdf", "background_pdf", RooArgSet(*obs), *background_data);
    RooHistPdf* backgroundUp_pdf = new RooHistPdf("backgroundUp_pdf", "backgroundUp_pdf", RooArgSet(*obs), *backgroundUp_data);
    RooHistPdf* backgroundDown_pdf = new RooHistPdf("backgroundDown_pdf", "backgroundDown_pdf", RooArgSet(*obs), *backgroundDown_data);

    // Now Fuse the Signal and BG nominals with their vars to form the Signal and BG models
    // Assign the NP
    RooRealVar* alpha = new RooRealVar("alpha", "alpha", 0., -5., 5.);
    // The Pdfs (the Shapes)
    PiecewiseInterpolation* signal_model = new PiecewiseInterpolation("signal_model", "signal_model",
                                *signal_pdf, RooArgList(*signalDown_pdf), RooArgList(*signalUp_pdf), RooArgList(*alpha));
    PiecewiseInterpolation* background_model = new PiecewiseInterpolation("background_model", "background_model",
                                *background_pdf, RooArgList(*backgroundDown_pdf), RooArgList(*backgroundUp_pdf), RooArgList(*alpha));
    // Fuse the Norms, too
    RooStats::HistFactory::FlexibleInterpVar* signal_norm_model = new RooStats::HistFactory::FlexibleInterpVar("signal_norm_model", "signal_norm_model",
                                RooArgList(*alpha), signal_norm->getVal(), {signalDown_norm->getVal()}, {signalUp_norm->getVal()}, {4});
    RooStats::HistFactory::FlexibleInterpVar* background_norm_model = new RooStats::HistFactory::FlexibleInterpVar("background_norm_model", "background_norm_model",
                                RooArgList(*alpha), background_norm->getVal(), {backgroundDown_norm->getVal()}, {backgroundUp_norm->getVal()}, {4});

    // Finally Fuse the Signal Model with the POI - This is the signal norm with the POI in there, too
    RooRealVar* mu = new RooRealVar("mu", "mu", 1.0, 0.0, 10.0);
    RooProduct* signal_norm_poi_model = new RooProduct("signal_norm_poi_model", "signal_norm_poi_model", RooArgList(*mu, *signal_norm_model));

    // Build the Sig+BG Model and the Constraint
    std::cout << "creating poisson_model..." << std::endl;
    RooRealSumPdf* poisson_model = new RooRealSumPdf("poisson_model", "poisson_model", RooArgList(*signal_model, *background_model), RooArgList(*signal_norm_poi_model, *background_norm_model), false);
    std::cout << "poisson_model created" << std::endl;
    
    RooRealVar* globalObs = new RooRealVar("globalObs", "globalObs", 0.0);
    RooGaussian* constraint = new RooGaussian("constraint", "constraint", *globalObs, *alpha, RooRealConstant::value(1.0));

    // Build the Final Model
    RooProdPdf* model = new RooProdPdf("model", "model", RooArgList(*poisson_model, *constraint));

    // Calculate an NLL
    RooNLLVar* test = (RooNLLVar*)model->createNLL(*data_data, RooFit::Verbose(true));
    std::cout << "test NLL Val: " << test->getVal() << std::endl;


    return 0;
}