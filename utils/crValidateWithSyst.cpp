#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataFilename = argv[2];
    const char *sysFilename = argv[3];


// #ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(12);
// #endif

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI = new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_EW", "mu_qqZZ_0", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., inf);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_EW = new RooRealVar("mu_EW", "mu_EW", 1.0, 0., inf);
    mu_EW->setConstant(true);
    RooRealVar* mu_qqZZ_0 = new RooRealVar("mu_qqZZ_0", "mu_qqZZ_0", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., inf);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_EW);
    pois.add(*mu_qqZZ_0);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ_0"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ_0"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ_0"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
#endif

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", 
    "gg_PDF_var", "EW_PDF_var", "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", 
    "HOEW_QCD_syst_2jet", "HOEW_syst", "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
    "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", 
    "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", 
    "H4l_Shower_UEPS_VBF_OffShell", "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", "JET_JER_EffectiveNP_7", 
    "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
    "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3", "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", 
    "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", "JET_EffectiveNP_Statistical1", 
    "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
    "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
    "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", 
    "JET_EtaIntercalibration_NonClosure_negEta", "JET_EtaIntercalibration_NonClosure_posEta", 
    "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
    "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", 
    "JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", 
    "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", "MUON_MS", "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
     "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", 
    "EL_EFF_ID_CorrUncertaintyNP14", "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", 
    "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", "EL_EFF_ID_CorrUncertaintyNP4", 
    "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
    "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", 
    "MUON_EFF_RECO_STAT", "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", 
    "JET_fJvtEfficiency", "PRW_DATASF"};

    // std::vector<int> badSysIndices = {1,3,5,6,21,30,42,43,44,45,52,56,69,70,71,72,73,74,123};
    // std::vector<std::string> systematicsList;
    // for (auto i=0lu; i<systematicsListFull.size(); i++){
    //     if (std::find(badSysIndices.begin(), badSysIndices.end(), i) == badSysIndices.end()){
    //         systematicsList.push_back(systematicsListFull[i]);
    //     }
    // }
#ifdef debug
    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;
#endif
    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    // std::cout << "nps.getSize(): " << nps.getSize() << std::endl;
    double npVal = 0.01;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");
    std::cout << "computed constraint pdf" << std::endl;
    RooRealVar* total_weight = new RooRealVar("total_weight", "total_weight", 0.0, -inf, inf);

    mu->setVal(1.0);
    TextToPoissonParams TTP(dataFilename, sysFilename, nps, processList, true);
#ifdef debug
    TTP.printCRDict();
    TTP.printNobsCRs();
    TTP.printVarYieldsDict();
#endif
    std::map<std::string, RooAddPdf*> theCRpdfs = TTP.generateCRpdfs(multipliers, ATLAS_LUMI, total_weight);
    std::map<std::string, RooRealVar*> theCRobservables = TTP.GetCRObservables();
    std::map<std::string, RooDataSet*> theCRdatasets = TTP.GetCRDatasets();

    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
    
    // Create NLLs with the CR Poissons
    RooNLLVar* test0 = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theCRdatasets.at("CR0"), RooFit::Verbose(true));
    // test0->Print("v");
    std::cout << "Just the CR0 NLL: " << test0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test1 = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theCRdatasets.at("CR1"), RooFit::Verbose(true));
    // test1->Print("v");
    std::cout << "Just the CR1 NLL: " << test1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test2 = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theCRdatasets.at("CR2"), RooFit::Verbose(true));
    // test2->Print("v");
    std::cout << "Just the CR2 NLL: " << test2->getVal() << std::endl;
    std::cout << std::endl;

    // Now Constrain them
    RooProdPdf* constrainedCR0 = new RooProdPdf("constrainedCR0", "constrainedCR0", RooArgList(*theCRpdfs.at("CR0"), *constraintPdf));
    RooProdPdf* constrainedCR1 = new RooProdPdf("constrainedCR1", "constrainedCR1", RooArgList(*theCRpdfs.at("CR1"), *constraintPdf));
    RooProdPdf* constrainedCR2 = new RooProdPdf("constrainedCR2", "constrainedCR2", RooArgList(*theCRpdfs.at("CR2"), *constraintPdf));

    // Make sure things still write to a workspace
    RooWorkspace* workspace = new RooWorkspace("combined");
    
    std::cout << "Importing CR0 Pdf..." << std::endl;
    workspace->import(*constrainedCR0, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR0 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR0", *constrainedCR0);

    std::cout << "Importing CR0 dataset..." << std::endl;
    workspace->import(*theCRdatasets.at("CR0"));
    std::cout << "CR0 dataset import successful" << std::endl;

    std::cout << "Importing CR1 Pdf..." << std::endl;
    workspace->import(*constrainedCR1, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR1 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR1", *constrainedCR1);

    std::cout << "Importing CR1 dataset..." << std::endl;
    workspace->import(*theCRdatasets.at("CR1"));
    std::cout << "CR1 dataset import successful" << std::endl;

    std::cout << "Importing CR2 Pdf..." << std::endl;
    workspace->import(*constrainedCR2, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR2 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR2", *constrainedCR2);

    std::cout << "Importing CR2 dataset..." << std::endl;
    workspace->import(*theCRdatasets.at("CR2"));
    std::cout << "CR2 dataset import successful" << std::endl;

    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("wsMC", "wsMC", workspace);
    wsMC->SetPdf("pdf_CR0");
    wsMC->SetPdf("pdf_CR1");
    wsMC->SetPdf("pdf_CR2");
    wsMC->SetObservables(RooArgSet(*theCRobservables.at("CR0"), *theCRobservables.at("CR1"), *theCRobservables.at("CR2")));
    wsMC->SetNuisanceParameters(RooArgSet(*ATLAS_LUMI, nps));
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());

    mu->setVal(0.93);
    mu_qqZZ_0->setVal(1.12);
    mu_qqZZ_1->setVal(0.86);
    mu_qqZZ_2->setVal(0.87);
    for (int i=89; i<nps.getSize(); i++){
        ((RooRealVar*)nps.at(i))->setVal(-0.02);
    }
    // nps.Print("v");

    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }

    // Find the Multipliers in the pdfs
    RooFormulaVar* mult_S_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_S"))->components().find("n_CR0_S"))->getParameter("f_s");
    RooFormulaVar* mult_S_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_S"))->components().find("n_CR1_S"))->getParameter("f_s");
    RooFormulaVar* mult_S_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_S"))->components().find("n_CR2_S"))->getParameter("f_s");

    RooFormulaVar* mult_SBI_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_SBI"))->components().find("n_CR0_SBI"))->getParameter("f_sbi");
    RooFormulaVar* mult_SBI_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_SBI"))->components().find("n_CR1_SBI"))->getParameter("f_sbi");
    RooFormulaVar* mult_SBI_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_SBI"))->components().find("n_CR2_SBI"))->getParameter("f_sbi");

    RooFormulaVar* mult_B_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_B"))->components().find("n_CR0_B"))->getParameter("f_b");
    RooFormulaVar* mult_B_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_B"))->components().find("n_CR1_B"))->getParameter("f_b");
    RooFormulaVar* mult_B_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_B"))->components().find("n_CR2_B"))->getParameter("f_b");

    RooFormulaVar* mult_EWB_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWB"))->components().find("n_CR0_EWB"))->getParameter("f_ewb");
    RooFormulaVar* mult_EWB_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWB"))->components().find("n_CR1_EWB"))->getParameter("f_ewb");
    RooFormulaVar* mult_EWB_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWB"))->components().find("n_CR2_EWB"))->getParameter("f_ewb");

    RooFormulaVar* mult_EWSBI_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWSBI"))->components().find("n_CR0_EWSBI"))->getParameter("f_ewsbi");
    RooFormulaVar* mult_EWSBI_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWSBI"))->components().find("n_CR1_EWSBI"))->getParameter("f_ewsbi");
    RooFormulaVar* mult_EWSBI_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWSBI"))->components().find("n_CR2_EWSBI"))->getParameter("f_ewsbi");

    RooFormulaVar* mult_EWSBI10_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWSBI10"))->components().find("n_CR0_EWSBI10"))->getParameter("f_ewsbi10");
    RooFormulaVar* mult_EWSBI10_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWSBI10"))->components().find("n_CR1_EWSBI10"))->getParameter("f_ewsbi10");
    RooFormulaVar* mult_EWSBI10_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWSBI10"))->components().find("n_CR2_EWSBI10"))->getParameter("f_ewsbi10");

    RooFormulaVar* mult_qqZZ_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_qqZZ"))->components().find("n_CR0_qqZZ"))->getParameter("f_qqZZ_0");
    RooFormulaVar* mult_qqZZ_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_qqZZ"))->components().find("n_CR1_qqZZ"))->getParameter("f_qqZZ_1");
    RooFormulaVar* mult_qqZZ_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_qqZZ"))->components().find("n_CR2_qqZZ"))->getParameter("f_qqZZ_2");

    RooFormulaVar* mult_ttV_CR0 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_ttV"))->components().find("n_CR0_ttV"))->getParameter("f_ttV");
    RooFormulaVar* mult_ttV_CR1 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_ttV"))->components().find("n_CR1_ttV"))->getParameter("f_ttV");
    RooFormulaVar* mult_ttV_CR2 = (RooFormulaVar*)((RooFormulaVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_ttV"))->components().find("n_CR2_ttV"))->getParameter("f_ttV");

    std::cout << std::endl;
    std::cout << "SBI Formulae: " << mult_SBI_CR0->getVal() << " " << mult_SBI_CR1->getVal() << " " << mult_SBI_CR2->getVal() << std::endl;    
    std::cout << "S Formulae: " << mult_S_CR0->getVal() << " " << mult_S_CR1->getVal() << " " << mult_S_CR2->getVal() << std::endl; 
    std::cout << "B Formulae: " << mult_B_CR0->getVal() << " " << mult_B_CR1->getVal() << " " << mult_B_CR2->getVal() << std::endl; 
    std::cout << "EWB Formulae: " << mult_EWB_CR0->getVal() << " " << mult_EWB_CR1->getVal() << " " << mult_EWB_CR2->getVal() << std::endl; 
    std::cout << "EWSBI Formulae: " << mult_EWSBI_CR0->getVal() << " " << mult_EWSBI_CR1->getVal() << " " << mult_EWSBI_CR2->getVal() << std::endl; 
    std::cout << "EWSBI10 Formulae: " << mult_EWSBI10_CR0->getVal() << " " << mult_EWSBI10_CR1->getVal() << " " << mult_EWSBI10_CR2->getVal() << std::endl; 
    std::cout << "ttV Formulae: " << mult_ttV_CR0->getVal() << " " << mult_ttV_CR1->getVal() << " " << mult_ttV_CR2->getVal() << std::endl; 
    std::cout << "qqZZ Formulae: " << mult_qqZZ_CR0->getVal() << " " << mult_qqZZ_CR1->getVal() << " " << mult_qqZZ_CR2->getVal() << std::endl; 
    std::cout << std::endl;

    // Try to find the interpolation factors in the pdfs
    RooStats::HistFactory::FlexibleInterpVar* interp_S_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_S"))->components().find("interpFactor_CR0_S");
    RooStats::HistFactory::FlexibleInterpVar* interp_S_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_S"))->components().find("interpFactor_CR1_S");
    RooStats::HistFactory::FlexibleInterpVar* interp_S_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_S"))->components().find("interpFactor_CR2_S");

    RooStats::HistFactory::FlexibleInterpVar* interp_SBI_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_SBI"))->components().find("interpFactor_CR0_SBI");
    RooStats::HistFactory::FlexibleInterpVar* interp_SBI_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_SBI"))->components().find("interpFactor_CR1_SBI");
    RooStats::HistFactory::FlexibleInterpVar* interp_SBI_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_SBI"))->components().find("interpFactor_CR2_SBI");

    RooStats::HistFactory::FlexibleInterpVar* interp_B_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_B"))->components().find("interpFactor_CR0_B");
    RooStats::HistFactory::FlexibleInterpVar* interp_B_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_B"))->components().find("interpFactor_CR1_B");
    RooStats::HistFactory::FlexibleInterpVar* interp_B_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_B"))->components().find("interpFactor_CR2_B");

    RooStats::HistFactory::FlexibleInterpVar* interp_EWB_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWB"))->components().find("interpFactor_CR0_EWB");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWB_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWB"))->components().find("interpFactor_CR1_EWB");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWB_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWB"))->components().find("interpFactor_CR2_EWB");

    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWSBI"))->components().find("interpFactor_CR0_EWSBI");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWSBI"))->components().find("interpFactor_CR1_EWSBI");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWSBI"))->components().find("interpFactor_CR2_EWSBI");

    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI10_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_EWSBI10"))->components().find("interpFactor_CR0_EWSBI10");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI10_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_EWSBI10"))->components().find("interpFactor_CR1_EWSBI10");
    RooStats::HistFactory::FlexibleInterpVar* interp_EWSBI10_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_EWSBI10"))->components().find("interpFactor_CR2_EWSBI10");
    
    RooStats::HistFactory::FlexibleInterpVar* interp_ttV_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_ttV"))->components().find("interpFactor_CR0_ttV");
    RooStats::HistFactory::FlexibleInterpVar* interp_ttV_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_ttV"))->components().find("interpFactor_CR1_ttV");
    RooStats::HistFactory::FlexibleInterpVar* interp_ttV_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_ttV"))->components().find("interpFactor_CR2_ttV");

    RooStats::HistFactory::FlexibleInterpVar* interp_qqZZ_CR0 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR0")->coefList().find("nu_CR0_qqZZ"))->components().find("interpFactor_CR0_qqZZ");
    RooStats::HistFactory::FlexibleInterpVar* interp_qqZZ_CR1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR1")->coefList().find("nu_CR1_qqZZ"))->components().find("interpFactor_CR1_qqZZ");
    RooStats::HistFactory::FlexibleInterpVar* interp_qqZZ_CR2 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)theCRpdfs.at("CR2")->coefList().find("nu_CR2_qqZZ"))->components().find("interpFactor_CR2_qqZZ");
    
    std::cout << std::endl;
    std::cout << "SBI Interp Factors: " << interp_SBI_CR0->getVal() << " " << interp_SBI_CR1->getVal() << " " << interp_SBI_CR2->getVal() << std::endl;    
    std::cout << "S Interp Factors: " << interp_S_CR0->getVal() << " " << interp_S_CR1->getVal() << " " << interp_S_CR2->getVal() << std::endl; 
    std::cout << "B Interp Factors: " << interp_B_CR0->getVal() << " " << interp_B_CR1->getVal() << " " << interp_B_CR2->getVal() << std::endl; 
    std::cout << "EWB Interp Factors: " << interp_EWB_CR0->getVal() << " " << interp_EWB_CR1->getVal() << " " << interp_EWB_CR2->getVal() << std::endl; 
    std::cout << "EWSBI Interp Factors: " << interp_EWSBI_CR0->getVal() << " " << interp_EWSBI_CR1->getVal() << " " << interp_EWSBI_CR2->getVal() << std::endl; 
    std::cout << "EWSBI10 Interp Factors: " << interp_EWSBI10_CR0->getVal() << " " << interp_EWSBI10_CR1->getVal() << " " << interp_EWSBI10_CR2->getVal() << std::endl; 
    std::cout << "ttV Interp Factors: " << interp_ttV_CR0->getVal() << " " << interp_ttV_CR1->getVal() << " " << interp_ttV_CR2->getVal() << std::endl; 
    std::cout << "qqZZ Interp Factors: " << interp_qqZZ_CR0->getVal() << " " << interp_qqZZ_CR1->getVal() << " " << interp_qqZZ_CR2->getVal() << std::endl; 
    std::cout << std::endl;

    

    // Compute NLLs with the set mu and alpha configuration
    RooNLLVar* constNLL = (RooNLLVar*)constraintPdf->createNLL(*theCRdatasets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint PDF NLL Val: " << constNLL->getVal() << std::endl;

    // Create NLLs with the CR Poissons
    RooNLLVar* nllConstrainedCR0 = (RooNLLVar*)constrainedCR0->createNLL(*theCRdatasets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    // nllConstrainedCR0->Print("v");
    std::cout << "nllConstrainedCR0->getVal(): " << nllConstrainedCR0->getVal() << std::endl;
    std::cout << "Constrained CR0 Val - Constraint Val: " << nllConstrainedCR0->getVal() - constNLL->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* nllConstrainedCR1 = (RooNLLVar*)constrainedCR1->createNLL(*theCRdatasets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    // nllConstrainedCR1->Print("v");
    std::cout << "nllConstrainedCR1->getVal(): " << nllConstrainedCR1->getVal() << std::endl;
    std::cout << "Constrained CR1 Val - Constraint Val: " << nllConstrainedCR1->getVal() - constNLL->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* nllConstrainedCR2 = (RooNLLVar*)constrainedCR2->createNLL(*theCRdatasets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    // nllConstrainedCR2->Print("v");
    std::cout << "nllConstrainedCR2->getVal(): " << nllConstrainedCR2->getVal() << std::endl;
    std::cout << "Constrained CR2 Val - Constraint Val: " << nllConstrainedCR2->getVal() - constNLL->getVal() << std::endl;
    std::cout << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}