#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "RooRandom.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug
#define Snapshot
#undef doMinimize
#undef doMinos
#undef doScan
#undef doHesse

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataPlusYieldsPlusCRNobsFilename = argv[2];
    const char *crYieldsVarsFilename = argv[3];
    const char *wsFilename = argv[4];
    const char *graphFilename = argv[5];
    const char *dataAsimov = argv[6];

#ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(12);
#endif

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0 - sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0)", RooArgList(pois["mu"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0+9.0*sqrt(@0)-10.0+sqrt(10.0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0-10.0*sqrt(@0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0)+sqrt(@0))", RooArgList(pois["mu"]));

    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
#endif

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};

#ifdef debug
    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;
#endif

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    std::cout << "Number of NPs: " << nps.getSize() << std::endl;
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Get the Control Region Pdfs
    bool runOnData = true;
    if (std::string(dataAsimov).compare("data")){
        runOnData = false;
        mu->setVal(1.0);
        mu_qqZZ->setVal(1.0);
        mu_qqZZ_1->setVal(1.0);
        mu_qqZZ_2->setVal(1.0);
        for (auto np : nps){
            ((RooRealVar*)np)->setVal(0.0);
        }
    }
    RooRealVar* total_weight = new RooRealVar("total_weight", "total_weight", -inf, inf);
    TextToPoissonParams crHandle(dataPlusYieldsPlusCRNobsFilename, crYieldsVarsFilename, nps, processList, runOnData);
    std::map<std::string, RooAddPdf*> theCRpdfs = crHandle.generateCRpdfs(multipliers, ATLAS_LUMI, total_weight);
    std::map<std::string, RooRealVar*> theCRobservables = crHandle.GetCRObservables();
    std::map<std::string, RooDataSet*> theDataSets = crHandle.GetCRDatasets();
    
#ifdef debug
    crHandle.printVarYieldsDict();
    theDataSets.at("CR0")->Print("v");
    std::cout << theDataSets.at("CR0")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR0")->sumEntries() << std::endl;
    theDataSets.at("CR1")->Print("v");
    std::cout << theDataSets.at("CR1")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR1")->sumEntries() << std::endl;
    theDataSets.at("CR2")->Print("v");
    std::cout << theDataSets.at("CR2")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR2")->sumEntries() << std::endl;

    RooNLLVar* test0 = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Verbose(true));
    // test0->Print("v");
    std::cout << "Just the CR0 NLL: " << test0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test1 = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Verbose(true));
    // test1->Print("v");
    std::cout << "Just the CR1 NLL: " << test1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test2 = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Verbose(true));
    // test2->Print("v");
    std::cout << "Just the CR2 NLL: " << test2->getVal() << std::endl;
    std::cout << std::endl;
#endif

    // Constrain the CR pdfs
    RooProdPdf* constrainedCR0 = new RooProdPdf("constrainedCR0", "constrainedCR0", RooArgList(*theCRpdfs.at("CR0"), *constraintPdf));
    RooProdPdf* constrainedCR1 = new RooProdPdf("constrainedCR1", "constrainedCR1", RooArgList(*theCRpdfs.at("CR1"), *constraintPdf));
    RooProdPdf* constrainedCR2 = new RooProdPdf("constrainedCR2", "constrainedCR2", RooArgList(*theCRpdfs.at("CR2"), *constraintPdf));

#ifdef debug
    RooNLLVar* someNLLjustConstrained0 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR0 DataSet: " << someNLLjustConstrained0->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained1 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR1 DataSet: " << someNLLjustConstrained1->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained2 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR2 DataSet: " << someNLLjustConstrained2->getVal() << std::endl;

    RooNLLVar* testCon0 = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR0 Constrained NLL: " << testCon0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon1 = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR1 Constrained NLL: " << testCon1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon2 = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR2 Constrained NLL: " << testCon2->getVal() << std::endl;
    std::cout << std::endl;
#endif

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"CR0", 0}, {"CR1", 1}, {"CR2", 2}});

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*constrainedCR0,*constrainedCR1,*constrainedCR2), *cat);
    RooArgSet observables;
    observables.add(*theCRobservables.at("CR0"));
    observables.add(*theCRobservables.at("CR1"));
    observables.add(*theCRobservables.at("CR2"));
    observables.add(*cat);

    std::string combDataName = "obsData_4l_offshell";
    if (std::string(dataAsimov).compare("data")){
        combDataName = "asimovData_4l_offshell";
    }
    RooDataSet* combinedData = new RooDataSet(combDataName.c_str(), combDataName.c_str(), observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));

#ifdef debug
    RooNLLVar* testCombined = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Combined NLL Value, no shift: " << testCombined->getVal() << std::endl;
    std::cout << "Constrained CR0 Expected Events: " << constrainedCR0->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR1 Expected Events: " << constrainedCR1->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR2 Expected Events: " << constrainedCR2->expectedEvents(&pois) << std::endl;
    std::cout << "Shifted Combined NLL Value: " << testCombined->getVal() - (constrainedCR0->expectedEvents(&pois) + constrainedCR1->expectedEvents(&pois) + constrainedCR2->expectedEvents(&pois))*std::log(4) << std::endl;

    double eeCR0 = constrainedCR0->expectedEvents(&pois);
    double eeCR1 = constrainedCR1->expectedEvents(&pois);
    double eeCR2 = constrainedCR2->expectedEvents(&pois);
    double combShift = (eeCR0 + eeCR1 + eeCR2)*std::log(4);
    double nllCR0 = testCon0->getVal();
    double nllCR1 = testCon1->getVal();
    double nllCR2 = testCon2->getVal();
    double nllSum = nllCR0 + nllCR1 + nllCR2;
    std::cout << "combShift: " << combShift << std::endl;
    std::cout << "nllSum: " << nllSum << std::endl;
    std::cout << "nllSum - combShift: " << nllSum - combShift << std::endl;
#endif 

    // Make sure things still write to a workspace
    RooWorkspace* workspace = new RooWorkspace("combined");
    
    std::cout << "Importing CR0 Pdf..." << std::endl;
    workspace->import(*constrainedCR0, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR0 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR0", *constrainedCR0);

    std::cout << "Importing CR0 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR0"));
    std::cout << "CR0 dataset import successful" << std::endl;

    std::cout << "Importing CR1 Pdf..." << std::endl;
    workspace->import(*constrainedCR1, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR1 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR1", *constrainedCR1);

    std::cout << "Importing CR1 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR1"));
    std::cout << "CR1 dataset import successful" << std::endl;

    std::cout << "Importing CR2 Pdf..." << std::endl;
    workspace->import(*constrainedCR2, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR2 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR2", *constrainedCR2);

    std::cout << "Importing CR2 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR2"));
    std::cout << "CR2 dataset import successful" << std::endl;

    std::cout << "Importing Constraint Pdf..." << std::endl;
    workspace->import(*constraintPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "Constraint Pdf import successful" << std::endl;
    workspace->defineSet("constraintPdf", *constraintPdf);

    std::cout << "Importing simPdf..." << std::endl;
    workspace->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "simPdf import successful" << std::endl;
    workspace->defineSet("simPdf",*simPdf);

    std::cout << "Importing Combined DataSet..." << std::endl;
    workspace->import(*combinedData);
    std::cout << "Combined DataSet import successful." << std::endl;
    
    // Define the ModelConfig for the Workspace
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("ModelConfig_4l_offshell", "ModelConfig_4l_offshell", workspace);
    wsMC->SetPdf("simPdf");
    wsMC->SetObservables(observables);
    wsMC->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());
    RooArgSet conditionalObservables("conditionalObservables");
    wsMC->SetConditionalObservables(conditionalObservables);
    workspace->import(*wsMC);

    RooMinimizer minim(*simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables())));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
    minim.setEps(0.01);
    minim.optimizeConst(2);
    minim.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    ATLAS_LUMI->setConstant(true);
    mu->setConstant(false);
    mu_qqZZ->setConstant(false);
    mu_qqZZ_1->setConstant(false);
    mu_qqZZ_2->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(false);
    }
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    RooFit::OwningPtr<RooFitResult> theSaveMigrad = minim.save("test", "test");
    const RooArgList& theSaveListMigrad = theSaveMigrad->floatParsFinal();
    theSaveListMigrad.Print("v");

    workspace->saveSnapshot("pulls_allSys", theSaveListMigrad, true);

#ifdef debug
    std::cout << "\nfinal workspace:\n" << std::endl;
    workspace->Print("v");
#endif
    std::cout << "PREPARE TO WRITE TO FILE "<< std::endl;
    workspace ->writeToFile(wsFilename);
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    double nll3CRs = simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
    std::cout << std::endl;
    std::cout << "NLL With these pulls: " << nll3CRs << std::endl;

    return 0;
}