#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug

void printVector(const std::vector<double> things){
    for (auto thing : things){
        std::cout << thing << ", ";
    }
    std::cout << std::endl;
}

inline double multiplyVector(const std::vector<double> things){
    double ret = 1.0;
    for (auto thing : things){
        ret *= thing;
    }
    return ret;
}

inline double sumVector(const std::vector<double> things){
    double ret = 0.0;
    for (auto thing : things){
        ret += thing;
    }
    return ret;
}

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *sysFilename = argv[2];

// #ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(18);
// #endif

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI = new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., inf);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_VBF = new RooRealVar("mu_VBF", "mu_VBF", 1.0, 0., inf);
    mu_VBF->setConstant(true);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., inf);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_VBF);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
#endif

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", 
    "gg_PDF_var", "EW_PDF_var", "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", 
    "HOEW_QCD_syst_2jet", "HOEW_syst", "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
    "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", 
    "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", 
    "H4l_Shower_UEPS_VBF_OffShell", "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", "JET_JER_EffectiveNP_7", 
    "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
    "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3", "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", 
    "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", "JET_EffectiveNP_Statistical1", 
    "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
    "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
    "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", 
    "JET_EtaIntercalibration_NonClosure_negEta", "JET_EtaIntercalibration_NonClosure_posEta", 
    "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
    "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", 
    "JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", 
    "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", "MUON_MS", "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
     "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", 
    "EL_EFF_ID_CorrUncertaintyNP14", "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", 
    "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", "EL_EFF_ID_CorrUncertaintyNP4", 
    "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
    "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", 
    "MUON_EFF_RECO_STAT", "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", 
    "JET_fJvtEfficiency", "PRW_DATASF"};

#ifdef debug
    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;
#endif

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    ((RooRealVar*)nps.find("alpha_H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape"))->setVal(0.0); // 0.2
    ((RooRealVar*)nps.find("alpha_HOEW_syst"))->setVal(0.0); // 0.1
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Read Per-Event Ratios and Weights, Event Yields, all for nominal and systematics
    ReadBinary binaryHandle(sysFilename, nps, processList);
    RooArgList eventYieldsNList = binaryHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = binaryHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gDown = binaryHandle.createGArgList("down");
    RooArgList gUp = binaryHandle.createGArgList("up");
    RooArgSet observables = binaryHandle.GetObservables();
    RooDataSet* testSetSR = binaryHandle.createRooDataSet("testSetSR", "testSetSR");
    // TTree* theTree = binaryHandle.GetRatiosWeightTTree();
    // std::cout << "The Tree of Ratios and the Weights:" << std::endl;

#ifdef debug
    std::vector<std::string> yieldNames = binaryHandle.GetEventYieldsNames();
    std::cout << "Event Yield Names: " << std::endl;
    for (auto name : yieldNames){
        std::cout << name << std::endl;
    }

    std::map<std::string, std::map<std::string, std::vector<double>>> yieldsMap = binaryHandle.GetEventYieldsMap();
    std::cout << std::endl;
    std::cout << std::endl;
    RooStats::HistFactory::FlexibleInterpVar* sMod = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_S"))->realComponents().find("mod_S");
    auto sUp = sMod->high();
    double sUpProduct = 1.0;
    std::cout << "S Up:" << std::endl;
    for (auto i=0lu; i<sUp.size(); i++){
        // std::cout << "Input Map Val, Var Vector Val for i = " << i << ": " << yieldsMap.at("up").at("S")[i] << "     " << sUp[i] << std::endl;
        std::cout << sUp[i] << std::endl;
        sUpProduct *= sUp[i];
    }
    std::cout << "Product of All S Up Values in array (equiv to alpha=1.0): " << sUpProduct << std::endl;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "S Down: " << std::endl;
    auto sDown = sMod->low();
    for (auto i=0lu; i<sDown.size(); i++){
        // std::cout << "Input Map Val, Var Vector Val for i = " << i << ": " << yieldsMap.at("down").at("S")[i] << "     " << sDown[i] << std::endl;
        std::cout << sDown[i] << std::endl;
    }

    double sNom = sMod->nominal();
    std::cout << "S Nominal Value: " << sNom << std::endl;

    sMod->printAllInterpCodes();
    const RooListProxy& sParamList = sMod->variables();
    for (int i=0; i<sParamList.getSize(); i++){
        ((RooRealVar*)sParamList.at(i))->Print();
    }

    testSetSR->Print("v");

    std::cout << "N Yields Values and their values: " << std::endl;
    for (auto n : eventYieldsNList){
        std::cout << n->GetName() << ": " << ((RooRealVar*)n)->getVal() << std::endl;
    }

    std::cout << "Nu Yields Values and their values: " << std::endl;
    for (auto nu : eventYieldsNuList){
        std::cout << nu->GetName() << ": " << ((RooRealVar*)nu)->getVal() << std::endl;
    }
#endif

    RooArgList procRatios(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);

    // Set the expected Events formula variable externally
    RooFormulaVar* expectedEventsSave = new RooFormulaVar("expectedEventsSave", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));

    RooDensityRatio* testPdf = new RooDensityRatio("testSR", "testSR", nps, eventYieldsNList, procRatios, multipliers, gDown, gUp, *expectedEventsSave);

#ifdef debug
    procRatios.Print("v");

    expectedEventsSave->Print("v");
    std::cout << "validateSystematics.cxx - expectedEventsSave->getVal(): " << expectedEventsSave->getVal() << std::endl;

    // testPdf->Print("v");
#endif

    // Must set the workspace for the ModelConfig to be created; Pdf must be a product between the RDR and the constraints
    RooProdPdf* theProd = new RooProdPdf("theProd", "theProd", RooArgList(*testPdf, *constraintPdf));
    RooWorkspace* ws = new RooWorkspace("testWS");
    ws->import(*testPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    ws->import(*testSetSR, RooFit::Silence());
    ws->import(*constraintPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    ws->import(*theProd, RooFit::RecycleConflictNodes(), RooFit::Silence());

    
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("wsMC", "wsMC", ws);
    wsMC->SetPdf("theProd");
    wsMC->SetObservables(observables);
    wsMC->SetNuisanceParameters(nps);
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());

    // Here are Jay's Pulls; Assign them to the POIs and NPs
    std::vector<std::string> jaysNPlist = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", "gg_PDF_var", "EW_PDF_var", 
            "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", "HOEW_QCD_syst_2jet", "HOEW_syst", 
            "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
            "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", "H4l_Shower_UEPS_VBF_OffShell", 
            "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11", "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", 
            "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3", "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", 
            "JET_JER_EffectiveNP_7", "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
            "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2", "JET_EffectiveNP_Mixed3", 
            "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", 
            "JET_EffectiveNP_Statistical1", "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
            "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
            "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", "JET_EtaIntercalibration_NonClosure_negEta", 
            "JET_EtaIntercalibration_NonClosure_posEta", "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
            "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", "JET_Pileup_OffsetMu", 
            "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", 
            "MUON_MS", "MUON_SAGITTA_RESBIAS", "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
            "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", "EL_EFF_ID_CorrUncertaintyNP14", 
            "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", 
            "EL_EFF_ID_CorrUncertaintyNP4", "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
            "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
            "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", "MUON_EFF_RECO_STAT", 
            "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", "JET_fJvtEfficiency", "PRW_DATASF"};
    std::vector<double> jaysPullsNPs = {-0.028277317083615246, -0.04057689010533464, 0.10369602885304745, 0.11066106781601417, -0.01070038669720914, -0.05759139700218771, -0.039368980796976906, 0.5389750637805085, -0.35797809100039923, 0.20192714938577377, 0.3119750126374231, -0.34065397302735523, 0.08902291167197891, -0.12265626665304194, -0.09637690460720441, -0.039706380932586105, -0.09290529968612696, -0.03373526893304729, -0.049685334887108606, -0.050419760428098634, -0.04376676978020435, 0.22378012646824283, 0.03879232998630511, 0.10630390150558454, 0.10758326132262591, -0.10897959543960042, -0.0028379359100459164, 0.11431385823033342, 0.11815194087567107, -0.14739965013029085, 0.2131143413627042, 0.5267357087866459, -0.46411726235064027, -0.32531435023980587, -0.10630155772201742, 0.07115133352005606, -0.13758632952538594, 0.015360317395978899, 0.06082579953899187, -0.04408293558549051, -0.19138766492421166, -0.04998098758399088, -0.06315685339836277, 0.25272849285388566, -0.0524907911301553, -0.46935315685699613, -0.10993653983666227, 0.0005480517066326902, -0.027944171055505323, 0.23626789670107323, -0.2991882518229628, -0.036770028863391396, -0.17627080691598332, -0.2051338696569478, -0.05143889369986522, -0.12036324644115359, 0.13613692361996083, 0.11139336643690637, 0.1974181069234188, -0.213246497746094, 0.1418124931083198, 0.046164060236882305, -0.15928099244585206, 0.005703217414649191, 0.028022178843108078, 0.015073922432917513, -0.2605904835707358, 0.07593053677512569, 0.009739070129780637, 0.11047471668654774, -0.1238551556982504, -0.05522619601358493, 0.08736245932540211, -0.2694307611782417, -0.08734056945760392, 0.2988457055106014, 0.22255049091117932, -0.10373253982659435, 0.0589675100652219, -0.18324467387631735, -0.022963698143251113, 0.12542490156363834, -0.09595508290568312, 0.01008181564580793, -0.052299910697367336, 0.0023535365823551466, -0.1428966470871945, -0.036892745852325395, 0.08386201945388114, 0.046127586103968665, 0.04902717670386127, 0.168580145738976, -0.02210869712145925, -0.030064319886695345, 0.04529172404444309, -0.18009054292510787, 0.135460661068939, 0.06599254978164655, 0.09024491707773363, -0.0792675310028688, 0.029778883434420044, -0.10196046636167728, -0.07091799955244524, -0.07057386812122683, 0.024698138674941413, 0.17801628648266543, -0.10758632942503409, -0.15534825109186184, -0.04007972536525407, -0.1296035966883202, -0.08943470544397047, 0.2062304815009452, 0.030195454621761614, -0.09351374542662536, 0.13286347620739783, -0.03730729501766865, -0.009331009163767445, -0.00016678167807109137, 0.01912980400614542, -0.01963682651102139, 0.08770610034598421, 0.10323520334183814, -0.0757368855199293, -0.006919896675076195, -0.014742474890948013, 0.006149342356215567, -0.09089024860922228};
    assert(jaysNPlist.size() == jaysPullsNPs.size());
    std::map<std::string, double> jayNPpullsMap;
    int i=0;
    for (auto npName : jaysNPlist){
        jayNPpullsMap[npName] = jaysPullsNPs[i];
        i++;
    }
    for (const auto& [key, value] : jayNPpullsMap){
        std::string alphaName = "alpha_"+key;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(value);
    }
    std::vector<double> jaysPullsPOIs = {0.8970120806742395, 1.1400534849538946, 0.8502735118360424, 0.8978423011657017};
    mu->setVal(jaysPullsPOIs[0]);
    mu_qqZZ->setVal(jaysPullsPOIs[1]);
    mu_qqZZ_1->setVal(jaysPullsPOIs[2]);
    mu_qqZZ_2->setVal(jaysPullsPOIs[3]);

    double testNPVal = 0.01;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(testNPVal);
        testNPVal += 0.01;
    }

    nps.Print("v");

    std::cout << "Interp Factors for the SR Yields: " << std::endl;
    RooStats::HistFactory::FlexibleInterpVar* sMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_S"))->realComponents().find("mod_S");
    RooStats::HistFactory::FlexibleInterpVar* sbiMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_SBI"))->realComponents().find("mod_SBI");
    RooStats::HistFactory::FlexibleInterpVar* bMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_B"))->realComponents().find("mod_B");
    RooStats::HistFactory::FlexibleInterpVar* ewbMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_EWB"))->realComponents().find("mod_EWB");
    RooStats::HistFactory::FlexibleInterpVar* ewsbiMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_EWSBI"))->realComponents().find("mod_EWSBI");
    RooStats::HistFactory::FlexibleInterpVar* ewsbi10Mod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_EWSBI10"))->realComponents().find("mod_EWSBI10");
    RooStats::HistFactory::FlexibleInterpVar* qqZZ0Mod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_qqZZ_0"))->realComponents().find("mod_qqZZ_0");
    RooStats::HistFactory::FlexibleInterpVar* qqZZ1Mod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_qqZZ_1"))->realComponents().find("mod_qqZZ_1");
    RooStats::HistFactory::FlexibleInterpVar* qqZZ2Mod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_qqZZ_2"))->realComponents().find("mod_qqZZ_2");
    RooStats::HistFactory::FlexibleInterpVar* ttVMod1 = (RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_ttV"))->realComponents().find("mod_ttV");
    std::cout << "S: " << sMod1->getVal() <<std::endl;
    std::cout << "SBI: " << sbiMod1->getVal() <<std::endl;
    std::cout << "B: " << bMod1->getVal() <<std::endl;
    std::cout << "EWB: " << ewbMod1->getVal() <<std::endl;
    std::cout << "EWSBI: " << ewsbiMod1->getVal() <<std::endl;
    std::cout << "EWSBI10: " << ewsbi10Mod1->getVal() <<std::endl;
    std::cout << "qqZZ_0: " << qqZZ0Mod1->getVal() <<std::endl;
    std::cout << "qqZZ_1: " << qqZZ1Mod1->getVal() <<std::endl;
    std::cout << "qqZZ_2: " << qqZZ2Mod1->getVal() <<std::endl;
    std::cout << "ttV: " << ttVMod1->getVal() <<std::endl;
    std::cout << std::endl;

    std::cout << "The Vectors that make above and their hand-calculated product: " << std::endl;
    const std::vector<double> sMod1Up = sMod1->high();
    std::cout << "S: ";
    printVector(sMod1Up);
    std::cout << "Hand Calculated S Product: " << multiplyVector(sMod1Up) << std::endl;
    std::cout << "Hand Calculated S Sum: " << sumVector(sMod1Up) << std::endl;
    double theSUpVal = ((RooProduct*)eventYieldsNuList.find("nu_SR_S"))->getVal();
    double theSNominalVal = ((RooRealVar*)((RooFormulaVar*)((RooProduct*)eventYieldsNuList.find("nu_SR_S"))->realComponents().find("nominal_yield_SR_S"))->dependents().at(0))->getVal();
    std::cout << "S Interpolated Value: " << theSUpVal << std::endl;
    std::cout << "S Nominal Value: " << theSNominalVal << std::endl;
    std::cout << "S Interp Factor from the division of above: " << theSUpVal/theSNominalVal << std::endl;
    const std::vector<double> sbiMod1Up = sbiMod1->high();
    std::cout << "SBI: ";
    printVector(sbiMod1Up);
    std::cout << "Hand Calculated SBI Product: " << multiplyVector(sbiMod1Up) << std::endl;
    const std::vector<double> bMod1Up = bMod1->high();
    std::cout << "B: ";
    printVector(bMod1Up);
    std::cout << "Hand Calculated B Product: " << multiplyVector(bMod1Up) << std::endl;
    const std::vector<double> ewbMod1Up = ewbMod1->high();
    std::cout << "EWB: ";
    printVector(ewbMod1Up);
    std::cout << "Hand Calculated EWB Product: " << multiplyVector(ewbMod1Up) << std::endl;
    const std::vector<double> ewsbiMod1Up = ewsbiMod1->high();
    std::cout << "EWSBI: ";
    printVector(ewsbiMod1Up);
    std::cout << "Hand Calculated EWSBI Product: " << multiplyVector(ewsbiMod1Up) << std::endl;
    const std::vector<double> ewsbi10Mod1Up = ewsbi10Mod1->high();
    std::cout << "EWSBI10: ";
    printVector(ewsbi10Mod1Up);
    std::cout << "Hand Calculated EWSBI10 Product: " << multiplyVector(ewsbi10Mod1Up) << std::endl;
    const std::vector<double> qqZZ0Mod1Up = qqZZ0Mod1->high();
    std::cout << "qqZZ_0: ";
    printVector(qqZZ0Mod1Up);
    std::cout << "Hand Calculated qqZZ_0 Product: " << multiplyVector(qqZZ0Mod1Up) << std::endl;
    const std::vector<double> qqZZ1Mod1Up = qqZZ1Mod1->high();
    std::cout << "qqZZ_1: ";
    printVector(qqZZ1Mod1Up);
    std::cout << "Hand Calculated qqZZ_1 Product: " << multiplyVector(qqZZ1Mod1Up) << std::endl;
    const std::vector<double> qqZZ2Mod1Up = qqZZ2Mod1->high();
    std::cout << "qqZZ_2: ";
    printVector(qqZZ2Mod1Up);
    std::cout << "Hand Calculated qqZZ_2 Product: " << multiplyVector(qqZZ2Mod1Up) << std::endl;
    const std::vector<double> ttVMod1Up = ttVMod1->high();
    std::cout << "ttV: ";
    printVector(ttVMod1Up);
    std::cout << "Hand Calculated ttV Product: " << multiplyVector(ttVMod1Up) << std::endl;

    
    RooNLLVar* someNLLjustSR = (RooNLLVar*)testPdf->createNLL(*testSetSR);

#ifdef debug
    someNLLjustSR->Print("v");
#endif

    std::cout << "Just the RooDensityRatio NLL with mu = " << mu->getVal() << ", all alphas set to " << ((RooRealVar*)nps.find("alpha_H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape"))->getVal() << ": " << std::endl;
    std::cout << 2*someNLLjustSR->getVal() << std::endl;
    std::cout << "NLL When I just call pdf->createNLL(): " << 2*testPdf->createNLL(*testSetSR)->getVal() << std::endl;

    // Hand-Calculate Extended Term
    double extTerm = 2*theProd->extendedTerm(*testSetSR, false, false);
    std::cout << "The Extended term with this set: " << extTerm << std::endl;
    std::cout << "RooDensityRatio expectedEvents(): " << theProd->expectedEvents(nullptr) << std::endl;
    double N = testSetSR->sumEntries();
    std::cout << "Sum of Weights (N): " << N << std::endl;
    std::cout << "What the Extended Term Should Be: " << 2*(theProd->expectedEvents(nullptr) - N * std::log(theProd->expectedEvents(nullptr))) << std::endl;
    std::cout << "NLL without the extended term: " << 2*(someNLLjustSR->getVal() - extTerm) << std::endl;

    for (auto np : nps){
        ((RooRealVar*)np)->setVal(-1.0);
    }
    nps.Print("v");

    std::cout << "Just the RooDensityRatio NLL with mu = " << mu->getVal() << ", all alphas set to " << ((RooRealVar*)nps.find("alpha_H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape"))->getVal() << ": " << std::endl;
    std::cout << 2*someNLLjustSR->getVal() << std::endl;
    std::cout << "NLL When I just call pdf->createNLL(): " << 2*testPdf->createNLL(*testSetSR)->getVal() << std::endl;

    // Hand-Calculate Extended Term
    extTerm = 2*theProd->extendedTerm(*testSetSR, false, false);
    std::cout << "The Extended term with this set: " << extTerm << std::endl;
    std::cout << "RooDensityRatio expectedEvents(): " << theProd->expectedEvents(nullptr) << std::endl;
    N = testSetSR->sumEntries();
    std::cout << "Sum of Weights (N): " << N << std::endl;
    std::cout << "What the Extended Term Should Be: " << 2*(theProd->expectedEvents(nullptr) - N * std::log(theProd->expectedEvents(nullptr))) << std::endl;
    std::cout << "NLL without the extended term: " << 2*(someNLLjustSR->getVal() - extTerm) << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}