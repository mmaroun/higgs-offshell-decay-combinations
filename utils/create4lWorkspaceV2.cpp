#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooAddition.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooWrapperPdf.h"
#include "RooRealSumPdf.h"
#include "RooFormulaVar.h"
#include "RooRatio.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "RooRandom.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooMsgService.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#define debug

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataPlusYieldsPlusCRNobsFilename = argv[2];
    const char *crYieldsVarsFilename = argv[3];
    const char *wsFilename = argv[4];

#ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(12);
#endif

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0 - sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0)", RooArgList(pois["mu"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0+9.0*sqrt(@0)-10.0+sqrt(10.0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0-10.0*sqrt(@0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0)+sqrt(@0))", RooArgList(pois["mu"]));

    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
#endif

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    // std::cout << "Number of NPs: " << nps.getSize() << std::endl;
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Set Up Everything for the Signal Region
    ReadBinary srHandle(dataPlusYieldsPlusCRNobsFilename, nps, processList);
    RooArgList eventYieldsNList = srHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = srHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gInterpsList = srHandle.createPiecewiseInterpsforGs();
    RooArgList gDown = srHandle.createGArgList("down");
    RooArgList gUp = srHandle.createGArgList("up");
    RooArgSet observables = srHandle.GetObservables();
    RooRealVar* total_weight = (RooRealVar*)observables.find("total_weight");
    observables.remove(*total_weight);
    RooDataSet* datasetSR = srHandle.createRooDataSet("obsDataSR", "obsDataSR");

    RooArgList procRatiosSR(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);

    // Create a dummy factor that is always equal to 1, depends on all observables so as not to recalculate normalization components of pdf
    // It's all the r_Xs, all the G*gs, and the total_weight
    RooArgList allObservables(RooRealConstant::value(0.));
    allObservables.add(observables);
    RooProduct* dummyProd = new RooProduct("dummyProd", "dummyProd", allObservables);
    // dummyProd->Print("v");
    RooAddition* dummyFactor = new RooAddition("dummyFactor", "dummyFactor", RooArgList(*dummyProd, RooRealConstant::value(1.0)));
    // dummyFactor->Print("v");
    std::cout << "Value of Dummy Factor (sanity check): " << dummyFactor->getVal() << std::endl;

    // Assemble the per-process pdf pieces
    RooArgList pdfPieces;
    RooArgList normPieces;
    const RooArgSet* test  = new RooArgSet();
    RooArgSet test1;
    RooArgSet test2;
    for (auto i=0lu; i<processList.size(); i++){
        std::string ratioName = "s_factor_"+processList[i];
        RooRatio* ratioPdf = new RooRatio(ratioName.c_str(), ratioName.c_str(), RooArgList(*gInterpsList.at(i)), RooArgList(*(RooStats::HistFactory::FlexibleInterpVar*)((RooProduct*)eventYieldsNuList.at(i))->realComponents().find(std::string("mod_"+processList[i]).c_str())));

        std::string prodName = "prod_process_"+processList[i];
        RooProduct* prodPdf = new RooProduct(prodName.c_str(), prodName.c_str(), RooArgList(*ratioPdf, *dummyFactor, *procRatiosSR.at(i)));
        int code = prodPdf->getAnalyticalIntegralWN(test1, test2, test, nullptr);
        double anaInt = prodPdf->analyticalIntegralWN(code, test, nullptr);
        std::cout << "Analytical Integral for " << prodPdf->GetName() << ", code " << code << ": " << anaInt << std::endl;

        std::string wrapperName = "pdf_process_"+processList[i];
        RooWrapperPdf* wrapPdf = new RooWrapperPdf(wrapperName.c_str(), wrapperName.c_str(), *prodPdf, true);
        pdfPieces.add(*wrapPdf);

        std::string factorName = "nfFactor_"+processList[i];
        RooProduct* factorProduct = new RooProduct(factorName.c_str(), factorName.c_str(), RooArgList(*eventYieldsNuList.at(i), *multipliers.at(i)));
        normPieces.add(*factorProduct);
    }
    RooAddPdf* modelNoConstraint = new RooAddPdf("pdfSRUnc", "pdfSRUnc", pdfPieces, normPieces, false);
    // RooMsgService::instance().Print();
    // RooMsgService::instance().getStream(1).addTopic(RooFit::Integration);
    // RooMsgService::instance().addStream(RooFit::DEBUG, RooFit::Topic(RooFit::Integration), RooFit::ClassName("RooProduct"));

    RooProdPdf* constrained_SR = new RooProdPdf("pdf_SR_4l_offshell", "pdf_SR_4l_offshell", RooArgList(*modelNoConstraint, *constraintPdf));
    double testNLL = 2*constrained_SR->createNLL(*datasetSR, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()), RooFit::EvalBackend("legacy"))->getVal();
    std::cout << std::endl;
    // RooMsgService::instance().Print();
    std::cout << std::endl;
    std::cout << "NLL for default params: " << testNLL << std::endl;

    bool runOnData = true;
    TextToPoissonParams crHandle(dataPlusYieldsPlusCRNobsFilename, crYieldsVarsFilename, nps, processList, runOnData);
    std::map<std::string, RooAddPdf*> theCRpdfs = crHandle.generateCRpdfs(multipliers, ATLAS_LUMI, total_weight);
    std::map<std::string, RooRealVar*> theCRobservables = crHandle.GetCRObservables();
    std::map<std::string, RooDataSet*> theDataSets = crHandle.GetCRDatasets();
    // Add the SR dataset to the Map of All Datasets
    theDataSets["SR"] = datasetSR; 

    // Constrain the CR pdfs
    RooProdPdf* constrainedCR0 = new RooProdPdf("constrainedCR0", "constrainedCR0", RooArgList(*theCRpdfs.at("CR0"), *constraintPdf));
    RooProdPdf* constrainedCR1 = new RooProdPdf("constrainedCR1", "constrainedCR1", RooArgList(*theCRpdfs.at("CR1"), *constraintPdf));
    RooProdPdf* constrainedCR2 = new RooProdPdf("constrainedCR2", "constrainedCR2", RooArgList(*theCRpdfs.at("CR2"), *constraintPdf));

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"CR0", 0}, {"CR1", 1}, {"CR2", 2}, {"SR", 3}});

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*constrainedCR0,*constrainedCR1,*constrainedCR2,*constrained_SR), *cat);
    observables.add(*theCRobservables.at("CR0"));
    observables.add(*theCRobservables.at("CR1"));
    observables.add(*theCRobservables.at("CR2"));
    observables.add(*cat);

    std::string combDataName = "obsData_4l_offshell";
    RooDataSet* combinedData = new RooDataSet(combDataName.c_str(), combDataName.c_str(), observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));

    // Save the workspace
    RooWorkspace* workspace = new RooWorkspace("combined");

    std::cout << "\n.... Importing pdf_SR ...." << std::endl;
    workspace->import(*constrained_SR, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "\n.... pdf_SR import successful ...." << std::endl;
    workspace->defineSet("pdf_SR", *constrained_SR);
    
    std::cout << "Importing CR0 Pdf..." << std::endl;
    workspace->import(*constrainedCR0, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR0 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR0", *constrainedCR0);

    std::cout << "Importing CR0 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR0"));
    std::cout << "CR0 dataset import successful" << std::endl;

    std::cout << "Importing CR1 Pdf..." << std::endl;
    workspace->import(*constrainedCR1, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR1 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR1", *constrainedCR1);

    std::cout << "Importing CR1 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR1"));
    std::cout << "CR1 dataset import successful" << std::endl;

    std::cout << "Importing CR2 Pdf..." << std::endl;
    workspace->import(*constrainedCR2, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR2 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR2", *constrainedCR2);

    std::cout << "Importing CR2 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR2"));
    std::cout << "CR2 dataset import successful" << std::endl;

    std::cout << "Importing Constraint Pdf..." << std::endl;
    workspace->import(*constraintPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "Constraint Pdf import successful" << std::endl;
    workspace->defineSet("constraintPdf", *constraintPdf);

    std::cout << "Importing simPdf..." << std::endl;
    workspace->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "simPdf import successful" << std::endl;
    workspace->defineSet("simPdf",*simPdf);

    workspace->defineSet("nuisanceParameters", nps);
    workspace->defineSet("globalObservables", SH.GetGlobalObservables());

    std::cout << "Importing Combined DataSet..." << std::endl;
    workspace->import(*combinedData);
    std::cout << "Combined DataSet import successful." << std::endl;
    
    // Define the ModelConfig for the Workspace
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("ModelConfig_4l_offshell", "ModelConfig_4l_offshell", workspace);
    wsMC->SetPdf("simPdf");
    wsMC->SetObservables(RooArgSet(*theCRobservables.at("CR0"),*theCRobservables.at("CR1"),*theCRobservables.at("CR2"), observables["r_S"], 
                             observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]));
    wsMC->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());
    RooArgSet conditionalObservables("conditionalObservables");
    wsMC->SetConditionalObservables(conditionalObservables);
    workspace->import(*wsMC);

    workspace->Print("v");

    std::cout << "PREPARE TO WRITE TO FILE "<< std::endl;
    workspace ->writeToFile(wsFilename);
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    /********************************************************/

    // Validate against a set of params
    std::vector<std::string> jaysNPlist = { "ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2",
       "ATLAS_EG_SCALE_ALL", "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", "ATLAS_EL_EFF_ID_CorrUncertaintyNP9",
       "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS",
       "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", "ATLAS_JET_EffectiveNP_Modelling1",
       "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4",
       "ATLAS_JET_EffectiveNP_Statistical5", "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta",
       "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg",
       "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6",
       "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV",
       "ATLAS_JET_Pileup_PtTerm", "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape",
       "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape",
       "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm",
       "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_MUON_EFF_TrigStatUncertainty", "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", "ATLAS_FT_EFF_Eigen_B_1",
       "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8",
       "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", "ATLAS_FT_EFF_Eigen_Light_2",
       "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9",  "ATLAS_JET_JvtEfficiency",
       "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet","ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", "ATLAS_PS_VBF_RE",
       "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet",
       "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};
    std::vector<double> jaysPullsNPs = {-2.29179944e-02, -3.98599784e-02,
        -1.69479630e-01, -1.19651979e-01,  1.33152839e-01,  8.62146319e-02,
        4.85883502e-02,  6.89084478e-02,  1.51341101e-01, -2.69191609e-02,
        -1.93222089e-02,  7.30546690e-02, -1.71481901e-01,  1.64799791e-01,
        -1.02626519e-01, -1.55396132e-02, -4.67613197e-02, -2.68400377e-02,
        -1.62117554e-01, -4.81442437e-02,  7.10303016e-02, -1.23164723e-01,
        -1.85357227e-01, -5.17553643e-02, -1.64671437e-01, -1.04531164e-01,
        1.97808804e-01,  3.18477831e-02, -7.77601672e-02,  1.54000641e-01,
        9.65441912e-02, -8.72307029e-02,  5.27325295e-02, -1.07953642e-01,
        -9.20219045e-02, -6.60482770e-02,  1.97201035e-02,  2.08019422e-01,
        -3.08886111e-02, -1.13994131e-02,  1.68634596e-02,  3.75317972e-02,
        7.32912549e-02, -2.87078663e-02, -7.26173126e-02,  1.30908228e-01,
        -8.38547116e-03, -2.74824259e-02,  2.56800655e-01, -2.11684851e-01,
        -6.40144883e-02, -3.99128194e-01, -6.95843623e-02, -1.44033394e-01,
        1.80928821e-01, -1.47806827e-01, -6.34380065e-01, -1.55865197e-01,
        -1.28674423e-01, -2.12304289e-02,  1.87258321e-01, -4.14088489e-01,
        -1.11358975e-01, -3.04937528e-01, -3.47302010e-01, -1.34545616e-01,
        -2.21724728e-01,  8.95447093e-02,  1.75076877e-02,  4.27454278e-01,
        -3.12102163e-01,  7.37164158e-02, -3.09708177e-02, -1.89043026e-01,
        -3.13460183e-02,  1.62105628e-02,  2.95826332e-02, -6.96113902e-03,
        1.16175891e-02,  8.93396870e-02,  9.13741171e-02,  4.11665180e-01,
        2.50575319e-01, -6.32898763e-01, -2.56197449e-01, -2.59203058e-01,
        2.54866327e-01, -3.21620536e-01,  6.19616732e-02,  1.04408968e-01,
        1.08911469e-01, -6.07725479e-02,  6.06156824e-01,  5.79681659e-02,
        4.37311255e-02,  6.69974831e-02, -6.06151896e-02, -1.99796204e-01,
        2.49182120e-01,  4.89910825e-02, -1.64173616e-02, -1.21286865e-01,
        -5.96760968e-02, -1.81530995e-02, -1.43048670e-01, -9.40512597e-02,
        -5.48192320e-02,  6.42037882e-02,  1.01906090e-01,  3.35978534e-01,
        8.83589312e-02, -2.11249126e-03,  2.07350567e-01,  4.26964289e-01,
        -3.75069356e-01,  9.91629078e-02,  3.28431587e-01,  6.16479939e-01,
        -2.85373720e-01, -1.72387313e-01, -7.05918463e-02, -1.81204234e-01,
        -9.01099720e-02, -4.40744815e-02, -5.02395715e-02,  1.02632563e-01,
        9.86604203e-02,  0.00000000e+00,  0.00000000e+00, -3.85502956e-03,
        -6.29536133e-03,  3.08907551e-02,  3.40160664e-02, -4.02614628e-02,
        -1.01164162e-02,  9.28103882e-03, -2.34489181e-03, -2.00261893e-04,
        5.40632839e-06,  1.12336296e-23,  1.12336296e-23,  1.12336296e-23,
        1.69352834e-02,  1.71129770e-03, -2.17818481e-03, -5.92623112e-05,
        1.96272919e-02, -5.74937101e-04,  1.09938720e-04,  1.48102649e-04,
        -1.41668833e-04, -4.22234560e-02,  1.15173761e-01, -1.66608195e-01,
        -1.67917946e-02,  1.92932344e-02,  5.95512098e-02, -8.40448722e-03,
        -4.30224233e-03, -5.39239545e-03,  5.18225294e-03, -4.69601072e-04,
        5.11703309e-03, -6.47641394e-05,  3.09096090e-02, -1.78387126e-01,
        5.66832080e-02,  2.16180770e-01,  7.76152098e-02,  6.32350484e-02,
        5.26300668e-02,  7.15170634e-02, -5.80053249e-02, -3.27983697e-02,
        -9.77668636e-03};
    assert(jaysNPlist.size() == jaysPullsNPs.size());
    std::map<std::string, double> jayNPpullsMap;
    int i=0;
    for (auto npName : jaysNPlist){
        jayNPpullsMap[npName] = jaysPullsNPs[i];
        i++;
    }
    for (auto npName : systematicsList){
        std::string alphaName = "alpha_"+npName;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(jayNPpullsMap.at(npName));
        // jaysPullsConstSum += jayNPpullsMap.at(npName)*jayNPpullsMap.at(npName);
    }
    std::vector<double> jaysPullsPOIs = {1.05515586e+00, 1.11278701e+00,  8.51480180e-01,  8.91379268e-01};
    mu->setVal(jaysPullsPOIs[0]);
    mu_qqZZ->setVal(jaysPullsPOIs[1]);
    mu_qqZZ_1->setVal(jaysPullsPOIs[2]);
    mu_qqZZ_2->setVal(jaysPullsPOIs[3]);

    double testValidationNLLVal = 2*simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()), RooFit::EvalBackend("legacy"))->getVal();
    double pi = 3.1415926585;
    double nllShift = 2*(combinedData->sumEntries()*std::log(cat->size()) + (nps.getSize())*std::log(std::sqrt(2*pi)));
    std::cout << "NLL for set params: " << testValidationNLLVal - nllShift << std::endl;

    double testValidationNLLValNoConstraint = 2*modelNoConstraint->createNLL(*datasetSR, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()), RooFit::EvalBackend("legacy"))->getVal();
    std::cout << "NLL for set params, unconstrained: " << testValidationNLLValNoConstraint << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}