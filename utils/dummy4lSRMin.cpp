#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "RooRandom.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug
#define Snapshot
#undef doMinimize
#undef doMinos
#undef doScan
#undef doHesse

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataPlusYieldsPlusCRNobsFilename = argv[2];
    const char *crYieldsVarsFilename = argv[3];
    const char *wsFilename = argv[4];
    const char *graphFilename = argv[5];
    const char *dataAsimov = argv[6];

#ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(12);
#endif

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0 - sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0)", RooArgList(pois["mu"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0+9.0*sqrt(@0)-10.0+sqrt(10.0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0-10.0*sqrt(@0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0)+sqrt(@0))", RooArgList(pois["mu"]));

    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }
#endif

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};

#ifdef debug
    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;
#endif

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    std::cout << "Number of NPs: " << nps.getSize() << std::endl;
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Set Up Everything for the Signal Region
    ReadBinary srHandle(dataPlusYieldsPlusCRNobsFilename, nps, processList);
    RooArgList eventYieldsNList = srHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = srHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gDown = srHandle.createGArgList("down");
    RooArgList gUp = srHandle.createGArgList("up");
    RooArgSet observables = srHandle.GetObservables();
    RooDataSet* dataSetSR = srHandle.createRooDataSet("testSetSR", "testSetSR");
    std::map<std::string, RooDataSet*> theDataSets;
    theDataSets["SR"] = dataSetSR;

#ifdef debug
    RooNLLVar* someNLLjustConstrainedSR = (RooNLLVar*)constraintPdf->createNLL(*dataSetSR, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with SR DataSet: " << someNLLjustConstrainedSR->getVal() << std::endl;
#endif

    RooArgList procRatiosSR(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);
    RooRealVar* total_weight = (RooRealVar*)observables.find("total_weight");

    // Set the expected Events formula variable externally
    RooFormulaVar* expectedEventsSaveSR = new RooFormulaVar("expectedEventsSaveSR", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));
#ifdef debug
    std::cout << "All Event Yields Nu for SR: " << std::endl;
    for (auto nu : eventYieldsNuList){
        std::cout << nu->GetName() << ": " << ((RooRealVar*)nu)->getVal() << std::endl;
    }

    std::cout << "N Yields Values and their values: " << std::endl;
    for (auto n : eventYieldsNList){
        std::cout << n->GetName() << ": " << ((RooRealVar*)n)->getVal() << std::endl;
    }

    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }

    std::cout << "SR Expected Events Save Value: " << expectedEventsSaveSR->getVal() << std::endl;
    double srSumEntries = dataSetSR->sumEntries();
    std::cout << "DataSet SR Sum Entries: " << srSumEntries << std::endl;
    std::cout << "SR Hand-Calculated Extended Term: " << expectedEventsSaveSR->getVal() - srSumEntries*std::log(expectedEventsSaveSR->getVal()) << std::endl;
#endif

    RooDensityRatio* pdf_SR = new RooDensityRatio("pdf_SR", "pdf_SR", nps, eventYieldsNList, procRatiosSR, multipliers, gDown, gUp, *expectedEventsSaveSR);
    RooProdPdf* constrained_SR = new RooProdPdf("constrained_SR", "constrained_SR", RooArgList(*pdf_SR, *constraintPdf));

#ifdef debug
    std::cout << "SR Extended Term: " << pdf_SR->extendedTerm(*dataSetSR, false, false) << std::endl;

    RooNLLVar* testNLLsr = (RooNLLVar*)pdf_SR->createNLL(*dataSetSR, RooFit::Verbose(true));
    std::cout << "NLL of Unconstrained SR: " << testNLLsr->getVal() << std::endl;
#endif

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"SR", 0}});

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*constrained_SR), *cat);
    observables.add(*cat);

    std::string combDataName = "obsData_4l_offshell";
    if (std::string(dataAsimov).compare("data")){
        combDataName = "asimovData_4l_offshell";
    }
    RooDataSet* combinedData = new RooDataSet(combDataName.c_str(), combDataName.c_str(), observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));

#ifdef debug
    RooNLLVar* testCombined = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Combined NLL Value, no shift: " << testCombined->getVal() << std::endl;
    std::cout << "Constrained SR Expected Events: " << constrained_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "Shifted Combined NLL Value: " << testCombined->getVal() - (constrained_SR->expectedEvents(nullptr))*std::log(1) << std::endl;

    double eeSR = constrained_SR->expectedEvents(nullptr);
    double combShift = (eeSR)*std::log(1);
    double nllSR = testNLLsr->getVal();
    double nllConst = someNLLjustConstrainedSR->getVal();
    double nllSum = nllSR + nllConst;
    std::cout << "combShift: " << combShift << std::endl;
    std::cout << "nllSum: " << nllSum << std::endl;
    std::cout << "nllSum - combShift: " << nllSum - combShift << std::endl;
#endif 

    // Make sure things still write to a workspace
    RooWorkspace* workspace = new RooWorkspace("combined");

    std::cout << "\n.... Importing pdf_SR ...." << std::endl;
    workspace->import(*constrained_SR, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "\n.... pdf_SR import successful ...." << std::endl;
    workspace->defineSet("pdf_SR", *constrained_SR);

    std::cout << "Importing Constraint Pdf..." << std::endl;
    workspace->import(*constraintPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "Constraint Pdf import successful" << std::endl;
    workspace->defineSet("constraintPdf", *constraintPdf);

    std::cout << "Importing simPdf..." << std::endl;
    workspace->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "simPdf import successful" << std::endl;
    workspace->defineSet("simPdf",*simPdf);

    std::cout << "Importing Combined DataSet..." << std::endl;
    workspace->import(*combinedData);
    std::cout << "Combined DataSet import successful." << std::endl;
    
    // Define the ModelConfig for the Workspace
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("ModelConfig_4l_offshell", "ModelConfig_4l_offshell", workspace);
    wsMC->SetPdf("simPdf");
    wsMC->SetObservables(observables);
    wsMC->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());
    RooArgSet conditionalObservables("conditionalObservables");
    wsMC->SetConditionalObservables(conditionalObservables);
    workspace->import(*wsMC);

    RooMinimizer minim(*simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables())));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
    minim.setEps(0.01);
    minim.optimizeConst(2);
    minim.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    ATLAS_LUMI->setConstant(true);
    mu->setConstant(false);
    mu_qqZZ->setConstant(false);
    mu_qqZZ_1->setConstant(false);
    mu_qqZZ_2->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(false);
    }
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    RooFit::OwningPtr<RooFitResult> theSaveMigrad = minim.save("test", "test");
    const RooArgList& theSaveListMigrad = theSaveMigrad->floatParsFinal();
    theSaveListMigrad.Print("v");

    workspace->saveSnapshot("pulls_allSys", theSaveListMigrad, true);

#ifdef debug
    std::cout << "\nfinal workspace:\n" << std::endl;
    workspace->Print("v");
#endif
    std::cout << "PREPARE TO WRITE TO FILE "<< std::endl;
    workspace ->writeToFile(wsFilename);
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    double nllFinalSR = simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
    std::cout << std::endl;
    std::cout << "NLL With these pulls: " << nllFinalSR << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}