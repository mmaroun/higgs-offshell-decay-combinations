#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "Math/MinimizerOptions.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

#undef debug

using namespace RooFit;

int main (int argc, char** argv){

    (void)argc;
    const char *rootFilename = argv[1];

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Load the Tree
    TFile* myRootFile(TFile::Open(rootFilename));
    if ((!myRootFile) || myRootFile->IsZombie()){
        delete myRootFile;
        throw std::runtime_error("ROOT File not found or is a zombie, exiting.");
    }
    TTree* paramWeightTree = myRootFile->Get<TTree>("nominal");
    if (!paramWeightTree) {
        throw std::runtime_error("Unable to retrieve params/weights tree; expected tree name 'nominal'");
    }

    const TObjArray* listOfBranches = paramWeightTree->GetListOfBranches();
    Long64_t nBranches = listOfBranches->GetEntries();
    RooArgSet testSet;
    for (int i=0ll; i<nBranches; i++){
        TBranch* b = paramWeightTree->GetBranch(listOfBranches->At(i)->GetName());
        testSet.add(*(new RooRealVar(b->GetName(), b->GetName(), -inf, inf)));
    }

    RooDataSet testDataSet("test", "test", testSet, RooFit::Import(*paramWeightTree), RooFit::WeightVar("weight_kFact", true));
    
    testDataSet.get(0);
    testDataSet.Print("v");
    std::cout << "Weight Var Value: " << testDataSet.weightVar()->getVal() << std::endl;

    return 0;
}