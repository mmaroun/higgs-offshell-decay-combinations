#include <TFile.h>
#include <TH1D.h>
#include <RooRealVar.h>
#include <RooAddPdf.h>
#include <RooArgList.h>
#include <RooArgSet.h>
#include <RooRealSumPdf.h>
#include <RooProduct.h>
#include <RooDataHist.h>
#include <RooHistPdf.h>
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooWorkspace.h>
#include <RooFormulaVar.h>
#include <RooStats/ModelConfig.h>
#include <RooMinimizer.h>
#include <RooFitResult.h>
#include <RooRandom.h>
#include <RooStats/HistFactory/FlexibleInterpVar.h>
#include <RooStats/HistFactory/PiecewiseInterpolation.h>
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include <iostream>

using namespace RooFit;

int main (int argc, const char* argv[]){

    (void)argc;
    std::string mu_string = argv[1];
    const char* filePrefix = argv[2];
    const char* wsFilename = argv[3];

    std::string mu_string2 = mu_string;
    double muVal = std::atof(mu_string2.replace(1,1,".").c_str());

    std::map<std::string, double> nominalDataNorms;
    std::map<std::string, std::map<std::string, std::vector<double>>> varDataNorms;

    std::map<std::string, RooHistPdf*> nominalHistPdfs;
    std::map<std::string, std::map<std::string, RooArgList>> varHistPdfs;

    RooRealVar* obs = new RooRealVar("obs_asimov", "obs_asimov", 0.0, 0.0, 1.0);

    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};

    std::vector<std::string> variations = {"up", "down"};

    std::cout << "Opening Histograms and Creating individual DataHists and HistPdfs..." << std::endl;
    TH1D* dataSM = new TH1D("data_SM", "data_SM", 200, 0., 1.);
    for (auto process : processList){

        std::string histFileName = std::string(filePrefix)+"_"+process+".root";
        TFile* histFile = TFile::Open(histFileName.c_str(), "read");
        std::string nomHistObjectName = "hist_mu_"+mu_string+"_"+process;
        TH1D* nomHist = (TH1D*)histFile->Get(nomHistObjectName.c_str());

        RooDataHist* nomDataHist = new RooDataHist(nomHistObjectName.c_str(), nomHistObjectName.c_str(), RooArgList(*obs), (TH1*)nomHist);
        if (!(process.compare("SBI") && process.compare("EWSBI") && process.compare("qqZZ_0") && process.compare("qqZZ_1") && process.compare("qqZZ_2") && process.compare("ttV"))){
            nomHist->Print("v");
            dataSM->Add(nomHist);
            std::cout << "SM histogram for " << process << " added for Eventual Asimov Dataset creation." << std::endl;
        }

        std::string nomHistPdfName = "pdf_"+nomHistObjectName;
        RooHistPdf* nomHistPdf = new RooHistPdf(nomHistPdfName.c_str(), nomHistPdfName.c_str(), RooArgSet(*obs), *nomDataHist);
        nominalHistPdfs[process] = nomHistPdf;

        std::string nomNormName = "norm_"+nomHistObjectName;
        RooRealVar* nomNorm = new RooRealVar(nomNormName.c_str(), nomNormName.c_str(), nomDataHist->sumEntries());
        nominalDataNorms[process] = nomNorm->getVal();

        for (auto var : variations){
            for (auto sysName : systematicsList){

                std::string varSysHistObjectName = "hist_mu_"+mu_string+"_"+process+"_"+sysName+"_"+var;
                TH1D* varHist = (TH1D*)histFile->Get(varSysHistObjectName.c_str());
                
                RooDataHist* varSysDataHist = new RooDataHist(varSysHistObjectName.c_str(), varSysHistObjectName.c_str(), RooArgList(*obs), (TH1*)varHist);
                std::string varHistPdfName = "pdf_"+varSysHistObjectName;
                RooHistPdf* varHistPdf = new RooHistPdf(varHistPdfName.c_str(), varHistPdfName.c_str(), RooArgSet(*obs), *varSysDataHist);
                varHistPdfs[process][var].add(*varHistPdf);

                std::string varNormName = "norm_"+varSysHistObjectName;
                RooRealVar* varNorm = new RooRealVar(varNormName.c_str(), varNormName.c_str(), varSysDataHist->sumEntries());
                varDataNorms[process][var].push_back(varNorm->getVal());
            }
        }
        histFile->Close();
    }
    std::cout << "...done with reading hists into data." << std::endl;

    // Make the dataset (a RooDataHist)
    std::cout << "Creating SM Dataset..." << std::endl;
    RooDataHist* asimovDataBinned = new RooDataHist(std::string("asimovData_mu_"+mu_string).c_str(), std::string("asimovData_mu_"+mu_string).c_str(), RooArgList(*obs), (TH1*)dataSM);
    std::cout << "...SM Dataset Created." << std::endl;

    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    std::cout << "Number of NPs: " << nps.getSize() << std::endl;
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    std::vector<int> interpCodes;
    for (int i=0; i<nps.getSize(); i++){
        interpCodes.push_back(4);
    }
    
    RooArgList pdfShapes;
    std::vector<RooStats::HistFactory::FlexibleInterpVar*> pdfNorms;
    std::cout << "Putting HistPdfs and Norms into RooStats objects..." << std::endl;
    for (auto process : processList){
        std::string pdfShapeName = "pdf_model_"+process+"_mu_"+mu_string;
        PiecewiseInterpolation* pdfShape = new PiecewiseInterpolation(pdfShapeName.c_str(), pdfShapeName.c_str(), *nominalHistPdfs.at(process), varHistPdfs.at(process).at("down"), varHistPdfs.at(process).at("up"), nps);
        pdfShapes.add(*pdfShape);

        std::string normVarName = "norm_model_"+process+"_mu_"+mu_string;
        RooStats::HistFactory::FlexibleInterpVar* normVar = new RooStats::HistFactory::FlexibleInterpVar(normVarName.c_str(), normVarName.c_str(), nps, nominalDataNorms.at(process), varDataNorms.at(process).at("down"), varDataNorms.at(process).at("up"), interpCodes);
        pdfNorms.push_back(normVar);
    }
    std::cout << "...done with creating RooStats objects." << std::endl;

    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);

    std::cout << "Making signal norms scalable by a POI... " << std::endl;
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0 - sqrt(@0)", RooArgList(*mu));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0)", RooArgList(*mu));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0)", RooArgList(*mu)); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0+9.0*sqrt(@0)-10.0+sqrt(10.0))", RooArgList(*mu));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0-10.0*sqrt(@0))", RooArgList(*mu));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0)+sqrt(@0))", RooArgList(*mu));

    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(*mu_qqZZ)); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(*mu_qqZZ, *mu_qqZZ_1)); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(*mu_qqZZ, *mu_qqZZ_1, *mu_qqZZ_2));

    RooProduct* s_norm_poi_model = new RooProduct(std::string("s_norm_poi_model_mu_"+mu_string).c_str(), std::string("s_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_S, *pdfNorms.at(0)));
    RooProduct* sbi_norm_poi_model = new RooProduct(std::string("sbi_norm_poi_model_mu_"+mu_string).c_str(), std::string("sbi_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_SBI, *pdfNorms.at(1)));
    RooProduct* b_norm_poi_model = new RooProduct(std::string("b_norm_poi_model_mu_"+mu_string).c_str(), std::string("b_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_B, *pdfNorms.at(2)));

    RooProduct* ewb_norm_poi_model = new RooProduct(std::string("ewb_norm_poi_model_mu_"+mu_string).c_str(), std::string("ewb_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_EWB, *pdfNorms.at(3)));
    RooProduct* ewsbi_norm_poi_model = new RooProduct(std::string("ewsbi_norm_poi_model_mu_"+mu_string).c_str(), std::string("ewsbi_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_EWSBI, *pdfNorms.at(4)));
    RooProduct* ewsbi10_norm_poi_model = new RooProduct(std::string("ewsbi10_norm_poi_model_mu_"+mu_string).c_str(), std::string("ewsbi10_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_EWSBI10, *pdfNorms.at(5)));

    RooProduct* qqZZ_norm_poi_model = new RooProduct(std::string("qqZZ_norm_poi_model_mu_"+mu_string).c_str(), std::string("qqZZ_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_qqZZ_0, *pdfNorms.at(6)));
    RooProduct* qqZZ_1_norm_poi_model = new RooProduct(std::string("qqZZ_1_norm_poi_model_mu_"+mu_string).c_str(), std::string("qqZZ_1_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_qqZZ_1, *pdfNorms.at(7)));
    RooProduct* qqZZ_2_norm_poi_model = new RooProduct(std::string("qqZZ_2_norm_poi_model_mu_"+mu_string).c_str(), std::string("qqZZ_1_norm_poi_model_mu_"+mu_string).c_str(), RooArgList(*f_qqZZ_2, *pdfNorms.at(8)));

    RooArgList pdf_norms_model(*s_norm_poi_model, *sbi_norm_poi_model, *b_norm_poi_model, *ewb_norm_poi_model, *ewsbi_norm_poi_model, *ewsbi10_norm_poi_model, 
                        *qqZZ_norm_poi_model, *qqZZ_1_norm_poi_model, *qqZZ_2_norm_poi_model, *pdfNorms.at(9));
    std::cout << "...done making scalable norms." << std::endl;

    RooRealSumPdf* poisson_model = new RooRealSumPdf(std::string("poisson_model_mu_"+mu_string).c_str(), std::string("poisson_model_mu_"+mu_string).c_str(), pdfShapes, pdf_norms_model, false);

    RooProdPdf* model = new RooProdPdf(std::string("pdf_model_mu_"+mu_string).c_str(), std::string("pdf_model_mu_"+mu_string).c_str(), RooArgList(*poisson_model, *constraintPdf));
    std::cout << "Model Created." << std::endl;

    // Make a WS and save the model, the data, the POI, the NPs, and the GOs to it, along with a ModelConfig
    RooWorkspace* ws = new RooWorkspace(std::string("WS_binnedAsimov_4l_mu_"+mu_string).c_str());
    ws->import(*model, RooFit::RecycleConflictNodes(), RooFit::Silence());
    ws->defineSet("pdf_model", *model);

    ws->defineSet("nuisanceParameters", nps);
    ws->defineSet("globalObservables", SH.GetGlobalObservables());

    ws->import(*asimovDataBinned);
    
    // Define the ModelConfig for the Workspace
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("ModelConfig", "ModelConfig", ws);
    wsMC->SetPdf(std::string("pdf_model_mu_"+mu_string).c_str());
    wsMC->SetObservables(RooArgSet(*obs));
    wsMC->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC->SetParametersOfInterest(*mu);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());
    RooArgSet conditionalObservables("conditionalObservables");
    wsMC->SetConditionalObservables(conditionalObservables);
    ws->import(*wsMC);

    // First find the global min, then compute the scan point
    // First Global Fit
    RooMinimizer minim(*model->createNLL(*asimovDataBinned, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables())));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
    minim.setEps(0.01);
    minim.optimizeConst(2);
    minim.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    mu->setConstant(false);
    mu_qqZZ->setConstant(false);
    mu_qqZZ_1->setConstant(false);
    mu_qqZZ_2->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(false);
    }
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    RooFit::OwningPtr<RooFitResult> theSaveMigrad = minim.save("globalFit", "globalFit");
    const RooArgList& theSaveListMigrad = theSaveMigrad->floatParsFinal();
    theSaveListMigrad.Print("v");

    ws->saveSnapshot("globalFitForThisMu", theSaveListMigrad, true);

    // Now the Scan Point
    mu->setVal(muVal);
    mu->setConstant(true);
    mu_qqZZ->setVal(1.0);
    mu_qqZZ_1->setVal(1.0);
    mu_qqZZ_2->setVal(1.0);
    mu_qqZZ->setConstant(false);
    mu_qqZZ_1->setConstant(false);
    mu_qqZZ_2->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(0.0);
        ((RooRealVar*)np)->setConstant(false);
    }
    status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    RooFit::OwningPtr<RooFitResult> theSaveMigrad1 = minim.save("scanPoint", "scanPoint");
    const RooArgList& theSaveListMigrad1 = theSaveMigrad1->floatParsFinal();
    theSaveListMigrad1.Print("v");

    ws->saveSnapshot("scanPointForThisMu", theSaveListMigrad1, true);

    std::cout << "PREPARE TO WRITE TO FILE "<< std::endl;
    ws->writeToFile(wsFilename);
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}