#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug

inline double computeGFactor(double npVal, double high, double low, double res){

    double mod = 1.0;
    if ((npVal < 1.0) && (npVal > -1.0)){
        double logHi = std::log(high);
        double logLo = std::log(low);
        double powUpLog = high <= 0.0 ? 0.0 : high * logHi;
        double powDownLog = low <= 0.0 ? 0.0 : -low * logLo;
        double powUpLog2 = high <= 0.0 ? 0.0 : powUpLog * logHi;
        double powDownLog2 = low <= 0.0 ? 0.0 : -powDownLog * logLo;
    
        double S0 = 0.5 * (high + low);
        double A0 = 0.5 * (high - low);
        double S1 = 0.5 * (powUpLog + powDownLog);
        double A1 = 0.5 * (powUpLog - powDownLog);
        double S2 = 0.5 * (powUpLog2 + powDownLog2);
        double A2 = 0.5 * (powUpLog2 - powDownLog2);

        double a = 1. / 8 * (15 * A0 - 7 * S1 + A2);
        double b = 1. / 8 * (-24 + 24 * S0 - 9 * A1 + S2);
        double c = 1. / 4 * (-5 * A0 + 5 * S1 - A2);
        double d = 1. / 4 * (12 - 12 * S0 + 7 * A1 - S2);
        double e = 1. / 8 * (+3 * A0 - 3 * S1 + A2);
        double f = 1. / 8 * (-8 + 8 * S0 - 5 * A1 + S2);

        mod = 1. + npVal * (a + npVal * (b + npVal * (c + npVal * (d + npVal * (e + npVal * f)))));
    }
    else if (npVal >= 1.0) {
        mod = std::pow(high, +npVal);
    } else {
        mod = std::pow(low, -npVal);
    } 
    return res * (mod - 1.0);
}

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    const char *inputFileName = argv[1];
    const char *outputTreeFile = argv[2];

    std::cout << std::fixed;
    std::cout << std::setprecision(12);

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", 140.1);

    RooArgSet pois;
    Double_t muValue = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muValue, 0., 10.);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Set up the NPs and Process List just to have
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_S", "@0 - sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_SBI", "sqrt(@0)", RooArgList(pois["mu"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_B", "1.0 - sqrt(@0)", RooArgList(pois["mu"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_EWB", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0+9.0*sqrt(@0)-10.0+sqrt(10.0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_EWSBI", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0-10.0*sqrt(@0))", RooArgList(pois["mu"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_EWSBI10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0)+sqrt(@0))", RooArgList(pois["mu"]));

    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};

    // Get the NPs
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }

    // Get All Yields info from Reference file
    ReadBinary inputHandle(inputFileName, nps, processList);
    std::map<std::string, std::map<std::string, std::vector<double>>> inputYieldMap = inputHandle.GetEventYieldsMap();
    RooArgList eventYieldsNList = inputHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = inputHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gDown = inputHandle.createGArgList("down");
    RooArgList gUp = inputHandle.createGArgList("up");

    RooRealVar* total_weight = (RooRealVar*)inputHandle.GetObservables().find("total_weight");

    RooFormulaVar* expectedEventsSave = new RooFormulaVar("expectedEventsSave", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));

    // RooDensityRatio* dr_Ref = new RooDensityRatio("dr_Ref", "dr_Ref", nps, eventYieldsNList, eventYieldsNuList, procRatiosSR, multipliers, gDown, gUp, *expectedEventsSave);

    // Load the TTree of Data
    TTree* inputTree = inputHandle.GetRatiosWeightTTree();
    // I need the branches of the r_X from these trees
    TBranch* r_S       = inputTree->GetBranch("r_S");
    TBranch* r_SBI     = inputTree->GetBranch("r_SBI");
    TBranch* r_B       = inputTree->GetBranch("r_B");
    TBranch* r_EWB     = inputTree->GetBranch("r_EWB");
    TBranch* r_EWSBI   = inputTree->GetBranch("r_EWSBI");
    TBranch* r_EWSBI10 = inputTree->GetBranch("r_EWSBI10");
    TBranch* r_qqZZ_0  = inputTree->GetBranch("r_qqZZ_0");
    TBranch* r_qqZZ_1  = inputTree->GetBranch("r_qqZZ_1");
    TBranch* r_qqZZ_2  = inputTree->GetBranch("r_qqZZ_2");
    TBranch* r_ttV     = inputTree->GetBranch("r_ttV");

    // Make the Sum - Asign the Proper Weight
    std::vector<double> inputRs(multipliers.getSize());
    r_S->SetAddress(&inputRs[0]);
    r_SBI->SetAddress(&inputRs[1]);
    r_B->SetAddress(&inputRs[2]);
    r_EWB->SetAddress(&inputRs[3]);
    r_EWSBI->SetAddress(&inputRs[4]);
    r_EWSBI10->SetAddress(&inputRs[5]);
    r_qqZZ_0->SetAddress(&inputRs[6]);
    r_qqZZ_1->SetAddress(&inputRs[7]);
    r_qqZZ_2->SetAddress(&inputRs[8]);
    r_ttV->SetAddress(&inputRs[9]);

    TFile* outputFile = new TFile(outputTreeFile, "recreate");
    TTree* llrRatios = new TTree("llr_oo", "llr_oo");

    double muStep = 0.05;
    int numMus = 61;
    std::vector<double> muVals;
    for (int i=0; i<=numMus; i++){
        muVals.push_back(i*muStep);
    }

    std::vector<double> llrBranchDoubles(muVals.size());
    std::vector<double> optScoreDoubles(muVals.size());
    for (auto i=0lu; i<muVals.size(); i++){
        std::string muString = std::to_string(muVals[i]).replace(1,1,"_").substr(0,4);
        std::string branchName = "llr_mu_"+muString;
        llrRatios->Branch(branchName.c_str(), &llrBranchDoubles[i], std::string(branchName+"/D").c_str());
    }

    // double upVal;
    // double downVal;

    std::cout << "Computing per-event learned likelihood ratios..." << std::endl;
    for (int i=0; i<inputTree->GetEntries(); i++){

        if (i % 50000 == 0){
            std::cout << "...processed " << i << " events... " << std::endl;
        }

        r_S->GetEntry(i);
        r_SBI->GetEntry(i);
        r_B->GetEntry(i);
        r_EWB->GetEntry(i);
        r_EWSBI->GetEntry(i);
        r_EWSBI10->GetEntry(i);
        r_qqZZ_0->GetEntry(i);
        r_qqZZ_1->GetEntry(i);
        r_qqZZ_2->GetEntry(i);
        r_ttV->GetEntry(i);   
#ifdef debug
        std::cout << "Event Index: " << i << std::endl;
#endif
        for (auto j=0lu; j<muVals.size(); j++){

            mu->setVal(muVals[j]);
#ifdef debug    
        std::cout << "Mu = " << mu->getVal() << std::endl;
#endif

            double numerator = 0.0;
            for (int k=0; k<multipliers.getSize(); k++){
                double gFactor = 1.0;
#ifdef debug
                std::cout << "Value of " << ((RooRealVar*)multipliers.at(k))->GetName() << ": " << ((RooRealVar*)multipliers.at(k))->getVal() << std::endl;
                std::cout << "Value of inputRs[k]: " << inputRs[k] << std::endl;
#endif
//                 if (!((std::abs(((RooRealVar*)multipliers.at(k))->getVal()) < 1e-15) || (inputRs[k] == 0))){
// #ifdef debug
//                     std::cout << "Entered sys Loop" << std::endl;
// #endif
//                     for (int n=0; n<nps.getSize(); n++){
//                         // Retrieve the proper var up and down here for the np, for the process
//                         std::string varUpName = "var_"+systematicsList[n]+"_ratio_"+processList[k]+"_up";
//                         std::string varDownName = "var_"+systematicsList[n]+"_ratio_"+processList[k]+"_down";
//                         TBranch* upVar = inputTree->GetBranch(varUpName.c_str());
//                         TBranch* downVar = inputTree->GetBranch(varDownName.c_str());
//                         upVar->SetAddress(&upVal);
//                         downVar->SetAddress(&downVal);
//                         upVar->GetEntry(i);
//                         downVar->GetEntry(i);
// #ifdef debug
//                         std::cout << systematicsList[n] <<  " has value " << ((RooRealVar*)nps.at(n))->getVal() << std::endl;
//                         std::cout << varUpName << " has value " << upVal << std::endl;
//                         std::cout << varDownName << " has value " << downVal << std::endl;
// #endif
//                         gFactor += computeGFactor(((RooRealVar*)nps.at(n))->getVal(), upVal, downVal, gFactor);
//                     }
//                 }
#ifdef debug
                
                std::cout << "Value of " << ((RooRealVar*)eventYieldsNList.at(k))->GetName() << ": " << ((RooRealVar*)eventYieldsNList.at(k))->getVal() << std::endl;
                std::cout << "Value of gFactor after sys loop: " << gFactor << std::endl;
#endif
                numerator += ((RooRealVar*)multipliers.at(k))->getVal() * ((RooRealVar*)eventYieldsNList.at(k))->getVal() * gFactor * inputRs[k];
            }

            llrBranchDoubles[j] = numerator/expectedEventsSave->getVal();
        }

        llrRatios->Fill();
    }
    std::cout << "...done with computation. Now saving and writing out..." << std::endl;

    llrRatios->Write();

    // Save the trees as friends to the original files still
    llrRatios->Print("v");
    outputFile->Close();

    std::cout << "Done." << std::endl;
    return 0;
}