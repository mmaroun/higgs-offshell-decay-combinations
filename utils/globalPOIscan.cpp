#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *sysFilename = argv[2];
    const char *graphFilename = argv[3];

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI = new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., inf);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_VBF = new RooRealVar("mu_VBF", "mu_VBF", 1.0, 0., inf);
    mu_VBF->setConstant(true);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., inf);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_VBF);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", 
    "gg_PDF_var", "EW_PDF_var", "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", 
    "HOEW_QCD_syst_2jet", "HOEW_syst", "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
    "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", 
    "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", 
    "H4l_Shower_UEPS_VBF_OffShell", "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", "JET_JER_EffectiveNP_7", 
    "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
    "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3", "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", 
    "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", "JET_EffectiveNP_Statistical1", 
    "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
    "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
    "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", 
    "JET_EtaIntercalibration_NonClosure_negEta", "JET_EtaIntercalibration_NonClosure_posEta", 
    "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
    "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", 
    "JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", 
    "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", "MUON_MS", "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
     "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", 
    "EL_EFF_ID_CorrUncertaintyNP14", "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", 
    "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", "EL_EFF_ID_CorrUncertaintyNP4", 
    "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
    "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", 
    "MUON_EFF_RECO_STAT", "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", 
    "JET_fJvtEfficiency", "PRW_DATASF"};

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Read Per-Event Ratios and Weights, Event Yields, all for nominal and systematics
    ReadBinary binaryHandle(sysFilename, nps, processList);
    RooArgList eventYieldsNList = binaryHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = binaryHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gDown = binaryHandle.createGArgList("down");
    RooArgList gUp = binaryHandle.createGArgList("up");
    RooArgSet observables = binaryHandle.GetObservables();
    RooDataSet* dataSetSR = binaryHandle.createRooDataSet("dataSetSR", "dataSetSR");

    RooArgList procRatios(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);

    // Set the expected Events formula variable externally
    RooFormulaVar* expectedEventsSave = new RooFormulaVar("expectedEventsSave", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));

    RooDensityRatio* pdf_SR = new RooDensityRatio("pdf_SR", "pdf_SR", nps, eventYieldsNList, procRatios, multipliers, gDown, gUp, *expectedEventsSave);

    // Must set the workspace for the ModelConfig to be created; Pdf must be a product between the RDR and the constraints
    RooProdPdf* constrained_SR = new RooProdPdf("constrained_SR", "constrained_SR", RooArgList(*pdf_SR, *constraintPdf));
    RooWorkspace* ws = new RooWorkspace("ws_SR");
    ws->import(*pdf_SR, RooFit::RecycleConflictNodes(), RooFit::Silence());
    ws->import(*dataSetSR, RooFit::Silence());
    ws->import(*constraintPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    ws->import(*constrained_SR, RooFit::RecycleConflictNodes(), RooFit::Silence());

    
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("wsMC", "wsMC", ws);
    wsMC->SetPdf("constrained_SR");
    wsMC->SetObservables(observables);
    wsMC->SetNuisanceParameters(nps);
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());

    mu->setVal(muVal);
    // RooNLLVar* nllSR = (RooNLLVar*)wsMC->GetPdf()->createNLL(*dataSetSR, RooFit::Verbose(true), RooFit::NumCPU(8,0));
    RooNLLVar* nllSR = (RooNLLVar*)constrained_SR->createNLL(*dataSetSR, RooFit::Verbose(true));
    std::cout << "nllSR value: " << nllSR->getVal() << std::endl;

    // Perform the scan
    RooMinimizer minim(*nllSR);
    minim.setStrategy(0);
    minim.setPrintLevel(1);

    ATLAS_LUMI->setConstant(true);
    mu->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(true);
    }
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    (void)status;
    double nll_min =  2*nllSR->getVal();
    
    TGraph* nll_scan = new TGraph();
    nll_scan->SetName("nll_scan");
    nll_scan->SetMarkerStyle(21);

    int numScanPoints = 25;
    float highestMuVal = 3.5;
    float divisor = numScanPoints/highestMuVal;
    for (int i=0; i<numScanPoints; i++){

        mu->setConstant(true);
        mu->setVal((float)i/divisor);

	    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
                                    ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
        (void)status;
	
        nll_scan->AddPoint(mu->getVal(), 2*nllSR->getVal()-nll_min);
    }

    TFile* fout = new TFile(graphFilename, "recreate");
    nll_scan->Write();
    fout->Close();

    std::cout << "Done." << std::endl;
    return 0;

}