#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <regex>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

#define debug

using namespace RooFit;

int main(int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *rootFilename = argv[2];
    const char *poisFilename = argv[3]; 
    const char *wsFilename   = argv[4];

    // Set Minuit as default minimizer
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the only NP so far
    RooRealVar* ATLAS_LUMI = new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_EW", "mu_qqZZ_0", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., inf);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_EW = new RooRealVar("mu_EW", "mu_EW", 1.0, 0., inf);
    mu_EW->setConstant(true);
    RooRealVar* mu_qqZZ_0 = new RooRealVar("mu_qqZZ_0", "mu_qqZZ_0", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., inf);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., inf);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_EW);
    pois.add(*mu_qqZZ_0);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};


    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", 
    "gg_PDF_var", "EW_PDF_var", "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", 
    "HOEW_QCD_syst_2jet", "HOEW_syst", "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
    "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", 
    "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", 
    "H4l_Shower_UEPS_VBF_OffShell", "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", "JET_JER_EffectiveNP_7", 
    "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
    "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3", "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", 
    "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", "JET_EffectiveNP_Statistical1", 
    "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
    "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
    "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", 
    "JET_EtaIntercalibration_NonClosure_negEta", "JET_EtaIntercalibration_NonClosure_posEta", 
    "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
    "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", 
    "JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", 
    "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", "MUON_MS", "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
     "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", 
    "EL_EFF_ID_CorrUncertaintyNP14", "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", 
    "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", "EL_EFF_ID_CorrUncertaintyNP4", 
    "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
    "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", 
    "MUON_EFF_RECO_STAT", "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", 
    "JET_fJvtEfficiency", "PRW_DATASF"}; 

    // Test Constraint PDF for shits and giggs - this has to be done per process - maybe in a map?
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Gather Info for SBI PDF
    ReadBinary binaryHandle(rootFilename, nps, processList);
    std::map<std::string, Double_t>* eventYields = binaryHandle.GetEventYieldsNamesAndValues();
    std::map<std::string, std::map<std::string, std::vector<double>>> allYieldsInfo = binaryHandle.GetEventYieldsMap();

#ifdef debug
    pois.Print("v");
    std::cout << "Event Yields" << std::endl;

    std::cout << std::fixed;
    std::cout << std::setprecision(15);
    std::cout << std::endl;
    std::cout << std::endl;
#endif
    
    TextToPoissonParams TTP(poisFilename, nullptr, nps, processList);
    std::map<std::string, std::map<std::string, Double_t>> crInfo = TTP.GetCRDict();

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_EW"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ_0"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ_0"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ_0"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

#ifdef debug
    std::cout << std::endl;
    std::cout << "f_S: " << f_S->getVal() << std::endl;
    std::cout << "f_SBI: " << f_SBI->getVal() << std::endl;
    std::cout << "f_B: " << f_B->getVal() << std::endl;
    std::cout << "f_EWB: " << f_EWB->getVal() << std::endl;
    std::cout << "f_EWSBI: " << f_EWSBI->getVal() << std::endl;
    std::cout << "f_EWSBI10: " << f_EWSBI10->getVal() << std::endl;
    std::cout << "f_qqZZ_0: " << f_qqZZ_0->getVal() << std::endl;
    std::cout << "f_qqZZ_1: " << f_qqZZ_1->getVal() << std::endl;
    std::cout << "f_qqZZ_2: " << f_qqZZ_2->getVal() << std::endl;
    std::cout << "f_ttV: " << f_ttV->getVal() << std::endl;
    std::cout << std::endl;
#endif

    // Calculate Expected Events for Each Process, store in Variables
    // CR0
    RooFormulaVar* n_CR0_S = new RooFormulaVar("n_CR0_S", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_S,RooRealConstant::value(crInfo["S"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_SBI = new RooFormulaVar("n_CR0_SBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_SBI,RooRealConstant::value(crInfo["SBI"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_B = new RooFormulaVar("n_CR0_B", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_B,RooRealConstant::value(crInfo["B"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_EWB = new RooFormulaVar("n_CR0_EWB", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWB,RooRealConstant::value(crInfo["EWB"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_EWSBI = new RooFormulaVar("n_CR0_EWSBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWSBI,RooRealConstant::value(crInfo["EWSBI"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_EWSBI10 = new RooFormulaVar("n_CR0_EWSBI10", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWSBI10,RooRealConstant::value(crInfo["EWSBI10"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_qqZZ = new RooFormulaVar("n_CR0_qqZZ", "@0*@1*@2/139.0",  RooArgList(*ATLAS_LUMI,*f_qqZZ_0,RooRealConstant::value(crInfo["qqZZ"]["n_jets_CR_bin0"])));
    RooFormulaVar* n_CR0_ttV = new RooFormulaVar("n_CR0_ttV", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_ttV,RooRealConstant::value(crInfo["ttV"]["n_jets_CR_bin0"])));

#ifdef debug
    std::cout << std::endl;
    std::cout << "n_CR0_S: " << n_CR0_S->getVal() << std::endl;
    std::cout << "n_CR0_SBI: " << n_CR0_SBI->getVal() << std::endl;
    std::cout << "n_CR0_B: " << n_CR0_B->getVal() << std::endl;
    std::cout << "n_CR0_EWB: " << n_CR0_EWB->getVal() << std::endl;
    std::cout << "n_CR0_EWSBI: " << n_CR0_EWSBI->getVal() << std::endl;
    std::cout << "n_CR0_EWSBI10: " << n_CR0_EWSBI10->getVal() << std::endl;
    std::cout << "n_CR0_qqZZ: " << n_CR0_qqZZ->getVal() << std::endl;
    std::cout << "n_CR0_ttV: " << n_CR0_ttV->getVal() << std::endl;
    std::cout << std::endl;
#endif

    // CR1
    RooFormulaVar* n_CR1_S = new RooFormulaVar("n_CR1_S", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_S,RooRealConstant::value(crInfo["S"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_SBI = new RooFormulaVar("n_CR1_SBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_SBI,RooRealConstant::value(crInfo["SBI"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_B = new RooFormulaVar("n_CR1_B", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_B,RooRealConstant::value(crInfo["B"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_EWB = new RooFormulaVar("n_CR1_EWB", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWB,RooRealConstant::value(crInfo["EWB"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_EWSBI = new RooFormulaVar("n_CR1_EWSBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWSBI,RooRealConstant::value(crInfo["EWSBI"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_EWSBI10 = new RooFormulaVar("n_CR1_EWSBI10", "@0*@1*@2/139.0",  RooArgList(*ATLAS_LUMI,*f_EWSBI10,RooRealConstant::value(crInfo["EWSBI10"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_qqZZ = new RooFormulaVar("n_CR1_qqZZ", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_qqZZ_1,RooRealConstant::value(crInfo["qqZZ"]["n_jets_CR_bin1"])));
    RooFormulaVar* n_CR1_ttV = new RooFormulaVar("n_CR1_ttV","@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_ttV,RooRealConstant::value(crInfo["ttV"]["n_jets_CR_bin1"])));


    // CR2
    RooFormulaVar* n_CR2_S = new RooFormulaVar("n_CR2_S", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_S,RooRealConstant::value(crInfo["S"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_SBI = new RooFormulaVar("n_CR2_SBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_SBI,RooRealConstant::value(crInfo["SBI"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_B = new RooFormulaVar("n_CR2_B", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_B,RooRealConstant::value(crInfo["B"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_EWB = new RooFormulaVar("n_CR2_EWB", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWB,RooRealConstant::value(crInfo["EWB"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_EWSBI = new RooFormulaVar("n_CR2_EWSBI", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_EWSBI,RooRealConstant::value(crInfo["EWSBI"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_EWSBI10 = new RooFormulaVar("n_CR2_EWSBI10", "@0*@1*@2/139.0",  RooArgList(*ATLAS_LUMI,*f_EWSBI10,RooRealConstant::value(crInfo["EWSBI10"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_qqZZ = new RooFormulaVar("n_CR2_qqZZ", "@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_qqZZ_2,RooRealConstant::value(crInfo["qqZZ"]["n_jets_CR_bin2"])));
    RooFormulaVar* n_CR2_ttV = new RooFormulaVar("n_CR2_ttV","@0*@1*@2/139.0", RooArgList(*ATLAS_LUMI,*f_ttV,RooRealConstant::value(crInfo["ttV"]["n_jets_CR_bin2"])));


    // SR
    /* Regarding above: The RooFormulaVars will be replaced by FlexibleInterpVars, where each will have a name like nu_SR_X, and 
    the parameter list will be all the alphas (the NPs), the nominal will be the eventYields->at("n_SR_X"), the 
    low will be a std::vector<double> of all the n_<SYS_NAME>_X_down, the high will be a std::vector<double> of all the 
    n_<SYS_NAME>_X_up, and the code will be a std::vector<int> of RooDensityRatio->GetInterpCode(). */

    std::vector<int> interpCodes; 
    for (int i=0; i<SH.GetNumberOfSystematics(); i++){
        interpCodes.push_back(4); // We use interpCode 4
    }

    // Check that yields are being calculated at 139.0 or 140.0 fb^(-1)
    // RooFormulaVar* n_SR_S = new RooFormulaVar("n_SR_S", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_S"]));
    // RooFormulaVar* n_SR_SBI = new RooFormulaVar("n_SR_SBI", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_SBI"]));
    // RooFormulaVar* n_SR_B = new RooFormulaVar("n_SR_B", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_B"]));
    // RooFormulaVar* n_SR_EWB = new RooFormulaVar("n_SR_EWB", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_EWB"]));
    // RooFormulaVar* n_SR_EWSBI = new RooFormulaVar("n_SR_EWSBI", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_EWSBI"]));
    // RooFormulaVar* n_SR_EWSBI10 = new RooFormulaVar("n_SR_EWSBI10", "@0*@1/139.0",  RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_EWSBI10"]));
    // RooFormulaVar* n_SR_qqZZ_0 = new RooFormulaVar("n_SR_qqZZ_0", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_qqZZ_0"]));
    // RooFormulaVar* n_SR_qqZZ_1 = new RooFormulaVar("n_SR_qqZZ_1", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_qqZZ_1"]));
    // RooFormulaVar* n_SR_qqZZ_2 = new RooFormulaVar("n_SR_qqZZ_2", "@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_qqZZ_2"]));
    // RooFormulaVar* n_SR_ttV = new RooFormulaVar("n_SR_ttV","@0*@1/139.0", RooArgList(*ATLAS_LUMI, (*eventYields)["n_SR_ttV"]));

    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_S = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_S", "nu_SR_S", nps, n_SR_S->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_SBI = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_SBI", "nu_SR_SBI", nps, n_SR_SBI->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_B = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_B", "nu_SR_B", nps, n_SR_B->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_EWB = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_EWB", "nu_SR_EWB", nps, n_SR_EWB->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_EWSBI = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_EWSBI", "nu_SR_EWSBI", nps, n_SR_EWSBI->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_EWSBI10 = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_EWSBI10", "nu_SR_EWSBI10", nps, n_SR_EWSBI10->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_qqZZ_0 = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_qqZZ_0", "nu_SR_qqZZ_0", nps, n_SR_qqZZ_0->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_qqZZ_1 = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_qqZZ_1", "nu_SR_qqZZ_1", nps, n_SR_qqZZ_1->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_qqZZ_2 = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_qqZZ_2", "nu_SR_qqZZ_2", nps, n_SR_qqZZ_2->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);
    // RooStats::HistFactory::FlexibleInterpVar* nu_SR_ttV = new RooStats::HistFactory::FlexibleInterpVar("nu_SR_ttV", "nu_SR_ttV", nps, n_SR_ttV->getVal(), n_SR_SYS_down, n_SR_SYS_up, interpCodes);

    // Probably Migrate to this code snippet at some point, maybe move it within ReadBinary if it gets reworked
    RooArgList eventYieldsList;
    for (auto processName : processList){
        std::string yieldName = "nu_SR_"+processName;
        eventYieldsList.add(*(new RooStats::HistFactory::FlexibleInterpVar(yieldName.c_str(), yieldName.c_str(), 
                            nps, allYieldsInfo.at("nominal").at(processName)[0]*ATLAS_LUMI->getVal()/139.0, 
                            allYieldsInfo.at("down").at(processName), allYieldsInfo.at("up").at(processName), interpCodes)));
    }
    eventYieldsList.Print("v");

    // RooArgList eventYieldsList(*n_SR_S, *n_SR_SBI, *n_SR_B, *n_SR_EWB, *n_SR_EWSBI, *n_SR_EWSBI10, *n_SR_qqZZ_0, *n_SR_qqZZ_1, *n_SR_qqZZ_2, *n_SR_ttV);
    // RooArgList eventYieldsList(*nu_SR_S, *nu_SR_SBI, *nu_SR_B, *nu_SR_EWB, *nu_SR_EWSBI, *nu_SR_EWSBI10, *nu_SR_qqZZ_0, *nu_SR_qqZZ_1, *nu_SR_qqZZ_2, *nu_SR_ttV);


    // Define Arg List of Ratios for NLL Calculation; get SR Dataset
    RooDataSet* dataSetSR = binaryHandle.createRooDataSet("srRatiosAndParams", "srRatiosAndParams");
    // RooArgSet observables = binaryHandle.GetObservables();

    // Must edit this for the actual ratios file (nominal + systematics) - Perhaps work into ReadBinary class
    const char* sysFilename = "foo.root"; // This will change when I have the actual name of the executable (it will be a cmd line arg)
    TFile* sysFile = TFile::Open(sysFilename, "read");
    TTree* tree = (TTree*)sysFile->Get("sys_var_per_event");
    RooArgSet observables;
    RooArgSet g_up;
    RooArgSet g_down;
    RooArgSet nominal;
    TObjArray* listOfRatiosAndWeights = tree->GetListOfBranches();
    TIter Itr(listOfRatiosAndWeights);
    std::vector<TBranch*> placeholderBranches;
    std::vector<double> branchEntryValues(listOfRatiosAndWeights->GetEntries(), 0.0);
    while (TObject* obj = Itr()){
        TBranch* br = (TBranch*)obj;
        placeholderBranches.push_back(br);
        observables.add(*(new RooRealVar(br->GetName(), br->GetName(), -100, 100)));
        if (std::regex_match(br->GetName(), std::regex("(r_)(.*)"))) {
            nominal.add(*(new RooRealVar(br->GetName(), br->GetName(), -100, 100)));
        } else if (std::regex_match(br->GetName(), std::regex("(.*)(_UP)"))) {
            g_up.add(*(new RooRealVar(br->GetName(), br->GetName(), -100, 100)));
        } else if (std::regex_match(br->GetName(), std::regex("(.*)(_DOWN)"))) {
            g_down.add(*(new RooRealVar(br->GetName(), br->GetName(), -100, 100)));
        }
    }
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooTreeDataStoreStar sysTreeStore("sys_var_ratios", "sys_var_ratios", tree, observables, nullptr);
    
    // Then there also comes the issue of converting the RooTreeDataStore/RooAbsDataStore to a RooAbsData*
    // Also ordering the observables RooArgSet to a RooArgList for all the branch names

#ifdef debug
    dataSetSR->Print("v");
    std::cout << "dataSetSR->sumEntries(): " << dataSetSR->sumEntries() << std::endl;
    std::cout << "dataSetSR->numEntries(): " << dataSetSR->numEntries() << std::endl;
    observables.Print("v");
#endif
    
    RooArgList procRatios(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);
    RooRealVar* total_weight = (RooRealVar*)observables.find("total_weight");

    // Put info into pdf_SR
    RooDensityRatio* pdf_SR = new RooDensityRatio("pdf_SR", "pdf_SR", nps, procRatios);

    // Initialize the Uniform Pdf for each process
    // CR0
    RooRealVar* obs_CR0 = new RooRealVar("obs_CR0", "obs_CR0", 0.0, 1.0);
    RooUniform* pdf_S_CR0 = new RooUniform("pdf_S_CR0", "pdf_S_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_SBI_CR0 = new RooUniform("pdf_SBI_CR0", "pdf_SBI_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_B_CR0 = new RooUniform("pdf_B_CR0", "pdf_B_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_EWB_CR0 = new RooUniform("pdf_EWB_CR0", "pdf_EWB_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_EWSBI_CR0 = new RooUniform("pdf_EWSBI_CR0", "pdf_EWSBI_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_EWSBI10_CR0 = new RooUniform("pdf_EWSBI10_CR0", "pdf_EWSBI10_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_qqZZ_CR0 = new RooUniform("pdf_qqZZ_CR0", "pdf_qqZZ_CR0", RooArgSet(*obs_CR0));
    RooUniform* pdf_ttV_CR0 = new RooUniform("pdf_ttV_CR0", "pdf_ttV_CR0", RooArgSet(*obs_CR0));

    // CR1
    RooRealVar* obs_CR1 = new RooRealVar("obs_CR1", "obs_CR1", 0.0, 1.0);
    RooUniform* pdf_S_CR1 = new RooUniform("pdf_S_CR1", "pdf_S_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_SBI_CR1 = new RooUniform("pdf_SBI_CR1", "pdf_SBI_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_B_CR1 = new RooUniform("pdf_B_CR1", "pdf_B_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_EWB_CR1 = new RooUniform("pdf_EWB_CR1", "pdf_EWB_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_EWSBI_CR1 = new RooUniform("pdf_EWSBI_CR1", "pdf_EWSBI_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_EWSBI10_CR1 = new RooUniform("pdf_EWSBI10_CR1", "pdf_EWSBI10_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_qqZZ_CR1 = new RooUniform("pdf_qqZZ_CR1", "pdf_qqZZ_CR1", RooArgSet(*obs_CR1));
    RooUniform* pdf_ttV_CR1 = new RooUniform("pdf_ttV_CR1", "pdf_ttV_CR1", RooArgSet(*obs_CR1));

    // CR2
    RooRealVar* obs_CR2 = new RooRealVar("obs_CR2", "obs_CR2", 0.0, 1.0);
    RooUniform* pdf_S_CR2 = new RooUniform("pdf_S_CR2", "pdf_S_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_SBI_CR2 = new RooUniform("pdf_SBI_CR2", "pdf_SBI_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_B_CR2 = new RooUniform("pdf_B_CR2", "pdf_B_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_EWB_CR2 = new RooUniform("pdf_EWB_CR2", "pdf_EWB_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_EWSBI_CR2 = new RooUniform("pdf_EWSBI_CR2", "pdf_EWSBI_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_EWSBI10_CR2 = new RooUniform("pdf_EWSBI10_CR2", "pdf_EWSBI10_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_qqZZ_CR2 = new RooUniform("pdf_qqZZ_CR2", "pdf_qqZZ_CR2", RooArgSet(*obs_CR2));
    RooUniform* pdf_ttV_CR2 = new RooUniform("pdf_ttV_CR2", "pdf_ttV_CR2", RooArgSet(*obs_CR2));

    // Create the RooAddPdfs for the CRs
    RooArgSet* argSetCR0 = new RooArgSet(*obs_CR0);
    RooArgSet* argSetCR1 = new RooArgSet(*obs_CR1);
    RooArgSet* argSetCR2 = new RooArgSet(*obs_CR2);
    RooAddPdf* pdf_CR0 = new RooAddPdf("pdf_CR0", "pdf_CR0", 
                                RooArgList(*pdf_S_CR0,*pdf_SBI_CR0,*pdf_B_CR0,*pdf_EWB_CR0,*pdf_EWSBI_CR0,*pdf_EWSBI10_CR0,*pdf_qqZZ_CR0,*pdf_ttV_CR0),
                                RooArgList(*n_CR0_S, *n_CR0_SBI, *n_CR0_B, *n_CR0_EWB, *n_CR0_EWSBI, *n_CR0_EWSBI10, *n_CR0_qqZZ, *n_CR0_ttV),
                                false);
    RooAddPdf* pdf_CR1 = new RooAddPdf("pdf_CR1", "pdf_CR1", 
                                RooArgList(*pdf_S_CR1,*pdf_SBI_CR1,*pdf_B_CR1,*pdf_EWB_CR1,*pdf_EWSBI_CR1,*pdf_EWSBI10_CR1,*pdf_qqZZ_CR1,*pdf_ttV_CR1),
                                RooArgList(*n_CR1_S, *n_CR1_SBI, *n_CR1_B, *n_CR1_EWB, *n_CR1_EWSBI, *n_CR1_EWSBI10, *n_CR1_qqZZ, *n_CR1_ttV),
                                false);
    RooAddPdf* pdf_CR2 = new RooAddPdf("pdf_CR2", "pdf_CR2", 
                                RooArgList(*pdf_S_CR2,*pdf_SBI_CR2,*pdf_B_CR2,*pdf_EWB_CR2,*pdf_EWSBI_CR2,*pdf_EWSBI10_CR2,*pdf_qqZZ_CR2,*pdf_ttV_CR2),
                                RooArgList(*n_CR2_S, *n_CR2_SBI, *n_CR2_B, *n_CR2_EWB, *n_CR2_EWSBI, *n_CR2_EWSBI10, *n_CR2_qqZZ, *n_CR2_ttV),
                                false);

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"CR0", 0}, {"CR1", 1}, {"CR2", 2}, {"SR", 3}});
    mu->setVal(1.0); // Value used for Asimov Dataset
    std::map<std::string, RooDataSet*> theDataSets;
    
    theDataSets["CR0"] = new RooDataSet("data_CR0", "data_CR0", RooArgSet(*obs_CR0, *total_weight), RooFit::WeightVar(*total_weight));
    theDataSets["CR0"]->add(RooArgSet(*obs_CR0, *total_weight), pdf_CR0->expectedEvents(argSetCR0), 0.0);
    theDataSets["CR1"] = new RooDataSet("data_CR1", "data_CR1", RooArgSet(*obs_CR1, *total_weight), RooFit::WeightVar(*total_weight));
    theDataSets["CR1"]->add(RooArgSet(*obs_CR1, *total_weight), pdf_CR1->expectedEvents(argSetCR1), 0.0);
    theDataSets["CR2"] = new RooDataSet("data_CR2", "data_CR2", RooArgSet(*obs_CR2, *total_weight), RooFit::WeightVar(*total_weight));
    theDataSets["CR2"]->add(RooArgSet(*obs_CR2, *total_weight), pdf_CR2->expectedEvents(argSetCR2), 0.0);
    theDataSets["SR"] = dataSetSR;

#ifdef debug
    std::cout << "pdf_CR0->expectedEvents() (N_obs): " << pdf_CR0->expectedEvents(argSetCR0)<< std::endl;
    std::cout << "pdf_CR1->expectedEvents() (N_obs): " << pdf_CR1->expectedEvents(argSetCR1)<< std::endl;
    std::cout << "pdf_CR2->expectedEvents() (N_obs): " << pdf_CR2->expectedEvents(argSetCR2)<< std::endl;
    Double_t combinatoricsShift = (pdf_CR0->expectedEvents(argSetCR0)+pdf_CR1->expectedEvents(argSetCR1)+pdf_CR2->expectedEvents(argSetCR2)+pdf_SR->expectedEvents(nullptr))*std::log(4);
    for (const auto& [key, value] : theDataSets){
        std::cout << key << std::endl;
        value->get(0);
        value->Print("v");
        std::cout << "total_weight has a value of " << value->weightVar()->getVal() << std::endl;
        std::cout << std::endl;
    }
#endif

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*pdf_CR0,*pdf_CR1,*pdf_CR2,*pdf_SR), *cat);
    observables.add(*obs_CR0);
    observables.add(*obs_CR1);
    observables.add(*obs_CR2);
    observables.add(*cat);
    RooDataSet* combinedData = new RooDataSet("asimovDataSet", "asimovDataSet", observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));
#ifdef debug
    std::cout << "simPdf: " << std::endl;
    simPdf->Print("v");
    std::cout << std::endl;
    std::cout << "Combined Data: " << std::endl;
    combinedData->Print("v");
    
    mu->setVal(muVal);
    // Create NLLs with the CR Poissons
    RooNLLVar* test0 = (RooNLLVar*)pdf_CR0->createNLL(*theDataSets.at("CR0"), RooFit::Verbose(true));
    // test0->Print("v");
    std::cout << "2*test0->getVal(): " << 2*test0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test1 = (RooNLLVar*)pdf_CR1->createNLL(*theDataSets.at("CR1"), RooFit::Verbose(true));
    // test1->Print("v");
    std::cout << "2*test1->getVal(): " << 2*test1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test2 = (RooNLLVar*)pdf_CR2->createNLL(*theDataSets.at("CR2"), RooFit::Verbose(true));
    // test2->Print("v");
    std::cout << "2*test2->getVal(): " << 2*test2->getVal() << std::endl;
    std::cout << std::endl;

    // Create NLL with SR
    Double_t expectedEventsSR = pdf_SR->expectedEvents(nullptr);
    std::cout << "SR PDF Expected Events: " << expectedEventsSR << std::endl;
    RooNLLVar* testSR = (RooNLLVar*)pdf_SR->createNLL(*theDataSets.at("SR"), RooFit::Verbose(true));
    // testSR->Print("v");
    std::cout << "testSR->getVal(): " << testSR->getVal() << std::endl;
    std::cout << std::endl;
    std::cout << "sum of individual NLLs: " << 2*(test0->getVal() + test1->getVal() + test2->getVal() + testSR->getVal()) << std::endl;

    // Verify that the Combined/Simultaneous PDF Gives the Correct NLL with the combined dataset
    RooNLLVar* testCombined = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Verbose(true));
    // testCombined->Print("v"); 
    std::cout << "testCombined->getVal(): " << testCombined->getVal() << std::endl;
    std::cout << "one half of sum of individual NLLs: " << test0->getVal() + test1->getVal() + test2->getVal() + testSR->getVal() << std::endl;
    std::cout << std::endl;

    std::cout << std::fixed;
    std::cout << std::setprecision(6);
    
    std::cout << "NLL of Combined 4l Regions, mu = " << mu->getVal() << ": " << 2*(testCombined->getVal() - combinatoricsShift) << std::endl;
    double theGraphShift = 2*(testCombined->getVal() - combinatoricsShift);
    // Repeat for the SR - just through extendedTerm()
    double theExtendedTerm = pdf_SR->extendedTerm(*dataSetSR, false, false);
    std::cout << "RooDensityRatio expectedEvents(): " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "RooDensityRatio Extended Term: " << theExtendedTerm << std::endl;
    std::cout << "What the Extended Term Should Be: "<< pdf_SR->expectedEvents(nullptr) - pdf_SR->expectedEvents(nullptr)*std::log(pdf_SR->expectedEvents(nullptr)) << std::endl;
#endif    
    
    // Make sure things still write to a workspace
    RooWorkspace* workspace = new RooWorkspace("combined");

    std::cout << "\n.... Importing pdf_SR ...." << std::endl;
    workspace->import(*pdf_SR, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "\n.... pdf_SR import successful ...." << std::endl;
    workspace->defineSet("pdf_SR", *pdf_SR);
    
    std::cout << "Importing CR0 Pdf..." << std::endl;
    workspace->import(*pdf_CR0, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR0 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR0", *pdf_CR0);

    std::cout << "Importing CR0 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR0"));
    std::cout << "CR0 dataset import successful" << std::endl;

    std::cout << "Importing CR1 Pdf..." << std::endl;
    workspace->import(*pdf_CR1, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR1 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR1", *pdf_CR1);

    std::cout << "Importing CR1 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR1"));
    std::cout << "CR1 dataset import successful" << std::endl;

    std::cout << "Importing CR2 Pdf..." << std::endl;
    workspace->import(*pdf_CR2, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "CR2 Pdf import successful" << std::endl;
    workspace->defineSet("pdf_CR2", *pdf_CR2);

    std::cout << "Importing CR2 dataset..." << std::endl;
    workspace->import(*theDataSets.at("CR2"));
    std::cout << "CR2 dataset import successful" << std::endl;

    std::cout << "Importing simPdf..." << std::endl;
    workspace->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "simPdf import successful" << std::endl;
    workspace->defineSet("simPdf",*simPdf);

    std::cout << "Saving SR Dataset to Aux file..." << std::endl;
    
    // Define the ModelConfig for the Workspace
    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("wsMC", "wsMC", workspace);
    wsMC->SetPdf("simPdf");
    wsMC->SetObservables(RooArgSet(*obs_CR0,*obs_CR1,*obs_CR2, observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]));
    wsMC->SetNuisanceParameters(RooArgSet(*ATLAS_LUMI));
    wsMC->SetParametersOfInterest(pois);

    std::cout << "\nfinal workspace:\n" << std::endl;
    workspace->Print("v");
    std::cout << "PREPARE TO WRITE TO FILE "<< std::endl;
    workspace ->writeToFile(wsFilename);
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    mu = (RooRealVar*) workspace->obj("mu");
    ATLAS_LUMI = (RooRealVar*) workspace->obj("ATLAS_LUMI");

#ifdef debug
    // Create NLL with the RooDensityRatio
    RooNLLVar* nllPoissonSR = (RooNLLVar*)workspace->pdf("pdf_SR")->createNLL(*dataSetSR, RooFit::Verbose(true)); 
    nllPoissonSR->Print("v");
    std::cout << "nllPoissonSR->getVal(): " << nllPoissonSR->getVal() << std::endl;
    std::cout << std::endl;

    // Create NLLs with the CR Poissons
    RooNLLVar* nllPoissonCR0 = (RooNLLVar*)workspace->pdf("pdf_CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Verbose(true));
    nllPoissonCR0->Print("v");
    std::cout << "nllPoissonCR0->getVal(): " << nllPoissonCR0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* nllPoissonCR1 = (RooNLLVar*)workspace->pdf("pdf_CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Verbose(true));
    nllPoissonCR1->Print("v");
    std::cout << "nllPoissonCR1->getVal(): " << nllPoissonCR1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* nllPoissonCR2 = (RooNLLVar*)workspace->pdf("pdf_CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Verbose(true));
    nllPoissonCR2->Print("v");
    std::cout << "nllPoissonCR2->getVal(): " << nllPoissonCR2->getVal() << std::endl;
    std::cout << std::endl;
#endif
    
    // Verify that the Combined/Simultaneous PDF Gives the Correct NLL with the combined dataset with ModelConfig
    RooNLLVar* nllCombined = (RooNLLVar*)wsMC->GetPdf()->createNLL(*combinedData, RooFit::Verbose(true), RooFit::NumCPU(8,0));

#ifdef debug
    nllCombined->Print("v"); 
    std::cout << "nllCombined->getVal(): " << nllCombined->getVal() << std::endl;
    std::cout << std::endl;

    std::cout << std::fixed;
    std::cout << std::setprecision(8);
    std::cout << "NLL of Combined 4l Regions: " << nllCombined->getVal() - (pdf_CR0->expectedEvents(&pois)+pdf_CR1->expectedEvents(&pois)+pdf_CR2->expectedEvents(&pois)+pdf_SR->expectedEvents(nullptr))*std::log(4) << std::endl;
#endif
    
    /* Create a Loop and a Graph now */
    RooMinimizer minim(*nllCombined);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
#ifdef debug
    minim.setPrintLevel(3);
#endif
    
    ATLAS_LUMI->setConstant(true);
    mu->setConstant(false);
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    (void)status;
    double nll_min =  2*nllCombined->getVal();
    
    TGraph* nll_scan = new TGraph();
    nll_scan->SetName("nll_scan");
    nll_scan->SetMarkerStyle(21);
    for (int i=0; i<100; i++){

        mu->setConstant(true);
        mu->setVal((float)i/30.);

	    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
                                    ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
        (void)status;

#ifdef debug
        std::cout << "mu: " << mu->getVal() << "     ";
        std::cout << "2*nllCombined: " << 2*nllCombined->getVal() << std::endl;
#endif
	
        nll_scan->AddPoint(mu->getVal(), 2*nllCombined->getVal()-nll_min);
    }
    std::string foutPath = "poiScan.root";
    TFile* fout = new TFile(foutPath.c_str(), "recreate");
    nll_scan->Write();
    fout->Close();

    return 0;
}
