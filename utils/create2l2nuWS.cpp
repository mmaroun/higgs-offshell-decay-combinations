#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "RooRandom.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "higgsOffshellDecayCombinations/ws2l2nuHandler.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#define debug
#define doTheMinimization

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    const char *inputFileName = argv[1];
    const char *wsFilenamePrefix = argv[2];

    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", 140.1);

    std::vector<std::string> pois_list = {"mu", "mu_ggF", "mu_VBF", "mu_emu", "mu_3lep", "mu_3lep_1", "mu_3lep_2", "mu_Zjets",
                                            "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    double muVal = 1.0;
    for (auto poiName : pois_list){
        RooRealVar* poi = new RooRealVar(poiName.c_str(), poiName.c_str(), muVal, 0.0, 10.0);
        pois.add(*poi);
    }
#ifdef debug
    pois.Print("v");
#endif

    std::vector<std::string> binNames = {"n_jets_CR_3lep_0jet_Incl_bin_1_13TeV","n_jets_CR_3lep_1jet_Incl_bin_1_13TeV",
            "n_jets_CR_3lep_2jet_Incl_bin_1_13TeV","mT_ZZ_CR_emu_Incl_bin_1_13TeV","mT_ZZ_CR_ZJets_Incl_bin_1_13TeV",
            "mT_ZZ_SR_ggF_Incl_bin_1_13TeV","mT_ZZ_SR_ggF_Incl_bin_2_13TeV","mT_ZZ_SR_ggF_Incl_bin_3_13TeV",
            "mT_ZZ_SR_ggF_Incl_bin_4_13TeV","mT_ZZ_SR_ggF_Incl_bin_5_13TeV","mT_ZZ_SR_ggF_Incl_bin_6_13TeV",
            "mT_ZZ_SR_ggF_Incl_bin_7_13TeV","mT_ZZ_SR_ggF_Incl_bin_8_13TeV","mT_ZZ_SR_ggF_Incl_bin_9_13TeV",
            "mT_ZZ_SR_ggF_Incl_bin_10_13TeV","mT_ZZ_SR_ggF_Incl_bin_11_13TeV","mT_ZZ_SR_ggF_Incl_bin_12_13TeV",
            "mT_ZZ_SR_ggF_Incl_bin_13_13TeV","mT_ZZ_SR_ggF_Incl_bin_14_13TeV","mT_ZZ_SR_ggF_Incl_bin_15_13TeV",
            "mT_ZZ_SR_Mixed_Incl_bin_1_13TeV","mT_ZZ_SR_Mixed_Incl_bin_2_13TeV","mT_ZZ_SR_Mixed_Incl_bin_3_13TeV",
            "mT_ZZ_SR_Mixed_Incl_bin_4_13TeV","mT_ZZ_SR_VBF_Incl_bin_1_13TeV","mT_ZZ_SR_VBF_Incl_bin_2_13TeV",
            "mT_ZZ_SR_VBF_Incl_bin_3_13TeV","mT_ZZ_SR_VBF_Incl_bin_4_13TeV"};

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"ggSNLO","ggBNLO","ggSBINLOI","VBFB","VBFSBI","VBFSBI10","Zjets_bkg","tot_emu_bkg",
                        "WZ_3lep_Part0","WZ_3lep_Part1","WZ_3lep_Part2","Other_bkg","qqZZ_1_Part0","qqZZ_1_Part1","qqZZ_1_Part2",
                        "qqZZ_2_Part0","qqZZ_2_Part1","qqZZ_2_Part2"};
    RooFormulaVar* f_ggSNLO = new RooFormulaVar("f_ggSNLO", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_ggBNLO = new RooFormulaVar("f_ggBNLO", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_ggSBINLOI = new RooFormulaVar("f_ggSBINLOI", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_VBFB = new RooFormulaVar("f_VBFB", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_VBFSBI = new RooFormulaVar("f_VBFSBI", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_VBFSBI10 = new RooFormulaVar("f_VBFSBI10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_Zjets_bkg = new RooFormulaVar("f_Zjets_bkg", "@0", RooArgList(pois["mu_Zjets"]));
    RooFormulaVar* f_tot_emu_bkg = new RooFormulaVar("f_tot_emu_bkg", "@0", RooArgList(pois["mu_emu"]));
    RooFormulaVar* f_WZ_3lep_Part0 = new RooFormulaVar("f_WZ_3lep_Part0", "@0", RooArgList(pois["mu_3lep"]));
    RooFormulaVar* f_WZ_3lep_Part1 = new RooFormulaVar("f_WZ_3lep_Part1", "@0*@1", RooArgList(pois["mu_3lep"], pois["mu_3lep_1"]));
    RooFormulaVar* f_WZ_3lep_Part2 = new RooFormulaVar("f_WZ_3lep_Part2", "@0*@1*@2", RooArgList(pois["mu_3lep"], pois["mu_3lep_1"], pois["mu_3lep_2"]));
    RooFormulaVar* f_Other_bkg = new RooFormulaVar("f_Other_bkg", "@0", RooArgList(RooRealConstant::value(1.0)));
    RooFormulaVar* f_qqZZ_1_Part0 = new RooFormulaVar("f_qqZZ_1_Part0", "@0", RooArgList(pois["mu_qqZZ"]));
    RooFormulaVar* f_qqZZ_1_Part1 = new RooFormulaVar("f_qqZZ_1_Part1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"]));
    RooFormulaVar* f_qqZZ_1_Part2 = new RooFormulaVar("f_qqZZ_1_Part2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"]));
    RooFormulaVar* f_qqZZ_2_Part0 = new RooFormulaVar("f_qqZZ_2_Part0", "@0", RooArgList(pois["mu_qqZZ"]));
    RooFormulaVar* f_qqZZ_2_Part1 = new RooFormulaVar("f_qqZZ_2_Part1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"]));
    RooFormulaVar* f_qqZZ_2_Part2 = new RooFormulaVar("f_qqZZ_2_Part2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"]));
    RooArgList multipliers(*f_ggSNLO, *f_ggBNLO, *f_ggSBINLOI, *f_VBFB, *f_VBFSBI, *f_VBFSBI10, *f_Zjets_bkg, *f_tot_emu_bkg, *f_WZ_3lep_Part0,
                            *f_WZ_3lep_Part1, *f_WZ_3lep_Part2, *f_Other_bkg, *f_qqZZ_1_Part0, *f_qqZZ_1_Part1, *f_qqZZ_1_Part2, 
                            *f_qqZZ_2_Part0, *f_qqZZ_2_Part1, *f_qqZZ_2_Part2);
#ifdef debug
    multipliers.Print("v");
#endif

    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", 
    "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", 
    "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", 
    "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", 
    "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_TrigStatUncertainty", 
    "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", 
    "ATLAS_FT_EFF_Eigen_B_1", "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", 
    "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8", "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", 
    "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", 
    "ATLAS_FT_EFF_Eigen_Light_2", "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", 
    "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9", "ATLAS_JET_JvtEfficiency", 
    "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet", "ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", 
    "ATLAS_PS_VBF_RE", "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", 
    "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet", "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", 
    "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    std::cout << "Number of NPs: " << nps.getSize() << std::endl;
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
#ifdef debug
    nps.Print("v");
#endif
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    RooRealVar* total_weight = new RooRealVar("total_weight", "total_weight", -inf, inf);

    Ws2l2nuHandler objHandle(inputFileName, nps, processList, binNames);
    std::map<std::string, RooAddPdf*> theBinPdfs = objHandle.generatePdfsAndDataSets(multipliers, ATLAS_LUMI, total_weight);
    RooArgSet theBinObservables = objHandle.GetWSObservables();
    std::map<std::string, RooDataSet*> theBinDataSets_Asimov = objHandle.GetAsimovDataSets();
    std::map<std::string, RooDataSet*> theBinDataSets_RealData = objHandle.GetRealDataSets();
#ifdef debug
    std::cout << std::endl;
    std::cout << "WS Handler Debug:" << std::endl;
    objHandle.PrintAsimovNominalDict();
    std::cout << std::endl;
    objHandle.PrintObsYieldsDict();
    std::cout << std::endl;
    objHandle.PrintVariationsDict();
    std::cout << std::endl;
    std::cout << "Asimov DSs:" << std::endl;
    for (const auto& [key, value] : theBinDataSets_Asimov){
        std::cout << key << ":" << std::endl;
        value->Print("v");
        std::cout << "Weight Var has value " << value->weightVar()->getVal() << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Obs DSs: " << std::endl;
    for (const auto& [key, value] : theBinDataSets_RealData){
        std::cout << key << ": " << std::endl;
        value->Print("v");
        std::cout << "Weight Var has value " << value->weightVar()->getVal() << std::endl;
    }

    std::cout << std::fixed;
    std::cout << std::setprecision(12);

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Real Data NLL Comparison for JAX: " << std::endl;
    double nllInitValue = 0.0;
    for (const auto& [key, value] : theBinPdfs){
        std::string obsName = "obs_"+std::string(key);
        RooArgSet* eeSet = new RooArgSet(*(RooRealVar*)theBinObservables.find(obsName.c_str()));
        double theEE = value->expectedEvents(eeSet);
        double theNobs = theBinDataSets_RealData.at(key)->sumEntries();
        double theNLLVal = value->createNLL(*theBinDataSets_RealData.at(key), RooFit::Constrain(SH.GetNuisanceParams()), RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
        std::cout << key << ": " << std::endl;
        std::cout << "      Expected Events: " << theEE << std::endl;
        std::cout << "      N_obs: " << theNobs << std::endl;
        std::cout << "      NLL: " << theNLLVal << std::endl;
        std::cout << "      NLL by Hand: " << theEE - theNobs*std::log(theEE) << std::endl;
        nllInitValue += theNLLVal;
    }
    std::cout << std::endl;
    std::cout << "Total NLL from Initial Values: " << 2*nllInitValue << std::endl;
    std::cout << std::endl;

    std::vector<double> poi_pulls = {1.05515586e+00, 1.0, 1.0, 1.11278701e+00,  8.51480180e-01,  8.91379268e-01,
                                1.04936778e+00,  9.23277464e-01,  7.50231664e-01,  8.93088395e-01, 1.07579694e+00};
    std::vector<std::string> poi_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2", "mu_3lep", "mu_3lep_1", "mu_3lep_2", "mu_Zjets", "mu_emu"};
    std::map<std::string, double> pullsPOIsMap;
    int kk=0;
    for (auto poiName : poi_list){
        pullsPOIsMap[poiName] = poi_pulls[kk];
        kk++;
    }
    for (const auto& [key, value] : pullsPOIsMap){
        std::cout << "Setting " << key << " to " << value << std::endl;
        ((RooRealVar*)pois.find(key.c_str()))->setVal(value);
    }

    std::vector<double> np_pulls = {-2.29179944e-02, -3.98599784e-02,
        -1.69479630e-01, -1.19651979e-01,  1.33152839e-01,  8.62146319e-02,
        4.85883502e-02,  6.89084478e-02,  1.51341101e-01, -2.69191609e-02,
        -1.93222089e-02,  7.30546690e-02, -1.71481901e-01,  1.64799791e-01,
        -1.02626519e-01, -1.55396132e-02, -4.67613197e-02, -2.68400377e-02,
        -1.62117554e-01, -4.81442437e-02,  7.10303016e-02, -1.23164723e-01,
        -1.85357227e-01, -5.17553643e-02, -1.64671437e-01, -1.04531164e-01,
        1.97808804e-01,  3.18477831e-02, -7.77601672e-02,  1.54000641e-01,
        9.65441912e-02, -8.72307029e-02,  5.27325295e-02, -1.07953642e-01,
        -9.20219045e-02, -6.60482770e-02,  1.97201035e-02,  2.08019422e-01,
        -3.08886111e-02, -1.13994131e-02,  1.68634596e-02,  3.75317972e-02,
        7.32912549e-02, -2.87078663e-02, -7.26173126e-02,  1.30908228e-01,
        -8.38547116e-03, -2.74824259e-02,  2.56800655e-01, -2.11684851e-01,
        -6.40144883e-02, -3.99128194e-01, -6.95843623e-02, -1.44033394e-01,
        1.80928821e-01, -1.47806827e-01, -6.34380065e-01, -1.55865197e-01,
        -1.28674423e-01, -2.12304289e-02,  1.87258321e-01, -4.14088489e-01,
        -1.11358975e-01, -3.04937528e-01, -3.47302010e-01, -1.34545616e-01,
        -2.21724728e-01,  8.95447093e-02,  1.75076877e-02,  4.27454278e-01,
        -3.12102163e-01,  7.37164158e-02, -3.09708177e-02, -1.89043026e-01,
        -3.13460183e-02,  1.62105628e-02,  2.95826332e-02, -6.96113902e-03,
        1.16175891e-02,  8.93396870e-02,  9.13741171e-02,  4.11665180e-01,
        2.50575319e-01, -6.32898763e-01, -2.56197449e-01, -2.59203058e-01,
        2.54866327e-01, -3.21620536e-01,  6.19616732e-02,  1.04408968e-01,
        1.08911469e-01, -6.07725479e-02,  6.06156824e-01,  5.79681659e-02,
        4.37311255e-02,  6.69974831e-02, -6.06151896e-02, -1.99796204e-01,
        2.49182120e-01,  4.89910825e-02, -1.64173616e-02, -1.21286865e-01,
        -5.96760968e-02, -1.81530995e-02, -1.43048670e-01, -9.40512597e-02,
        -5.48192320e-02,  6.42037882e-02,  1.01906090e-01,  3.35978534e-01,
        8.83589312e-02, -2.11249126e-03,  2.07350567e-01,  4.26964289e-01,
        -3.75069356e-01,  9.91629078e-02,  3.28431587e-01,  6.16479939e-01,
        -2.85373720e-01, -1.72387313e-01, -7.05918463e-02, -1.81204234e-01,
        -9.01099720e-02, -4.40744815e-02, -5.02395715e-02,  1.02632563e-01,
        9.86604203e-02,  0.00000000e+00,  0.00000000e+00, -3.85502956e-03,
        -6.29536133e-03,  3.08907551e-02,  3.40160664e-02, -4.02614628e-02,
        -1.01164162e-02,  9.28103882e-03, -2.34489181e-03, -2.00261893e-04,
        5.40632839e-06,  1.12336296e-23,  1.12336296e-23,  1.12336296e-23,
        1.69352834e-02,  1.71129770e-03, -2.17818481e-03, -5.92623112e-05,
        1.96272919e-02, -5.74937101e-04,  1.09938720e-04,  1.48102649e-04,
        -1.41668833e-04, -4.22234560e-02,  1.15173761e-01, -1.66608195e-01,
        -1.67917946e-02,  1.92932344e-02,  5.95512098e-02, -8.40448722e-03,
        -4.30224233e-03, -5.39239545e-03,  5.18225294e-03, -4.69601072e-04,
        5.11703309e-03, -6.47641394e-05,  3.09096090e-02, -1.78387126e-01,
        5.66832080e-02,  2.16180770e-01,  7.76152098e-02,  6.32350484e-02,
        5.26300668e-02,  7.15170634e-02, -5.80053249e-02, -3.27983697e-02,
        -9.77668636e-03};
    std::vector<std::string> np_pulls_list = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2",
       "ATLAS_EG_SCALE_ALL", "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", "ATLAS_EL_EFF_ID_CorrUncertaintyNP9",
       "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS",
       "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", "ATLAS_JET_EffectiveNP_Modelling1",
       "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4",
       "ATLAS_JET_EffectiveNP_Statistical5", "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta",
       "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg",
       "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6",
       "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV",
       "ATLAS_JET_Pileup_PtTerm", "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape",
       "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape",
       "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm",
       "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_MUON_EFF_TrigStatUncertainty", "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", "ATLAS_FT_EFF_Eigen_B_1",
       "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8",
       "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", "ATLAS_FT_EFF_Eigen_Light_2",
       "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9",  "ATLAS_JET_JvtEfficiency",
       "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet","ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", "ATLAS_PS_VBF_RE",
       "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet",
       "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};
    std::map<std::string, double> pullsNPsMap;
    int ff=0;
    for (auto npName : np_pulls_list){
        pullsNPsMap[npName] = np_pulls[ff];
        ff++;
    }
    double rawNPvalSum = 0.0;
    for (const auto& [key, value] : pullsNPsMap){
        std::cout << "Setting " << key << " to " << value << std::endl;
        std::string alphaName = "alpha_"+key;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(value);
        rawNPvalSum += 0.5*value*value;
    }

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Real Data NLL Comparison for JAX, with JAX Pulls Values: " << std::endl;
    double nllTotalPulls = 0.0;
    for (const auto& [key, value] : theBinPdfs){
        std::string obsName = "obs_"+std::string(key);
        RooArgSet* eeSet = new RooArgSet(*(RooRealVar*)theBinObservables.find(obsName.c_str()));
        double theEE = value->expectedEvents(eeSet);
        double theNobs = theBinDataSets_RealData.at(key)->sumEntries();
        double theNLLVal = value->createNLL(*theBinDataSets_RealData.at(key), RooFit::Constrain(SH.GetNuisanceParams()), RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
        std::cout << key << ": " << std::endl;
        std::cout << "      Expected Events: " << theEE << std::endl;
        std::cout << "      N_obs: " << theNobs << std::endl;
        std::cout << "      NLL: " << theNLLVal << std::endl;
        std::cout << "      NLL by Hand: " << theEE - theNobs*std::log(theEE) << std::endl;
        nllTotalPulls += theNLLVal;
    }
    std::cout << std::endl;
    std::cout << "Total NLL: " << 2*nllTotalPulls << std::endl;
    std::cout << "What the Constrained NLL should be: " << 2*(nllTotalPulls + rawNPvalSum) << std::endl;
    std::cout << std::endl;

#endif

    // Constrain Each Pdf, build the RooCategory
    RooArgList constrainedPdfs;
    std::map<std::string, int> binNameMap;
    int i=0;
    for (const auto& [key, value] : theBinPdfs){
        std::string constPdfName = "constrained_Pdf_"+key;
        RooProdPdf* constrainedPdf = new RooProdPdf(constPdfName.c_str(), constPdfName.c_str(), RooArgList(*constraintPdf, *value));
        constrainedPdfs.add(*constrainedPdf);
        binNameMap[key] = i;
        i++;
    }
    RooCategory* cat = new RooCategory("cat", "cat", binNameMap);

    // Create a RooSimultaneous, Combined Real Data and Combined Asimov Data
    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", constrainedPdfs, *cat);

    RooDataSet* asimovData = new RooDataSet("asimov_data_2l2nu_offshell", "asimov_data_2l2nu_offshell", theBinObservables,
                                            RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theBinDataSets_Asimov));

    RooDataSet* obsData = new RooDataSet("obs_data_2l2nu_offshell", "obs_data_2l2nu_offshell", theBinObservables,
                                            RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theBinDataSets_RealData));
    std::cout << "obsData sum of Entries: " << obsData->sumEntries() << std::endl;

    double nllShift = obsData->sumEntries()*std::log(cat->size()) + nps.getSize()*std::log(std::sqrt(2*3.14159265359));
#ifdef debug
    double simNLL = simPdf->createNLL(*obsData, RooFit::Constrain(nps), RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
    std::cout << "Shifted NLL Value, RooFit: " << 2*(simNLL - nllShift) << std::endl;
#endif

    // Save to RooWorkspaces (data and asimov)
// #ifndef debug
    RooWorkspace* wsAsimov = new RooWorkspace("combined");
    RooWorkspace* wsData = new RooWorkspace("combined");

    for (int i=0; i<constrainedPdfs.getSize(); i++){
        std::cout << "Importing " << ((RooProdPdf*)constrainedPdfs.at(i))->GetName() << " to Asimov and Data WSs..." << std::endl;
        wsAsimov->import(*((RooProdPdf*)constrainedPdfs.at(i)), RooFit::RecycleConflictNodes(), RooFit::Silence());
        wsData->import(*((RooProdPdf*)constrainedPdfs.at(i)), RooFit::RecycleConflictNodes(), RooFit::Silence());
        std::cout << "...Imports Successsful." << std::endl;
    }

    std::cout << "Importing SimPdf to Asimov and Obs Data WSs..." << std::endl;
    wsAsimov->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    wsData->import(*simPdf, RooFit::RecycleConflictNodes(), RooFit::Silence());
    std::cout << "...Imports Successful." << std::endl;

    for (const auto& [key, value] : theBinDataSets_Asimov){
        std::cout << "Importing Asimov DS for " << key << "..." << std::endl;
        wsAsimov->import(*value);
        std::cout << "...Import Successful." << std::endl;
    }

    for (const auto& [key, value] : theBinDataSets_RealData){
        std::cout << "Importing Obs DS for " << key << "..." << std::endl;
        wsData->import(*value);
        std::cout << "...Import Successful." << std::endl;
    }

    std::cout << "Importing Combined Data to Asimov WS..." << std::endl;
    wsAsimov->import(*asimovData);
    std::cout << "...Import Successful." << std::endl;

    std::cout << "Importing Combined Data to Obs WS..." << std::endl;
    wsData->import(*obsData);
    std::cout << "...Import Successful." << std::endl;

    RooStats::ModelConfig* wsMC = new RooStats::ModelConfig("ModelConfig_2l2nu_offshell", "ModelConfig_2l2nu_offshell", wsAsimov);
    wsMC->SetPdf("simPdf");
    wsMC->SetObservables(theBinObservables);
    wsMC->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC->SetParametersOfInterest(pois);
    wsMC->SetGlobalObservables(SH.GetGlobalObservables());
    RooArgSet conditionalObservables("conditionalObservables");
    wsMC->SetConditionalObservables(conditionalObservables);
    wsAsimov->import(*wsMC);

    RooStats::ModelConfig* wsMC_1 = new RooStats::ModelConfig("ModelConfig_2l2nu_offshell", "ModelConfig_2l2nu_offshell", wsData);
    wsMC_1->SetPdf("simPdf");
    wsMC_1->SetObservables(theBinObservables);
    wsMC_1->SetNuisanceParameters(SH.GetNuisanceParams());
    wsMC_1->SetParametersOfInterest(pois);
    wsMC_1->SetGlobalObservables(SH.GetGlobalObservables());
    wsMC_1->SetConditionalObservables(conditionalObservables);
    wsData->import(*wsMC_1);

    std::string wsAsimovFile = std::string(wsFilenamePrefix)+"_Asimov.root";
    std::cout << "PREPARE TO WRITE ASIMOV WS TO FILE " << wsAsimovFile << std::endl;
    wsAsimov->writeToFile(wsAsimovFile.c_str());
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;

    std::string wsObsFile = std::string(wsFilenamePrefix)+"_RealData.root";
    std::cout << "PREPARE TO WRITE OBS WS TO FILE " << wsObsFile << std::endl;
    wsData->writeToFile(wsObsFile.c_str());
    std::cout << "FILE WRITTEN SUCCESSFULLY " << std::endl;
    std::cout << std::endl;
// #endif

#ifdef doTheMinimization
    RooMinimizer minim(*simPdf->createNLL(*obsData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()), RooFit::EvalBackend("legacy")));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    minim.setStrategy(0);
    minim.setPrintLevel(1);
    minim.setEps(0.01);
    minim.optimizeConst(2);
    minim.setOffsetting(true);
    RooRandom::randomGenerator()->SetSeed(0);

    ATLAS_LUMI->setConstant(true);
    ((RooRealVar*)pois.find("mu"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_qqZZ"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_qqZZ_1"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_qqZZ_2"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_emu"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_3lep"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_3lep_1"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_3lep_2"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_Zjets"))->setConstant(false);
    ((RooRealVar*)pois.find("mu_ggF"))->setConstant(true);
    ((RooRealVar*)pois.find("mu_VBF"))->setConstant(true);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(false);
    }
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    // RooFit::OwningPtr<RooFitResult> theSaveMigrad = minim.save("test", "test");
    // const RooArgList& theSaveListMigrad = theSaveMigrad->floatParsFinal();
    // theSaveListMigrad.Print("v");
#endif

    std::cout << std::endl;
    double rooMinOptNLL = simPdf->createNLL(*obsData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
    std::cout << "NLL with what RooMinimizer returns: " << rooMinOptNLL << std::endl;
    std::cout << "2*NLL with shift: " << 2*(rooMinOptNLL - nllShift) << std::endl;
    std::cout << std::endl;


    std::vector<double> poi_pullsAgain = {1.031304741576178, 1.0, 1.0, 0.9268798607344227, 1.2557793291242163, 0.9521357244085134, 1.0604325388735572, 0.9035010771184924, 0.7400841634459748, 0.8945601342579615, 1.078004123128236, 1.031304741576176};
    std::vector<std::string> poi_listAgain = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2", "mu_3lep", "mu_3lep_1", "mu_3lep_2", "mu_Zjets", "mu_emu"};
    std::map<std::string, double> pullsPOIsMapAgain;
    int gg=0;
    for (auto poiName : poi_listAgain){
        pullsPOIsMapAgain[poiName] = poi_pullsAgain[gg];
        gg++;
    }
    for (const auto& [key, value] : pullsPOIsMapAgain){
        std::cout << "Setting " << key << " to " << value << std::endl;
        ((RooRealVar*)pois.find(key.c_str()))->setVal(value);
    }

    // std::vector<double> np_pullsAgain = {-2.29179944e-02, -3.98599784e-02,
    //     -1.69479630e-01, -1.19651979e-01,  1.33152839e-01,  8.62146319e-02,
    //     4.85883502e-02,  6.89084478e-02,  1.51341101e-01, -2.69191609e-02,
    //     -1.93222089e-02,  7.30546690e-02, -1.71481901e-01,  1.64799791e-01,
    //     -1.02626519e-01, -1.55396132e-02, -4.67613197e-02, -2.68400377e-02,
    //     -1.62117554e-01, -4.81442437e-02,  7.10303016e-02, -1.23164723e-01,
    //     -1.85357227e-01, -5.17553643e-02, -1.64671437e-01, -1.04531164e-01,
    //     1.97808804e-01,  3.18477831e-02, -7.77601672e-02,  1.54000641e-01,
    //     9.65441912e-02, -8.72307029e-02,  5.27325295e-02, -1.07953642e-01,
    //     -9.20219045e-02, -6.60482770e-02,  1.97201035e-02,  2.08019422e-01,
    //     -3.08886111e-02, -1.13994131e-02,  1.68634596e-02,  3.75317972e-02,
    //     7.32912549e-02, -2.87078663e-02, -7.26173126e-02,  1.30908228e-01,
    //     -8.38547116e-03, -2.74824259e-02,  2.56800655e-01, -2.11684851e-01,
    //     -6.40144883e-02, -3.99128194e-01, -6.95843623e-02, -1.44033394e-01,
    //     1.80928821e-01, -1.47806827e-01, -6.34380065e-01, -1.55865197e-01,
    //     -1.28674423e-01, -2.12304289e-02,  1.87258321e-01, -4.14088489e-01,
    //     -1.11358975e-01, -3.04937528e-01, -3.47302010e-01, -1.34545616e-01,
    //     -2.21724728e-01,  8.95447093e-02,  1.75076877e-02,  4.27454278e-01,
    //     -3.12102163e-01,  7.37164158e-02, -3.09708177e-02, -1.89043026e-01,
    //     -3.13460183e-02,  1.62105628e-02,  2.95826332e-02, -6.96113902e-03,
    //     1.16175891e-02,  8.93396870e-02,  9.13741171e-02,  4.11665180e-01,
    //     2.50575319e-01, -6.32898763e-01, -2.56197449e-01, -2.59203058e-01,
    //     2.54866327e-01, -3.21620536e-01,  6.19616732e-02,  1.04408968e-01,
    //     1.08911469e-01, -6.07725479e-02,  6.06156824e-01,  5.79681659e-02,
    //     4.37311255e-02,  6.69974831e-02, -6.06151896e-02, -1.99796204e-01,
    //     2.49182120e-01,  4.89910825e-02, -1.64173616e-02, -1.21286865e-01,
    //     -5.96760968e-02, -1.81530995e-02, -1.43048670e-01, -9.40512597e-02,
    //     -5.48192320e-02,  6.42037882e-02,  1.01906090e-01,  3.35978534e-01,
    //     8.83589312e-02, -2.11249126e-03,  2.07350567e-01,  4.26964289e-01,
    //     -3.75069356e-01,  9.91629078e-02,  3.28431587e-01,  6.16479939e-01,
    //     -2.85373720e-01, -1.72387313e-01, -7.05918463e-02, -1.81204234e-01,
    //     -9.01099720e-02, -4.40744815e-02, -5.02395715e-02,  1.02632563e-01,
    //     9.86604203e-02,  0.00000000e+00,  0.00000000e+00, -3.85502956e-03,
    //     -6.29536133e-03,  3.08907551e-02,  3.40160664e-02, -4.02614628e-02,
    //     -1.01164162e-02,  9.28103882e-03, -2.34489181e-03, -2.00261893e-04,
    //     5.40632839e-06,  1.12336296e-23,  1.12336296e-23,  1.12336296e-23,
    //     1.69352834e-02,  1.71129770e-03, -2.17818481e-03, -5.92623112e-05,
    //     1.96272919e-02, -5.74937101e-04,  1.09938720e-04,  1.48102649e-04,
    //     -1.41668833e-04, -4.22234560e-02,  1.15173761e-01, -1.66608195e-01,
    //     -1.67917946e-02,  1.92932344e-02,  5.95512098e-02, -8.40448722e-03,
    //     -4.30224233e-03, -5.39239545e-03,  5.18225294e-03, -4.69601072e-04,
    //     5.11703309e-03, -6.47641394e-05,  3.09096090e-02, -1.78387126e-01,
    //     5.66832080e-02,  2.16180770e-01,  7.76152098e-02,  6.32350484e-02,
    //     5.26300668e-02,  7.15170634e-02, -5.80053249e-02, -3.27983697e-02,
    //     -9.77668636e-03};
    std::vector<double> np_pullsAgain = {-0.008497006369598643, 0.005784214816944088, -0.08337182580659457, 0.0656953776579293, 0.0001464497600723337, 0.0001828541741808634, -0.0002263084798965099, -0.0006750122182577466, -0.00014834848372828442, -0.00043034858929828144, -0.0008398292783946147, -0.00028880324292050535, 0.000605269407473599, 0.0004044791068589349, -0.00019406671090115815, 0.00030170679185153356, -0.0005605273751255461, 0.0005799845298018624, 0.0010388084106394942, 0.005885905092075992, 0.0, 0.0, 0.0, 0.0, 0.0013761275834351236, -0.0009408737336667448, -0.0007014291847154586, -0.0005324864602693685, 0.007734747031979417, 0.0, 0.0, 0.0, 0.0, -5.101030461231203e-05, -0.0007697141866883331, -0.001514399838688594, -0.0017846577491234267, 0.005288351068103159, 0.0019761513130180405, -0.0025216911651697775, 0.005400292672386652, 0.0072967882654876, -0.0005833605862039994, 0.0, -0.006459911259341354, 0.0, -0.00013387622895696797, -0.00014472044532662788, 0.015236614244483542, -0.05953264631601706, -0.08246164970249839, -0.2517271078925254, -0.05331978502559848, -0.055577282357172436, -0.09135566433158283, -0.06745160088135095, -0.08111368241354654, -0.02856867211533099, -0.08653586775714386, -0.03473239618978847, -0.049460440953408485, -0.06143422698166102, -0.0647798097188982, -0.059611669085069305, -0.0306766915708009, -0.06055885568587105, -0.0685496798509951, -0.060987053912408676, -0.06991776919272433, -0.020187237590683152, -0.04152571631897798, -0.06113855907177918, -0.05876757118122328, -0.07173813905293994, -0.045198759258977525, -0.0001916983788197192, -0.002196127018145128, -0.027998580825618506, 0.0005461929976302595, 0.010122628829814625, 0.1251875682089155, 0.1060984069687089, -0.10386372618831517, 0.07327110472707023, -0.011481681095851652, -0.18843734006071125, 0.10501527367822519, -0.14023924425862377, -0.01818561009565124, 0.026172553252357608, -0.00022086837373792075, 0.06720125867885776, 0.005734096472430179, -0.017650677848610664, 0.02684812740183458, -0.007452082229101735, 0.07364140023265656, -0.06401980078808996, -0.06122332925540152, -0.04447979950435921, 0.0786619901842117, 0.006384722699217138, -0.011453805798775685, -0.0014140377164427672, 0.035105198832132804, -0.00048693985182344423, 0.00508270780872078, 2.6156095089243075e-24, -2.5918918189339245e-21, 0.031505414058968, 0.07310393331823363, 0.0, 0.0, 0.0, 0.0, 0.0, 0.30648283129978354, -0.13888968718374395, 0.003102762889162737, -0.09365630380889445, 0.006854983848685197, -0.03173286930463304, -0.012693435406587029, -0.004760493145500436, 0.0, 0.0, 0.0, 0.0, 0.0, -0.004085071545169489, -0.0032822585758367825, 0.054650418575494, 0.03117210900627374, -0.04222153147279223, -0.00945002400020606, 0.008847658646459063, -0.0023821962574814112, -0.0001777824392966085, 8.10972980727287e-05, 2.2065003595241707e-24, 2.2065003595241707e-24, 2.2065003595241707e-24, 0.008993064781493657, 0.0007294789443411906, -0.001373047549262371, -5.359417816347937e-05, 0.0038742561205009096, -0.00043211827943323187, 8.112130329735889e-05, 4.6548238523474235e-05, -0.00015066143949346342, -0.016481427510889414, 0.027652073508433822, -0.05440274239559178, 0.05603090934570321, 0.014876484543141038, 0.018805628208492204, -0.005697263743267558, -0.005994237397603575, -0.0029118984410951548, -0.0006936265890467009, -0.003589875024023638, -0.015367151995917816, -0.00364024820748886, 0.00046288574187437444, -0.14722410935144012, 0.00192028104917062, 0.08450501874273511, 0.057967434505132365, 0.06065798572882642, 0.06109305461714277, 0.001564515839361315, -0.007818132274580284, 0.019165763434568647, -0.06557532851371808};
    std::vector<std::string> np_pulls_listAgain = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2",
       "ATLAS_EG_SCALE_ALL", "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", "ATLAS_EL_EFF_ID_CorrUncertaintyNP9",
       "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS",
       "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", "ATLAS_JET_EffectiveNP_Modelling1",
       "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4",
       "ATLAS_JET_EffectiveNP_Statistical5", "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta",
       "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg",
       "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6",
       "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV",
       "ATLAS_JET_Pileup_PtTerm", "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape",
       "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape",
       "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm",
       "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_MUON_EFF_TrigStatUncertainty", "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", "ATLAS_FT_EFF_Eigen_B_1",
       "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8",
       "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", "ATLAS_FT_EFF_Eigen_Light_2",
       "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9",  "ATLAS_JET_JvtEfficiency",
       "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet","ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", "ATLAS_PS_VBF_RE",
       "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet",
       "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};
    std::map<std::string, double> pullsNPsMapAgain;
    int mm=0;
    for (auto npName : np_pulls_listAgain){
        pullsNPsMapAgain[npName] = np_pullsAgain[mm];
        mm++;
    }
    for (const auto& [key, value] : pullsNPsMapAgain){
        std::cout << "Setting " << key << " to " << value << std::endl;
        std::string alphaName = "alpha_"+key;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(value);
    }
    std::cout << std::endl;

    double jaxMinOptNLL = simPdf->createNLL(*obsData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal();
    std::cout << "NLL with what JAX returns: " << jaxMinOptNLL << std::endl;
    std::cout << "2*NLL with shift: " << 2*(jaxMinOptNLL - nllShift) << std::endl;


    std::cout << "Done." << std::endl;

    return 0;
}