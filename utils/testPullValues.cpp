#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#define debug

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataPlusYieldsPlusCRNobsFilename = argv[2];
    const char *crYieldsVarsFilename = argv[3];

    std::cout << std::fixed;
    std::cout << std::setprecision(15);

    std::cout << "Creating simPdf..." << std::endl;
    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_VBF = new RooRealVar("mu_VBF", "mu_VBF", 1.0, 0., inf);
    mu_VBF->setConstant(true);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_VBF);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);
    std::cout << "Hello from pois" << std::endl;
    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);
    std::cout << "Hello from multipliers" << std::endl;
    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
    "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
    "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
    "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
    "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
    "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
    "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
    "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
    "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
    "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
    "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
    "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
    "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
    "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
    "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
    "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};
    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");
    std::cout << "Hello from constraintPdf" << std::endl;
    // Set Up Everything for the Signal Region
    ReadBinary srHandle(dataPlusYieldsPlusCRNobsFilename, nps, processList);
    std::cout << "Hello from ReadBinary Constructor" << std::endl;
    RooArgList eventYieldsNList = srHandle.createNYieldsList(ATLAS_LUMI);
    std::cout << "Hello from eventYieldsNList" << std::endl;
    RooArgList eventYieldsNuList = srHandle.createNuYieldsList(ATLAS_LUMI);
    std::cout << "Hello from eventYieldsNuList" << std::endl;
    RooArgList gDown = srHandle.createGArgList("down");
    std::cout << "Hello from gDown" << std::endl;
    RooArgList gUp = srHandle.createGArgList("up");
    std::cout << "Hello from gUp" << std::endl;
    RooArgSet observables = srHandle.GetObservables();
    RooDataSet* dataSetSR = srHandle.createRooDataSet("testSetSR", "testSetSR");
    dataSetSR->Print("v");
    std::cout << "Hello from dataSetSR" << std::endl;
    RooArgList procRatiosSR(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);
    RooRealVar* total_weight = (RooRealVar*)observables.find("total_weight");
    std::cout << "Hello" << std::endl;
    // Set the expected Events formula variable externally
    RooFormulaVar* expectedEventsSaveSR = new RooFormulaVar("expectedEventsSaveSR", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));

    RooDensityRatio* pdf_SR = new RooDensityRatio("pdf_SR", "pdf_SR", nps, eventYieldsNList, procRatiosSR, multipliers, gDown, gUp, *expectedEventsSaveSR);

    // Get the Control Region Pdfs
    bool runOnData = true;
    TextToPoissonParams crHandle(dataPlusYieldsPlusCRNobsFilename, crYieldsVarsFilename, nps, processList, runOnData);
    std::map<std::string, RooAddPdf*> theCRpdfs = crHandle.generateCRpdfs(multipliers, ATLAS_LUMI, total_weight);
    std::map<std::string, RooRealVar*> theCRobservables = crHandle.GetCRObservables();
    std::map<std::string, RooDataSet*> theDataSets = crHandle.GetCRDatasets();
    // Add the SR dataset to the Map of All Datasets
    theDataSets["SR"] = dataSetSR; 

    // Constrain the CR pdfs
    RooProdPdf* constrainedCR0 = new RooProdPdf("constrainedCR0", "constrainedCR0", RooArgList(*theCRpdfs.at("CR0"), *constraintPdf));
    RooProdPdf* constrainedCR1 = new RooProdPdf("constrainedCR1", "constrainedCR1", RooArgList(*theCRpdfs.at("CR1"), *constraintPdf));
    RooProdPdf* constrainedCR2 = new RooProdPdf("constrainedCR2", "constrainedCR2", RooArgList(*theCRpdfs.at("CR2"), *constraintPdf));

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"CR0", 0}, {"CR1", 1}, {"CR2", 2}, {"SR", 3}});

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*constrainedCR0,*constrainedCR1,*constrainedCR2,*pdf_SR), *cat);
    observables.add(*theCRobservables.at("CR0"));
    observables.add(*theCRobservables.at("CR1"));
    observables.add(*theCRobservables.at("CR2"));
    observables.add(*cat);

    RooDataSet* combinedData = new RooDataSet("realDataSet", "realDataSet", observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));
    std::cout << "simPdf and combinedData created." << std::endl;

    double constValAlpha0p0 = ((RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables())))->getVal();

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }

    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;

    RooNLLVar* someNLLjustConstrainedSR = (RooNLLVar*)constraintPdf->createNLL(*dataSetSR, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with SR DataSet: " << someNLLjustConstrainedSR->getVal() << std::endl;

    std::cout << "All Event Yields Nu for SR: " << std::endl;
    for (auto nu : eventYieldsNuList){
        std::cout << nu->GetName() << ": " << ((RooRealVar*)nu)->getVal() << std::endl;
    }

    std::cout << "N Yields Values and their values: " << std::endl;
    for (auto n : eventYieldsNList){
        std::cout << n->GetName() << ": " << ((RooRealVar*)n)->getVal() << std::endl;
    }

    std::cout << std::endl;
    std::cout << "SR Per-Event Variations: " << std::endl;


    std::cout << std::endl;
    std::cout << "SR Yield Variations:" << std::endl;
    std::map<std::string, std::map<std::string, std::vector<double>>> dataEventYieldsMap = srHandle.GetEventYieldsMap();
    for (const auto& [key, value] : dataEventYieldsMap){
        for (const auto& [valKey, valValue] : value){
            std::cout << key << ", " << valKey << ": [";
            for (auto valValueValue : valValue){
                std::cout << valValueValue << ", ";
            }
            std::cout << "]" << std::endl;
        }
    }

    std::cout << std::endl;
    std::cout << "CR Event Yield Variations:" << std::endl;
    crHandle.printVarYieldsDict();
    std::cout << std::endl;

    std::cout << "SR Expected Events Save Value: " << expectedEventsSaveSR->getVal() << std::endl;
    double srSumEntries = dataSetSR->sumEntries();
    std::cout << "DataSet SR Sum Entries: " << srSumEntries << std::endl;
    std::cout << "SR Hand-Calculated Extended Term: " << expectedEventsSaveSR->getVal() - srSumEntries*std::log(expectedEventsSaveSR->getVal()) << std::endl;

    std::cout << std::endl;
    std::cout << "gDown Variations: " << std::endl;
    gDown.Print("v");
    std::cout << std::endl;
    std::cout << "gUp Variations: " << std::endl;
    gUp.Print("v");
    std::cout << std::endl;

    std::cout << "SR Extended Term: " << pdf_SR->extendedTerm(*dataSetSR, false, false) << std::endl;

    RooNLLVar* testNLLsr = (RooNLLVar*)pdf_SR->createNLL(*dataSetSR, RooFit::Verbose(true));
    std::cout << "NLL of Unconstrained SR: " << testNLLsr->getVal() << std::endl;

    theDataSets.at("CR0")->Print("v");
    std::cout << theDataSets.at("CR0")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR0")->sumEntries() << std::endl;
    theDataSets.at("CR1")->Print("v");
    std::cout << theDataSets.at("CR1")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR1")->sumEntries() << std::endl;
    theDataSets.at("CR2")->Print("v");
    std::cout << theDataSets.at("CR2")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR2")->sumEntries() << std::endl;

    RooNLLVar* test0 = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Verbose(true));
    // test0->Print("v");
    std::cout << "Just the CR0 NLL: " << test0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test1 = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Verbose(true));
    // test1->Print("v");
    std::cout << "Just the CR1 NLL: " << test1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test2 = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Verbose(true));
    // test2->Print("v");
    std::cout << "Just the CR2 NLL: " << test2->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* someNLLjustConstrained0 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR0 DataSet: " << someNLLjustConstrained0->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained1 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR1 DataSet: " << someNLLjustConstrained1->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained2 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR2 DataSet: " << someNLLjustConstrained2->getVal() << std::endl;

    RooNLLVar* testCon0 = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR0 Constrained NLL: " << testCon0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon1 = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR1 Constrained NLL: " << testCon1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon2 = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR2 Constrained NLL: " << testCon2->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCombined = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Combined NLL Value, no shift: " << testCombined->getVal() << std::endl;
    std::cout << "Constrained CR0 Expected Events: " << constrainedCR0->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR1 Expected Events: " << constrainedCR1->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR2 Expected Events: " << constrainedCR2->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained SR Expected Events: " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "Shifted Combined NLL Value: " << testCombined->getVal() - (constrainedCR0->expectedEvents(&pois) + constrainedCR1->expectedEvents(&pois) + constrainedCR2->expectedEvents(&pois) + pdf_SR->expectedEvents(nullptr))*std::log(4) << std::endl;

    double eeCR0 = constrainedCR0->expectedEvents(&pois);
    double eeCR1 = constrainedCR1->expectedEvents(&pois);
    double eeCR2 = constrainedCR2->expectedEvents(&pois);
    double eeSR = pdf_SR->expectedEvents(nullptr);
    double combShift = (eeCR0 + eeCR1 + eeCR2 + eeSR)*std::log(4);
    double nllCR0 = testCon0->getVal();
    double nllCR1 = testCon1->getVal();
    double nllCR2 = testCon2->getVal();
    double nllSR = testNLLsr->getVal();
    double nllConst = someNLLjustConstrainedSR->getVal();
    double nllSum = nllCR0 + nllCR1 + nllCR2 + nllSR + nllConst;
    std::cout << "combShift: " << combShift << std::endl;
    std::cout << "nllSum: " << nllSum << std::endl;
    std::cout << "nllSum - combShift: " << nllSum - combShift << std::endl;
#endif 

    // Here are Jay's Pulls; Assign them to the POIs and NPs
    // std::vector<std::string> jaysNPlist = {"ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2", "ATLAS_EG_SCALE_ALL", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP9", "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", 
    //         "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    //         "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    //         "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", 
    //         "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", 
    //         "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS", 
    //         "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", 
    //         "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", 
    //         "ATLAS_JET_EffectiveNP_Modelling1", "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", 
    //         "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", 
            // "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4", "ATLAS_JET_EffectiveNP_Statistical5", 
            // "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", 
            // "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", 
            // "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", 
            // "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg", 
            // "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", 
            // "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6", 
            // "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", 
            // "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV", "ATLAS_JET_Pileup_PtTerm", 
            // "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", 
            // "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape", "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", 
            // "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", 
            // "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape", "ATLAS_H4l_Shower_UEPS_VBF_OffShell", 
            // "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", 
            // "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm", "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", 
            // "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet"};
    std::vector<std::string> jaysNPlist = { "ATLAS_alpha_LUMI", "ATLAS_EG_RESOLUTION_ALL", "ATLAS_EG_SCALE_AF2",
       "ATLAS_EG_SCALE_ALL", "ATLAS_EL_EFF_ID_CorrUncertaintyNP0", "ATLAS_EL_EFF_ID_CorrUncertaintyNP1", "ATLAS_EL_EFF_ID_CorrUncertaintyNP2", "ATLAS_EL_EFF_ID_CorrUncertaintyNP3", "ATLAS_EL_EFF_ID_CorrUncertaintyNP4", "ATLAS_EL_EFF_ID_CorrUncertaintyNP5", "ATLAS_EL_EFF_ID_CorrUncertaintyNP6", "ATLAS_EL_EFF_ID_CorrUncertaintyNP7", "ATLAS_EL_EFF_ID_CorrUncertaintyNP8", "ATLAS_EL_EFF_ID_CorrUncertaintyNP9",
       "ATLAS_EL_EFF_ID_CorrUncertaintyNP10", "ATLAS_EL_EFF_ID_CorrUncertaintyNP11", "ATLAS_EL_EFF_ID_CorrUncertaintyNP12", "ATLAS_EL_EFF_ID_CorrUncertaintyNP13", "ATLAS_EL_EFF_ID_CorrUncertaintyNP14", "ATLAS_EL_EFF_ID_CorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10",
       "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_MUON_EFF_ISO_STAT", "ATLAS_MUON_EFF_ISO_SYS", "ATLAS_MUON_EFF_RECO_STAT", "ATLAS_MUON_EFF_RECO_STAT_LOWPT", "ATLAS_MUON_EFF_RECO_SYS", "ATLAS_MUON_EFF_RECO_SYS_LOWPT", "ATLAS_MUON_EFF_TTVA_STAT", "ATLAS_MUON_EFF_TTVA_SYS", "ATLAS_MUON_ID", "ATLAS_MUON_MS",
       "ATLAS_MUON_SAGITTA_RESBIAS", "ATLAS_MUON_SAGITTA_RHO", "ATLAS_MUON_SCALE", "ATLAS_JET_BJES_Response", "ATLAS_JET_EffectiveNP_Detector1", "ATLAS_JET_EffectiveNP_Detector2", "ATLAS_JET_EffectiveNP_Mixed1", "ATLAS_JET_EffectiveNP_Mixed2", "ATLAS_JET_EffectiveNP_Mixed3", "ATLAS_JET_EffectiveNP_Modelling1",
       "ATLAS_JET_EffectiveNP_Modelling2", "ATLAS_JET_EffectiveNP_Modelling3", "ATLAS_JET_EffectiveNP_Modelling4", "ATLAS_JET_EffectiveNP_Statistical1", "ATLAS_JET_EffectiveNP_Statistical2", "ATLAS_JET_EffectiveNP_Statistical3", "ATLAS_JET_EffectiveNP_Statistical4",
       "ATLAS_JET_EffectiveNP_Statistical5", "ATLAS_JET_EffectiveNP_Statistical6", "ATLAS_JET_EtaIntercalibration_Modelling", "ATLAS_JET_EtaIntercalibration_NonClosure_2018data", "ATLAS_JET_EtaIntercalibration_NonClosure_highE", "ATLAS_JET_EtaIntercalibration_NonClosure_negEta",
       "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", "ATLAS_JET_EtaIntercalibration_TotalStat", "ATLAS_JET_Flavor_Composition_VBF", "ATLAS_JET_Flavor_Composition_gg", "ATLAS_JET_Flavor_Composition_qq", "ATLAS_JET_Flavor_Response_VBF", "ATLAS_JET_Flavor_Response_gg",
       "ATLAS_JET_Flavor_Response_qq", "ATLAS_JET_JER_DataVsMC_MC16", "ATLAS_JET_JER_EffectiveNP_1", "ATLAS_JET_JER_EffectiveNP_2", "ATLAS_JET_JER_EffectiveNP_3", "ATLAS_JET_JER_EffectiveNP_4", "ATLAS_JET_JER_EffectiveNP_5", "ATLAS_JET_JER_EffectiveNP_6",
       "ATLAS_JET_JER_EffectiveNP_7", "ATLAS_JET_JER_EffectiveNP_8", "ATLAS_JET_JER_EffectiveNP_10", "ATLAS_JET_JER_EffectiveNP_11", "ATLAS_JET_JER_EffectiveNP_12restTerm", "ATLAS_JET_Pileup_OffsetMu", "ATLAS_JET_Pileup_OffsetNPV",
       "ATLAS_JET_Pileup_PtTerm", "ATLAS_JET_Pileup_RhoTopology", "ATLAS_JET_PunchThrough_MC16", "ATLAS_JET_SingleParticle_HighPt", "ATLAS_JET_fJvtEfficiency", "ATLAS_PRW_DATASF", "ATLAS_PS_qqZZ_CKKW_0Jet_Shape", "ATLAS_PS_qqZZ_CKKW_1Jet_Shape",
       "ATLAS_PS_qqZZ_CKKW_2Jet_Shape", "ATLAS_PS_qqZZ_QSF_Shape", "ATLAS_PS_ggZZ_CKKW_Norm", "ATLAS_PS_ggZZ_CKKW_Shape", "ATLAS_PS_ggZZ_CSSKIN_Norm", "ATLAS_PS_ggZZ_CSSKIN_Shape", "ATLAS_PS_ggZZ_QSF_Norm", "ATLAS_PS_ggZZ_QSF_Shape",
       "ATLAS_H4l_Shower_UEPS_VBF_OffShell", "ATLAS_HOEW_QCD_0Jet", "ATLAS_HOEW_QCD_1Jet", "ATLAS_HOEW_QCD_2Jet", "ATLAS_HOEW", "ATLAS_HOQCD_0Jet", "ATLAS_HOQCD_1Jet", "ATLAS_HOQCD_2Jet", "ATLAS_HOQCD_VBF", "ATLAS_VBF_PDF", "ATLAS_QCD_ggZZk_Norm",
       "ATLAS_QCD_ggZZk_Shape", "ATLAS_gg_PDF", "ATLAS_qq_PDF_0Jet", "ATLAS_qq_PDF_1Jet", "ATLAS_qq_PDF_2Jet", "ATLAS_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", "ATLAS_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
       "ATLAS_MUON_EFF_TrigStatUncertainty", "ATLAS_MUON_EFF_TrigSystUncertainty", "ATLAS_MET_SoftTrk_ResoPara", "ATLAS_MET_SoftTrk_ResoPerp", "ATLAS_FT_EFF_Eigen_B_0", "ATLAS_FT_EFF_Eigen_B_1",
       "ATLAS_FT_EFF_Eigen_B_2", "ATLAS_FT_EFF_Eigen_B_3", "ATLAS_FT_EFF_Eigen_B_4", "ATLAS_FT_EFF_Eigen_B_5", "ATLAS_FT_EFF_Eigen_B_6", "ATLAS_FT_EFF_Eigen_B_7", "ATLAS_FT_EFF_Eigen_B_8",
       "ATLAS_FT_EFF_Eigen_C_0", "ATLAS_FT_EFF_Eigen_C_1", "ATLAS_FT_EFF_Eigen_C_2", "ATLAS_FT_EFF_Eigen_C_3", "ATLAS_FT_EFF_Eigen_Light_0", "ATLAS_FT_EFF_Eigen_Light_1", "ATLAS_FT_EFF_Eigen_Light_2",
       "ATLAS_FT_EFF_Eigen_Light_3", "ATLAS_FT_EFF_extrapolation", "ATLAS_FT_EFF_extrapolation_from_charm", "ATLAS_JET_Flavor_Composition", "ATLAS_JET_Flavor_Response", "ATLAS_JET_JER_EffectiveNP_9",  "ATLAS_JET_JvtEfficiency",
       "ATLAS_MET_SoftTrk_Scale", "ATLAS_WZ_PDF_0Jet", "ATLAS_WZ_PDF_1Jet", "ATLAS_WZ_PDF_2Jet","ATLAS_ATLAS_PS_VBF_EIG", "ATLAS_PS_VBF_HAD", "ATLAS_PS_VBF_RE",
       "ATLAS_PS_VBF_PDF", "ATLAS_PS_qqZZ_CSSKIN_Norm", "ATLAS_PS_qqZZ_CSSKIN_Shape", "ATLAS_PS_qqZZ_QSF_Norm", "ATLAS_qqZZNLO_EW", "ATLAS_qqZZ_PDF_0Jet",
       "ATLAS_qqZZ_PDF_1Jet", "ATLAS_qqZZ_PDF_2Jet", "ATLAS_HOQCD_WZ_0Jet", "ATLAS_HOQCD_WZ_1Jet", "ATLAS_HOQCD_WZ_2Jet", "ATLAS_HOQCD_Zjets"};
    std::cout << "Made the Jay NP List" << std::endl;
    std::vector<double> jaysPullsNPs;
    if (lumiVal == 139.0){
        jaysPullsNPs = {-0.028277317083615246, -0.04057689010533464, 0.10369602885304745, 0.11066106781601417, -0.01070038669720914, -0.05759139700218771, -0.039368980796976906, 0.5389750637805085, -0.35797809100039923, 0.20192714938577377, 0.3119750126374231, -0.34065397302735523, 0.08902291167197891, -0.12265626665304194, -0.09637690460720441, -0.039706380932586105, -0.09290529968612696, -0.03373526893304729, -0.049685334887108606, -0.050419760428098634, -0.04376676978020435, 0.22378012646824283, 0.03879232998630511, 0.10630390150558454, 0.10758326132262591, -0.10897959543960042, -0.0028379359100459164, 0.11431385823033342, 0.11815194087567107, -0.14739965013029085, 0.2131143413627042, 0.5267357087866459, -0.46411726235064027, -0.32531435023980587, -0.10630155772201742, 0.07115133352005606, -0.13758632952538594, 0.015360317395978899, 0.06082579953899187, -0.04408293558549051, -0.19138766492421166, -0.04998098758399088, -0.06315685339836277, 0.25272849285388566, -0.0524907911301553, -0.46935315685699613, -0.10993653983666227, 0.0005480517066326902, -0.027944171055505323, 0.23626789670107323, -0.2991882518229628, -0.036770028863391396, -0.17627080691598332, -0.2051338696569478, -0.05143889369986522, -0.12036324644115359, 0.13613692361996083, 0.11139336643690637, 0.1974181069234188, -0.213246497746094, 0.1418124931083198, 0.046164060236882305, -0.15928099244585206, 0.005703217414649191, 0.028022178843108078, 0.015073922432917513, -0.2605904835707358, 0.07593053677512569, 0.009739070129780637, 0.11047471668654774, -0.1238551556982504, -0.05522619601358493, 0.08736245932540211, -0.2694307611782417, -0.08734056945760392, 0.2988457055106014, 0.22255049091117932, -0.10373253982659435, 0.0589675100652219, -0.18324467387631735, -0.022963698143251113, 0.12542490156363834, -0.09595508290568312, 0.01008181564580793, -0.052299910697367336, 0.0023535365823551466, -0.1428966470871945, -0.036892745852325395, 0.08386201945388114, 0.046127586103968665, 0.04902717670386127, 0.168580145738976, -0.02210869712145925, -0.030064319886695345, 0.04529172404444309, -0.18009054292510787, 0.135460661068939, 0.06599254978164655, 0.09024491707773363, -0.0792675310028688, 0.029778883434420044, -0.10196046636167728, -0.07091799955244524, -0.07057386812122683, 0.024698138674941413, 0.17801628648266543, -0.10758632942503409, -0.15534825109186184, -0.04007972536525407, -0.1296035966883202, -0.08943470544397047, 0.2062304815009452, 0.030195454621761614, -0.09351374542662536, 0.13286347620739783, -0.03730729501766865, -0.009331009163767445, -0.00016678167807109137, 0.01912980400614542, -0.01963682651102139, 0.08770610034598421, 0.10323520334183814, -0.0757368855199293, -0.006919896675076195, -0.014742474890948013, 0.006149342356215567, -0.09089024860922228};
    } else if (lumiVal == 140.1){
        // jaysPullsNPs = {-0.012496032454380154, -0.0400056254860768, 0.10364889192698343, 0.10950473756857282, -0.012188509679781004, -0.05936402505415373, -0.042149640010176286, 0.5383544091672853, -0.3590321917181704, 0.20123930287929676, 0.31177115336191824, -0.3394202316288195, 0.08891605646889278, -0.12469530547222472, -0.09700751407940876, -0.04026335339143542, -0.09240467065187614, -0.03369605300549336, -0.05907172300885323, -0.05097183601256459, -0.04444166005327034, 0.23161139920483537, 0.03823091256970539, 0.10146157363204515, 0.10701221722459532, -0.1114434302607561, -0.0029355500033677365, 0.11410854291177712, 0.11963938973625574, -0.1461722600997389, 0.2130797858927254, 0.5266114710412951, -0.4628738996459979, -0.3247038120932962, -0.10699059801015531, 0.07198271633150832, -0.1373054105777113, 0.016685744199550257, 0.06070597601946429, -0.043867969150196445, -0.19132761254596764, -0.04969160728232421, -0.06158560209222409, 0.2525729352559891, -0.052667888843327036, -0.46850310239427206, -0.1090463577627291, 0.0004891731123676131, -0.027530151020155772, 0.23574040986731898, -0.2987893528737023, -0.03683904755321762, -0.17579848152165808, -0.20437003578675447, -0.051284772873042124, -0.12004151106045265, 0.1363309319712283, 0.11120168621754883, 0.19544624741420435, -0.21259310948236235, 0.1420949837361368, 0.046987605409074616, -0.15915081435771844, 0.006085914111691074, 0.028344338971433444, 0.015105331494624246, -0.2596072421755275, 0.07623607288385792, 0.00979668624267281, 0.11033685343943991, -0.12511360599846577, -0.055223929884141564, 0.08711686320229929, -0.2682616234733659, -0.08705014024532631, 0.29838521869667856, 0.22257175861708992, -0.10337270016914561, 0.05861138163458018, -0.18321393086578597, -0.023079664817760248, 0.12529050077636913, -0.09603160800194672, 0.01020119672843432, -0.052204969727288074, 0.002676230074920253, -0.142507679160502, -0.03684333984463206, 0.08377526562649795, 0.046068606123347594, 0.04891758499853946, 0.16853405007962008, -0.022256875445474212, -0.029998146360469522, 0.045106289306806456, -0.1799598467613667, 0.13518932618395904, 0.06601722693559887, 0.09023998299357856, -0.0793377320708784, 0.029801263665942466, -0.10203667605526644, -0.07052099118084343, -0.07050004448316566, 0.024519442606201595, 0.17751936269735277, -0.10758199644059951, -0.15496057944837874, -0.039835834352765574, -0.12926087340989575, -0.0890156290610973, 0.20606640244719496, 0.03059045277065673, -0.09390816986570183, 0.1325124053660061, -0.03774778481709979, -0.01031973597844864, -0.00026526297392863023, 0.019060333308352096, -0.02009763773728091, 0.08748939448533492, 0.10295476384242913, -0.0762659270206395, -0.007330339624365804, -0.01418413618177592, 0.006687519753556338, -0.08957489446905423};
        // jaysPullsNPs = {-3.20367039e-02, -4.40858955e-02,  9.95628333e-02,  7.46524271e-02,
        // -6.66492560e-02, -9.81669036e-02, -5.84175224e-02,  5.80116436e-01,
        // -4.47206522e-01,  2.10768283e-01,  3.73772639e-01, -4.66864600e-01,
        // 9.80907626e-02, -5.12345537e-02, -7.41856280e-03, 5.07037188e-02,
        // -1.18165996e-01, -8.06251348e-02, -3.75791529e-01, -4.93932476e-02,
        // 2.65319785e-04, -1.20076235e-01,  3.06605948e-02, 1.04923080e-02,
        // 1.25479025e-01, -1.65691765e-01,  1.42808167e-03,  1.55043637e-01,
        // 1.36918170e-01, -2.14112734e-01,  2.30971866e-01,  6.30249354e-01,
        // -4.85359976e-01, -4.04467422e-01, -1.57631392e-01,  4.42478421e-03,
        // -1.12297184e-01, -3.08989805e-02,  5.31327564e-02, -3.27631747e-02,
        // -3.00698862e-01, -4.99012446e-02, -9.31470713e-02,  2.74018254e-01,
        // -4.17308253e-02, -4.87784748e-01, -1.11095223e-01,  4.27705897e-02,
        // -3.39385891e-02,  2.71389234e-01, -3.65711190e-01, -1.20864583e-01,
        // -2.53924794e-01, -2.98630618e-01, -4.46159753e-02, -1.50141513e-01,
        // 1.44157582e-01,  1.14760273e-01,  1.86704871e-01, -2.21811726e-01,
        // 1.50748884e-01, -1.05639047e-02, -1.25852286e-01,  1.41661482e-02,
        // 4.04906022e-02,  2.16563764e-02, -2.69399710e-01,  1.00523984e-01,
        // 1.08669223e-02,  1.08912065e-01, -5.69494574e-02, -3.92690717e-02,
        // 5.08167793e-02, -2.19391630e-01, -1.18252902e-01,  3.15269858e-01,
        // 2.32112573e-01, -1.33670675e-01,  2.92224233e-02, -2.04127253e-01,
        // 6.35864558e-03,  1.33746712e-01, -1.14816924e-01, -2.23035607e-02,
        // -3.99855287e-02, -3.37469314e-02, -1.72389488e-01, -4.30516392e-02,
        // 6.93789740e-02,  5.52248979e-02,  8.04405298e-02,  1.68223574e-01,
        // -3.95211382e-02, -6.75410592e-03,  7.40481936e-02, -1.88773016e-01,
        // 1.93995074e-01,  7.81295773e-02,  1.01448326e-01, -8.53560389e-02,
        // 6.98526254e-02, -1.17644953e-01, -8.85460642e-02, -3.56314534e-02,
        // 5.20355941e-03,  2.14956948e-01, -1.18834961e-01, -1.87674368e-01,
        // -5.47204516e-02, -1.77156317e-01, -1.29423863e-01,  2.08824866e-01,
        // 5.12781546e-02, -6.55950408e-02, 1.32381225e-01, -3.08716828e-02,
        // -7.89168403e-03, -9.79177332e-03,  6.91127626e-04, -9.76821080e-03,
        // 5.36595380e-02,  1.48506008e-01, -1.14037932e-01, 2.59820296e-03,
        // -3.19991349e-02,  1.24177558e-02, -1.50362689e-01};
        jaysPullsNPs = {-2.29179944e-02, -3.98599784e-02,
        -1.69479630e-01, -1.19651979e-01,  1.33152839e-01,  8.62146319e-02,
        4.85883502e-02,  6.89084478e-02,  1.51341101e-01, -2.69191609e-02,
        -1.93222089e-02,  7.30546690e-02, -1.71481901e-01,  1.64799791e-01,
        -1.02626519e-01, -1.55396132e-02, -4.67613197e-02, -2.68400377e-02,
        -1.62117554e-01, -4.81442437e-02,  7.10303016e-02, -1.23164723e-01,
        -1.85357227e-01, -5.17553643e-02, -1.64671437e-01, -1.04531164e-01,
        1.97808804e-01,  3.18477831e-02, -7.77601672e-02,  1.54000641e-01,
        9.65441912e-02, -8.72307029e-02,  5.27325295e-02, -1.07953642e-01,
        -9.20219045e-02, -6.60482770e-02,  1.97201035e-02,  2.08019422e-01,
        -3.08886111e-02, -1.13994131e-02,  1.68634596e-02,  3.75317972e-02,
        7.32912549e-02, -2.87078663e-02, -7.26173126e-02,  1.30908228e-01,
        -8.38547116e-03, -2.74824259e-02,  2.56800655e-01, -2.11684851e-01,
        -6.40144883e-02, -3.99128194e-01, -6.95843623e-02, -1.44033394e-01,
        1.80928821e-01, -1.47806827e-01, -6.34380065e-01, -1.55865197e-01,
        -1.28674423e-01, -2.12304289e-02,  1.87258321e-01, -4.14088489e-01,
        -1.11358975e-01, -3.04937528e-01, -3.47302010e-01, -1.34545616e-01,
        -2.21724728e-01,  8.95447093e-02,  1.75076877e-02,  4.27454278e-01,
        -3.12102163e-01,  7.37164158e-02, -3.09708177e-02, -1.89043026e-01,
        -3.13460183e-02,  1.62105628e-02,  2.95826332e-02, -6.96113902e-03,
        1.16175891e-02,  8.93396870e-02,  9.13741171e-02,  4.11665180e-01,
        2.50575319e-01, -6.32898763e-01, -2.56197449e-01, -2.59203058e-01,
        2.54866327e-01, -3.21620536e-01,  6.19616732e-02,  1.04408968e-01,
        1.08911469e-01, -6.07725479e-02,  6.06156824e-01,  5.79681659e-02,
        4.37311255e-02,  6.69974831e-02, -6.06151896e-02, -1.99796204e-01,
        2.49182120e-01,  4.89910825e-02, -1.64173616e-02, -1.21286865e-01,
        -5.96760968e-02, -1.81530995e-02, -1.43048670e-01, -9.40512597e-02,
        -5.48192320e-02,  6.42037882e-02,  1.01906090e-01,  3.35978534e-01,
        8.83589312e-02, -2.11249126e-03,  2.07350567e-01,  4.26964289e-01,
        -3.75069356e-01,  9.91629078e-02,  3.28431587e-01,  6.16479939e-01,
        -2.85373720e-01, -1.72387313e-01, -7.05918463e-02, -1.81204234e-01,
        -9.01099720e-02, -4.40744815e-02, -5.02395715e-02,  1.02632563e-01,
        9.86604203e-02,  0.00000000e+00,  0.00000000e+00, -3.85502956e-03,
        -6.29536133e-03,  3.08907551e-02,  3.40160664e-02, -4.02614628e-02,
        -1.01164162e-02,  9.28103882e-03, -2.34489181e-03, -2.00261893e-04,
        5.40632839e-06,  1.12336296e-23,  1.12336296e-23,  1.12336296e-23,
        1.69352834e-02,  1.71129770e-03, -2.17818481e-03, -5.92623112e-05,
        1.96272919e-02, -5.74937101e-04,  1.09938720e-04,  1.48102649e-04,
        -1.41668833e-04, -4.22234560e-02,  1.15173761e-01, -1.66608195e-01,
        -1.67917946e-02,  1.92932344e-02,  5.95512098e-02, -8.40448722e-03,
        -4.30224233e-03, -5.39239545e-03,  5.18225294e-03, -4.69601072e-04,
        5.11703309e-03, -6.47641394e-05,  3.09096090e-02, -1.78387126e-01,
        5.66832080e-02,  2.16180770e-01,  7.76152098e-02,  6.32350484e-02,
        5.26300668e-02,  7.15170634e-02, -5.80053249e-02, -3.27983697e-02,
        -9.77668636e-03};
    }
    std::cout << "Made the Jay NP Value vector" << std::endl;
    assert(jaysNPlist.size() == jaysPullsNPs.size());
    std::map<std::string, double> jayNPpullsMap;
    int i=0;
    for (auto npName : systematicsList){
        jayNPpullsMap[npName] = jaysPullsNPs[i];
        i++;
    }
    
    double jaysPullsConstSum = 0.0;
    // for (const auto& [key, value] : jayNPpullsMap){
    for (auto npName : systematicsList){
        // std::string alphaName = "alpha_"+key;
        std::string alphaName = "alpha_"+npName;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(jayNPpullsMap.at(npName));
        // jaysPullsConstSum += value*value;
        jaysPullsConstSum += jayNPpullsMap.at(npName)*jayNPpullsMap.at(npName);
    }
    jaysPullsConstSum /= 2;
    std::vector<double> jaysPullsPOIs;
    if (lumiVal == 139.0){
        jaysPullsPOIs = {0.8970120806742395, 1.1400534849538946, 0.8502735118360424, 0.8978423011657017};
    } else if (lumiVal == 140.1){
        // jaysPullsPOIs = {0.8952429716921346, 1.1302247129896659, 0.849765948930971, 0.8966767580943703};
        // jaysPullsPOIs = {0.0,  1.12097043e+00,  8.44419079e-01,  8.96774171e-01};
        jaysPullsPOIs = {1.05515586e+00, 1.11278701e+00,  8.51480180e-01,  8.91379268e-01};
    }
    mu->setVal(jaysPullsPOIs[0]);
    mu_qqZZ->setVal(jaysPullsPOIs[1]);
    mu_qqZZ_1->setVal(jaysPullsPOIs[2]);
    mu_qqZZ_2->setVal(jaysPullsPOIs[3]);
    std::cout << "Mu and Alpha values set" << std::endl;

    nps.Print("v");

    // Calculate the NLL from these values
    RooNLLVar* nllJaysPulls = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLcombData = (RooNLLVar*)constraintPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllSRJaysPulls = (RooNLLVar*)pdf_SR->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR0JaysPulls = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR1JaysPulls = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR2JaysPulls = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllconstCR0JaysPulls = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllconstCR1JaysPulls = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllconstCR2JaysPulls = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooArgSet* argSetCR0 = new RooArgSet(*theCRobservables.at("CR0"));
    RooArgSet* argSetCR1 = new RooArgSet(*theCRobservables.at("CR1"));
    RooArgSet* argSetCR2 = new RooArgSet(*theCRobservables.at("CR2"));
    std::cout << "nllJaysPulls value: " << nllJaysPulls->getVal() << std::endl;
    std::cout << "Total NLL just from createNLL(): " << simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal() << std::endl;
    std::cout << "nllSRJaysPulls value: " << 2*nllSRJaysPulls->getVal() << std::endl;
    std::cout << "SR Expected Events: " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "Expected Events Var Value: " << expectedEventsSaveSR->getVal() << std::endl;
    std::cout << "SR Extended Term: " << 2*pdf_SR->extendedTerm(*dataSetSR, false, false) << std::endl;
    std::cout << "nllCR0JaysPulls value: " << 2*nllCR0JaysPulls->getVal() << std::endl;
    std::cout << "nllCR1JaysPulls value: " << 2*nllCR1JaysPulls->getVal() << std::endl;
    std::cout << "nllCR2JaysPulls value: " << 2*nllCR2JaysPulls->getVal() << std::endl;
    std::cout << "nllconstCR0JaysPulls value: " << 2*nllconstCR0JaysPulls->getVal() << std::endl;
    std::cout << "nllconstCR1JaysPulls value: " << 2*nllconstCR1JaysPulls->getVal() << std::endl;
    std::cout << "nllconstCR2JaysPulls value: " << 2*nllconstCR2JaysPulls->getVal() << std::endl;
    std::cout << "All Individual NLLs added together: " << 2*(nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal()) << std::endl;
    std::cout << "Pdf SR EE: " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "PDF Constrained CR0 EE: " << constrainedCR0->expectedEvents(argSetCR0) << std::endl;
    std::cout << "PDF Constrained CR1 EE: " << constrainedCR1->expectedEvents(argSetCR1) << std::endl;
    std::cout << "PDF Constrained CR1 EE: " << constrainedCR2->expectedEvents(argSetCR2) << std::endl;
    std::cout << "PDF Regular CR0 EE: " << theCRpdfs.at("CR0")->expectedEvents(argSetCR0) << std::endl;
    std::cout << "PDF Regular CR1 EE: " << theCRpdfs.at("CR1")->expectedEvents(argSetCR1) << std::endl;
    std::cout << "PDF Regular CR1 EE: " << theCRpdfs.at("CR2")->expectedEvents(argSetCR2) << std::endl;
    std::cout << "SimPdf Val: " << (nllSRJaysPulls->getVal() + nllconstCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() - (pdf_SR->expectedEvents(nullptr) + constrainedCR0->expectedEvents(argSetCR0) + constrainedCR1->expectedEvents(argSetCR1) + constrainedCR2->expectedEvents(argSetCR2))*std::log(4)) << std::endl;
    std::cout << "Difference between constNLLcombData value with pull alphas and for all alphas = 0.0: " << 2*(constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "Hand-Calculated Constraint Sum: " << 2*jaysPullsConstSum << std::endl;
    std::cout << "Number I need to reproduce: " << 2*(nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() + constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    double pi = 3.1415926535;
    std::cout << "Number I need from simPdf NLL - shift: " << 2*(nllJaysPulls->getVal() - combinedData->sumEntries()*std::log(4) - nps.getSize()*std::log(std::sqrt(2*pi))) << std::endl;

    jaysPullsPOIs = {0.8970120806742395, 1.1400534849538946, 0.8502735118360424, 0.8978423011657017};
    mu->setVal(jaysPullsPOIs[0]);
    mu_qqZZ->setVal(jaysPullsPOIs[1]);
    mu_qqZZ_1->setVal(jaysPullsPOIs[2]);
    mu_qqZZ_2->setVal(jaysPullsPOIs[3]);
    double newJaysPullsValues = 0.0;
    double testNPVal = 0.01;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(testNPVal);
        newJaysPullsValues += testNPVal * testNPVal;
        // testNPVal += 0.01;
    }
    newJaysPullsValues/=2;
    nps.Print("v");
    std::cout << std::endl;

    RooArgList testN = pdf_SR->GetEventYieldsN();
    RooArgList testNPs = pdf_SR->GetNPs();
    RooArgList testMultipliers = pdf_SR->GetMultipliers();
 
    std::cout << "testN: " << std::endl;
    testN.Print("v");
    std::cout << std::endl;
    std::cout << "testNPs: " << std::endl;
    testNPs.Print("v");
    std::cout << std::endl;
    std::cout << "testMultipliers: " << std::endl;
    testMultipliers.Print("v");
    std::cout << std::endl;

    RooNLLVar* newSRNLL = (RooNLLVar*)pdf_SR->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));

    std::cout << "Now with custom setting:" << std::endl;
    std::cout << "nllJaysPulls value: " << nllJaysPulls->getVal() << std::endl;
    std::cout << "Total NLL just from createNLL(): " << simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()))->getVal() << std::endl;
    std::cout << "nllSRJaysPulls value, original variable: " << 2*nllSRJaysPulls->getVal() << std::endl;
    std::cout << "nllSRJaysPulls value, new variable: " << 2*newSRNLL->getVal() << std::endl;
    std::cout << "SR Expected Events: " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "SR Extended Term: " << 2*pdf_SR->extendedTerm(*dataSetSR, false, false) << std::endl;
    std::cout << "nllCR0JaysPulls value: " << 2*nllCR0JaysPulls->getVal() << std::endl;
    std::cout << "nllCR1JaysPulls value: " << 2*nllCR1JaysPulls->getVal() << std::endl;
    std::cout << "nllCR2JaysPulls value: " << 2*nllCR2JaysPulls->getVal() << std::endl;
    std::cout << "All Individual NLLs added together: " << 2*(nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal()) << std::endl;
    std::cout << "Difference between constNLLcombData value with pull alphas and for all alphas = 0.0: " << 2*(constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "Hand-Calculated Constraint Sum: " << 2*newJaysPullsValues << std::endl;
    std::cout << "Number I need to reproduce, old var: " << 2*(nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() + constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "Number I need to reproduce, new var: " << 2*(newSRNLL->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() + constNLLcombData->getVal() - constValAlpha0p0) << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}