#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TRandom2.h"
#include "TLegend.h"
#include "TString.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMarker.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooUniform.h"
#include "RooExponential.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooStringView.h"
#include "RooAddPdf.h"
#include "RooPoisson.h"
#include "RooProduct.h"
#include "RooArgList.h"
#include "RooFitLegacy/RooTreeData.h"
#include "RooUniform.h"
#include "RooFormulaVar.h"
#include "RooNLLVar.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooAbsTestStatistic.h"
#include "RooRealConstant.h"
#include "higgsOffshellDecayCombinations/RooDensityRatio.h"
#include "higgsOffshellDecayCombinations/ReadBinary.h"
#include "higgsOffshellDecayCombinations/TextToPoissonParams.h"
#include "higgsOffshellDecayCombinations/HandleSystematics.h"
#include "higgsOffshellDecayCombinations/RooTreeDataStoreStar.h"
#include "higgsOffshellDecayCombinations/RooDataSetStar.h"
#include "higgsOffshellDecayCombinations/RooNLLVarStar.h"
#include "Math/MinimizerOptions.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdexcept>
#include <limits>

using namespace RooFit;

#undef debug

int main (int argc, const char* argv[]){

    // Parse Args
    (void)argc;
    double lumiVal = std::atof(argv[1]);
    const char *dataPlusYieldsPlusCRNobsFilename = argv[2];
    const char *crYieldsVarsFilename = argv[3];

#ifdef debug
    std::cout << std::fixed;
    std::cout << std::setprecision(12);
#endif

    std::cout << "Creating simPdf..." << std::endl;
    // Define Infinity as a float for RooRealVar limits
    float inf = std::numeric_limits<float>::infinity();

    // Define the luminosity
    RooRealVar* ATLAS_LUMI= new RooRealVar("ATLAS_LUMI", "ATLAS_LUMI", lumiVal);

    // Define POIs
    std::vector<const char*> pois_list = {"mu", "mu_ggF", "mu_VBF", "mu_qqZZ", "mu_qqZZ_1", "mu_qqZZ_2"};
    RooArgSet pois;
    Double_t muVal = 1.0;
    RooRealVar* mu = new RooRealVar("mu", "mu", muVal, 0., 10.);
    RooRealVar* mu_ggF = new RooRealVar("mu_ggF", "mu_ggF", 1.0, 0., inf);
    mu_ggF->setConstant(true);
    RooRealVar* mu_VBF = new RooRealVar("mu_VBF", "mu_VBF", 1.0, 0., inf);
    mu_VBF->setConstant(true);
    RooRealVar* mu_qqZZ = new RooRealVar("mu_qqZZ", "mu_qqZZ", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_1 = new RooRealVar("mu_qqZZ_1", "mu_qqZZ_1", 1.0, 0., 10.);
    RooRealVar* mu_qqZZ_2 = new RooRealVar("mu_qqZZ_2", "mu_qqZZ_2", 1.0, 0., 10.);
    pois.add(*mu);
    pois.add(*mu_ggF);
    pois.add(*mu_VBF);
    pois.add(*mu_qqZZ);
    pois.add(*mu_qqZZ_1);
    pois.add(*mu_qqZZ_2);

    // Include a list of the Decay Processes
    std::vector<std::string> processList = {"S", "SBI", "B", "EWB", "EWSBI", "EWSBI10", "qqZZ_0", "qqZZ_1", "qqZZ_2", "ttV"};

    // Make Formulas of Multipliers for CR yields and for SR class
    RooFormulaVar* f_S = new RooFormulaVar("f_s", "@0*@1 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_SBI = new RooFormulaVar("f_sbi", "sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"]));
    RooFormulaVar* f_B = new RooFormulaVar("f_b", "1.0 - sqrt(@0*@1)", RooArgList(pois["mu"], pois["mu_ggF"])); 
    RooFormulaVar* f_EWB = new RooFormulaVar("f_ewb", "(1.0/(-10.0+sqrt(10.0)))*((1.0-sqrt(10.0))*@0*@1+9.0*sqrt(@0*@1)-10.0+sqrt(10.0))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI = new RooFormulaVar("f_ewsbi", "(1.0/(-10.0+sqrt(10.0)))*(sqrt(10.0)*@0*@1-10.0*sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_EWSBI10 = new RooFormulaVar("f_ewsbi10", "(1.0/(-10.0+sqrt(10.0)))*(-(@0*@1)+sqrt(@0*@1))", RooArgList(pois["mu"], pois["mu_VBF"]));
    RooFormulaVar* f_qqZZ_0 = new RooFormulaVar("f_qqZZ_0", "@0", RooArgList(pois["mu_qqZZ"])); 
    RooFormulaVar* f_qqZZ_1 = new RooFormulaVar("f_qqZZ_1", "@0*@1", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"])); 
    RooFormulaVar* f_qqZZ_2 = new RooFormulaVar("f_qqZZ_2", "@0*@1*@2", RooArgList(pois["mu_qqZZ"], pois["mu_qqZZ_1"], pois["mu_qqZZ_2"])); 
    RooFormulaVar* f_ttV = new RooFormulaVar("f_ttV", "@0", RooRealConstant::value(1.0));
    RooArgList multipliers(*f_S, *f_SBI, *f_B, *f_EWB, *f_EWSBI, *f_EWSBI10, *f_qqZZ_0, *f_qqZZ_1, *f_qqZZ_2, *f_ttV);

    // Set Systematics info
    std::vector<std::string> systematicsList = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", 
    "gg_PDF_var", "EW_PDF_var", "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", 
    "HOEW_QCD_syst_2jet", "HOEW_syst", "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
    "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", 
    "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
    "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", 
    "H4l_Shower_UEPS_VBF_OffShell", "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", "JET_JER_EffectiveNP_7", 
    "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
    "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3", "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", 
    "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", "JET_EffectiveNP_Statistical1", 
    "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
    "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
    "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", 
    "JET_EtaIntercalibration_NonClosure_negEta", "JET_EtaIntercalibration_NonClosure_posEta", 
    "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
    "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", 
    "JET_Pileup_OffsetMu", "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", 
    "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", "MUON_MS", "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
     "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", 
    "EL_EFF_ID_CorrUncertaintyNP14", "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", 
    "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", "EL_EFF_ID_CorrUncertaintyNP4", 
    "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
    "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", 
    "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", 
    "MUON_EFF_RECO_STAT", "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", 
    "JET_fJvtEfficiency", "PRW_DATASF"};

    // Get constraint pdf, nps
    HandleSystematics SH(systematicsList);
    RooArgList nps = SH.GetNuisanceParams();
    double npVal = 0.0;
    for (auto np : nps){
        ((RooRealVar*)np)->setVal(npVal);
    }
    RooProdPdf* constraintPdf = SH.ComputeConstraintPdf("constraintPdf", "constraintPdf");

    // Set Up Everything for the Signal Region
    ReadBinary srHandle(dataPlusYieldsPlusCRNobsFilename, nps, processList);
    RooArgList eventYieldsNList = srHandle.createNYieldsList(ATLAS_LUMI);
    RooArgList eventYieldsNuList = srHandle.createNuYieldsList(ATLAS_LUMI);
    RooArgList gDown = srHandle.createGArgList("down");
    RooArgList gUp = srHandle.createGArgList("up");
    RooArgSet observables = srHandle.GetObservables();
    RooDataSet* dataSetSR = srHandle.createRooDataSet("testSetSR", "testSetSR");

    RooArgList procRatiosSR(observables["r_S"], observables["r_SBI"], observables["r_B"], observables["r_EWB"],
                             observables["r_EWSBI"], observables["r_EWSBI10"], observables["r_qqZZ_0"], 
                             observables["r_qqZZ_1"], observables["r_qqZZ_2"], observables["r_ttV"]);
    RooRealVar* total_weight = (RooRealVar*)observables.find("total_weight");

    // Set the expected Events formula variable externally
    RooFormulaVar* expectedEventsSaveSR = new RooFormulaVar("expectedEventsSaveSR", 
                "@0*@1 + @2*@3 + @4*@5 + @6*@7 + @8*@9 + @10*@11 + @12*@13 + @14*@15 + @16*@17 + @18*@19",
                RooArgList(*eventYieldsNuList.at(0), *multipliers.at(0), *eventYieldsNuList.at(1), *multipliers.at(1), *eventYieldsNuList.at(2), *multipliers.at(2),
                           *eventYieldsNuList.at(3), *multipliers.at(3), *eventYieldsNuList.at(4), *multipliers.at(4), *eventYieldsNuList.at(5), *multipliers.at(5),
                           *eventYieldsNuList.at(6), *multipliers.at(6), *eventYieldsNuList.at(7), *multipliers.at(7), *eventYieldsNuList.at(8), *multipliers.at(8),
                           *eventYieldsNuList.at(9), *multipliers.at(9)));

    RooDensityRatio* pdf_SR = new RooDensityRatio("pdf_SR", "pdf_SR", nps, eventYieldsNList, procRatiosSR, multipliers, gDown, gUp, *expectedEventsSaveSR);

    // Get the Control Region Pdfs
    bool runOnData = true;
    TextToPoissonParams crHandle(dataPlusYieldsPlusCRNobsFilename, crYieldsVarsFilename, nps, processList, runOnData);
    std::map<std::string, RooAddPdf*> theCRpdfs = crHandle.generateCRpdfs(multipliers, ATLAS_LUMI, total_weight);
    std::map<std::string, RooRealVar*> theCRobservables = crHandle.GetCRObservables();
    std::map<std::string, RooDataSet*> theDataSets = crHandle.GetCRDatasets();
    // Add the SR dataset to the Map of All Datasets
    theDataSets["SR"] = dataSetSR; 

    // Constrain the CR pdfs
    RooProdPdf* constrainedCR0 = new RooProdPdf("constrainedCR0", "constrainedCR0", RooArgList(*theCRpdfs.at("CR0"), *constraintPdf));
    RooProdPdf* constrainedCR1 = new RooProdPdf("constrainedCR1", "constrainedCR1", RooArgList(*theCRpdfs.at("CR1"), *constraintPdf));
    RooProdPdf* constrainedCR2 = new RooProdPdf("constrainedCR2", "constrainedCR2", RooArgList(*theCRpdfs.at("CR2"), *constraintPdf));

    // Create RooSimultaneous with Combined Dataset Acc. To Category
    RooCategory* cat = new RooCategory("cat", "cat", {{"CR0", 0}, {"CR1", 1}, {"CR2", 2}, {"SR", 3}});

    RooSimultaneous* simPdf = new RooSimultaneous("simPdf", "simPdf", RooArgList(*constrainedCR0,*constrainedCR1,*constrainedCR2,*pdf_SR), *cat);
    observables.add(*theCRobservables.at("CR0"));
    observables.add(*theCRobservables.at("CR1"));
    observables.add(*theCRobservables.at("CR2"));
    observables.add(*cat);

    RooDataSet* combinedData = new RooDataSet("realDataSet", "realDataSet", observables, 
					      RooFit::WeightVar(*total_weight), RooFit::Index(*cat), RooFit::Import(theDataSets));
    std::cout << "simPdf and combinedData created." << std::endl;

#ifdef debug
    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }

    std::cout << "Systematics List: ";
    for (auto i=0lu; i<systematicsList.size(); i++){
        std::cout << systematicsList[i] << ", ";
        if (i%5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Total Number of Systematics: " << systematicsList.size() << std::endl;

    RooNLLVar* someNLLjustConstrainedSR = (RooNLLVar*)constraintPdf->createNLL(*dataSetSR, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with SR DataSet: " << someNLLjustConstrainedSR->getVal() << std::endl;

    std::cout << "All Event Yields Nu for SR: " << std::endl;
    for (auto nu : eventYieldsNuList){
        std::cout << nu->GetName() << ": " << ((RooRealVar*)nu)->getVal() << std::endl;
    }

    std::cout << "N Yields Values and their values: " << std::endl;
    for (auto n : eventYieldsNList){
        std::cout << n->GetName() << ": " << ((RooRealVar*)n)->getVal() << std::endl;
    }

    std::cout << "Multipliers and their values: " << std::endl;
    for (auto mult : multipliers){
        std::cout << mult->GetName() << ": " << ((RooRealVar*)mult)->getVal() << std::endl;
    }

    std::cout << "SR Expected Events Save Value: " << expectedEventsSaveSR->getVal() << std::endl;
    double srSumEntries = dataSetSR->sumEntries();
    std::cout << "DataSet SR Sum Entries: " << srSumEntries << std::endl;
    std::cout << "SR Hand-Calculated Extended Term: " << expectedEventsSaveSR->getVal() - srSumEntries*std::log(expectedEventsSaveSR->getVal()) << std::endl;

    std::cout << "SR Extended Term: " << pdf_SR->extendedTerm(*dataSetSR, false, false) << std::endl;

    RooNLLVar* testNLLsr = (RooNLLVar*)pdf_SR->createNLL(*dataSetSR, RooFit::Verbose(true));
    std::cout << "NLL of Unconstrained SR: " << testNLLsr->getVal() << std::endl;

    theDataSets.at("CR0")->Print("v");
    std::cout << theDataSets.at("CR0")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR0")->sumEntries() << std::endl;
    theDataSets.at("CR1")->Print("v");
    std::cout << theDataSets.at("CR1")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR1")->sumEntries() << std::endl;
    theDataSets.at("CR2")->Print("v");
    std::cout << theDataSets.at("CR2")->weightVar()->getVal() << std::endl;
    std::cout << theDataSets.at("CR2")->sumEntries() << std::endl;

    RooNLLVar* test0 = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Verbose(true));
    // test0->Print("v");
    std::cout << "Just the CR0 NLL: " << test0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test1 = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Verbose(true));
    // test1->Print("v");
    std::cout << "Just the CR1 NLL: " << test1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* test2 = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Verbose(true));
    // test2->Print("v");
    std::cout << "Just the CR2 NLL: " << test2->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* someNLLjustConstrained0 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR0 DataSet: " << someNLLjustConstrained0->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained1 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR1 DataSet: " << someNLLjustConstrained1->getVal() << std::endl;

    RooNLLVar* someNLLjustConstrained2 = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Constraint NLL with CR2 DataSet: " << someNLLjustConstrained2->getVal() << std::endl;

    RooNLLVar* testCon0 = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR0 Constrained NLL: " << testCon0->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon1 = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR1 Constrained NLL: " << testCon1->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCon2 = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "CR2 Constrained NLL: " << testCon2->getVal() << std::endl;
    std::cout << std::endl;

    RooNLLVar* testCombined = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "Combined NLL Value, no shift: " << testCombined->getVal() << std::endl;
    std::cout << "Constrained CR0 Expected Events: " << constrainedCR0->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR1 Expected Events: " << constrainedCR1->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained CR2 Expected Events: " << constrainedCR2->expectedEvents(&pois) << std::endl;
    std::cout << "Constrained SR Expected Events: " << pdf_SR->expectedEvents(nullptr) << std::endl;
    std::cout << "Shifted Combined NLL Value: " << testCombined->getVal() - (constrainedCR0->expectedEvents(&pois) + constrainedCR1->expectedEvents(&pois) + constrainedCR2->expectedEvents(&pois) + pdf_SR->expectedEvents(nullptr))*std::log(4) << std::endl;

    double eeCR0 = constrainedCR0->expectedEvents(&pois);
    double eeCR1 = constrainedCR1->expectedEvents(&pois);
    double eeCR2 = constrainedCR2->expectedEvents(&pois);
    double eeSR = pdf_SR->expectedEvents(nullptr);
    double combShift = (eeCR0 + eeCR1 + eeCR2 + eeSR)*std::log(4);
    double nllCR0 = testCon0->getVal();
    double nllCR1 = testCon1->getVal();
    double nllCR2 = testCon2->getVal();
    double nllSR = testNLLsr->getVal();
    double nllConst = someNLLjustConstrainedSR->getVal();
    double nllSum = nllCR0 + nllCR1 + nllCR2 + nllSR + nllConst;
    std::cout << "combShift: " << combShift << std::endl;
    std::cout << "nllSum: " << nllSum << std::endl;
    std::cout << "nllSum - combShift: " << nllSum - combShift << std::endl;
#endif 

    std::cout << std::fixed;
    std::cout << std::setprecision(15);

    // Perform a Global Fit to Get My Pulls
    RooNLLVar* nllFull = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLcombData = (RooNLLVar*)constraintPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLsrData = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLcr0Data = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLcr1Data = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* constNLLcr2Data = (RooNLLVar*)constraintPdf->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "nllFull value: " << nllFull->getVal() << std::endl;
    std::cout << "constNLL value: " << constNLLcombData->getVal() << std::endl;
    std::cout << "constNLLsrData value: " << constNLLsrData->getVal() << std::endl;
    std::cout << "constNLLcr0Data value: " << constNLLcr0Data->getVal() << std::endl;
    std::cout << "constNLLcr1Data value: " << constNLLcr1Data->getVal() << std::endl;
    std::cout << "constNLLcr2Data value: " << constNLLcr2Data->getVal() << std::endl;
    std::cout << "nllFull value without the added constraint term: " << nllFull->getVal() - constNLLcombData->getVal() << std::endl;
    std::cout << "nllFull value without the added constraint term, SR data: " << nllFull->getVal() - constNLLsrData->getVal() << std::endl;
    std::cout << "nllFull value without the added constraint term, CR0 data: " << nllFull->getVal() - constNLLcr0Data->getVal() << std::endl;
    std::cout << "nllFull value without the added constraint term, CR1 data: " << nllFull->getVal() - constNLLcr1Data->getVal() << std::endl;
    std::cout << "nllFull value without the added constraint term, CR2 data: " << nllFull->getVal() - constNLLcr2Data->getVal() << std::endl;
    double constValAlpha0p0 = constNLLcombData->getVal();

    /* Set the Initial Values to Jay's Pulls to Optimize The Minimizer */
    /*                                                                 */
    std::vector<std::string> startNPlist = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", "gg_PDF_var", "EW_PDF_var", 
            "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", "HOEW_QCD_syst_2jet", "HOEW_syst", 
            "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
            "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", "H4l_Shower_UEPS_VBF_OffShell", 
            "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11", "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", 
            "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3", "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", 
            "JET_JER_EffectiveNP_7", "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
            "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2", "JET_EffectiveNP_Mixed3", 
            "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", 
            "JET_EffectiveNP_Statistical1", "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
            "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
            "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", "JET_EtaIntercalibration_NonClosure_negEta", 
            "JET_EtaIntercalibration_NonClosure_posEta", "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
            "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", "JET_Pileup_OffsetMu", 
            "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", 
            "MUON_MS", "MUON_SAGITTA_RESBIAS", "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
            "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", "EL_EFF_ID_CorrUncertaintyNP14", 
            "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", 
            "EL_EFF_ID_CorrUncertaintyNP4", "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
            "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
            "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", "MUON_EFF_RECO_STAT", 
            "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", "JET_fJvtEfficiency", "PRW_DATASF"};
    std::vector<double> startNPs = {-0.012304123211624683, -0.05676484099486917, 0.10939412502257628, 0.09638654928806716, -0.0058844252493451925, -0.06036371391323871, -0.03773664535473304, 0.5793434693040871, -0.4212014447990021, 0.21307168687755318, 0.3836150694261321, -0.39502421103420504, 0.0944781062049346, -0.1144102187662508, -0.09140647609202203, -0.025362269145717015, -0.1324008500628735, -0.03277737220725146, -0.05614153586944149, -0.050788955850813965, -0.04672728958667602, 0.2338628653997735, 0.05365856229758719, 0.09159777655918631, 0.11682165071070977, -0.113644163528267, -0.0035655684639608336, 0.14369059995211889, 0.15687110738196564, -0.15961590447182816, 0.2182628855898944, 0.5987569298109362, -0.5313331405754305, -0.4038120247234137, -0.14977323516550634, 0.07562245792050515, -0.14845436970025247, -0.009304755341245707, 0.058816132370854146, -0.021001200725458493, -0.182495970531472, -0.05329179807718245, -0.0717892265524447, 0.29797915399877106, -0.06653055504789945, -0.5288158190703922, -0.13966120138674182, -0.029037186337836716, -0.027018665723132315, 0.2507913141952367, -0.35337273407308145, -0.047177334812360056, -0.20920348095519858, -0.28011238051784765, -0.05924563762028058, -0.13650632777024027, 0.15768386558782635, 0.1272509299633453, 0.20547795351572848, -0.25356793466740873, 0.16108384642245754, 0.026054324889089906, -0.1425065072428116, -0.007767656727963554, 0.01846641808500754, 0.01468159423410477, -0.25602516497379013, 0.10952903594904492, 0.009830068386587098, 0.10821577323867902, -0.10431604772509426, -0.058840845017583104, 0.0992555238152754, -0.2740406663589481, -0.14069961136070505, 0.32728545460242153, 0.2297893680452763, -0.11148346923574244, 0.06234896055040964, -0.18047187761570105, -0.030666006444667514, 0.13473017969716183, -0.10020347619081518, -0.013779689663337223, -0.04750234474811328, -0.01926876231491791, -0.16647182416480644, -0.039757208238752284, 0.08454565173267604, 0.04379765117371479, 0.06987027817753091, 0.15217488354939762, -0.02752346419457425, -0.011212408571258553, 0.07759116726063546, -0.18010443158362216, 0.1628624960743229, 0.06821414471583617, 0.09421697291339483, -0.08632369610947666, 0.04975729702963196, -0.10382823337336287, -0.09670931458705488, -0.06707559613782098, 0.021844984822358214, 0.20116242063199496, -0.12606627919654068, -0.19185378723819724, -0.04131165860228641, -0.16703310286428247, -0.10316440863915259, 0.20326071626422054, 0.03519802767673355, -0.09667528025334116, 0.15645707245613, -0.03169324300338893, 0.012305829270434982, -0.004906894436337248, 0.013069144651165376, -0.029354931539325255, 0.07947198665695093, 0.13335639122965937, -0.08957011499742167, -0.008428209564661093, -0.03723189922003388, -0.007676995146901545, -0.13030715963188513};
    assert(startNPlist.size() == startNPs.size());
    std::map<std::string, double> startNPsMap;
    int kk=0;
    for (auto npName : startNPlist){
        startNPsMap[npName] = startNPs[kk];
        kk++;
    }
    double startPullsConstSum = 0.0;
    for (const auto& [key, value] : startNPsMap){
        std::string alphaName = "alpha_"+key;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(value);
        startPullsConstSum += value*value;
    }
    startPullsConstSum /= 2;
    std::vector<double> startPOIs = {0.896231117323111, 1.1300810104535157, 0.8488535803412169, 0.9004627681528321};
    mu->setVal(startPOIs[0]);
    mu_qqZZ->setVal(startPOIs[1]);
    mu_qqZZ_1->setVal(startPOIs[2]);
    mu_qqZZ_2->setVal(startPOIs[3]);
    /*                                                                 */
    /* Set the Initial Values to Jay's Pulls to Optimize The Minimizer */
    std::cout << "Starting Parameters for Minimizer: " << std::endl;
    nps.Print("v");
    pois.Print("v");

    RooMinimizer minim(*simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()), RooFit::GlobalObservables(SH.GetGlobalObservables())));
    minim.setStrategy(0);
    minim.setPrintLevel(3);

    ATLAS_LUMI->setConstant(true);
    mu->setConstant(false);
    mu_qqZZ->setConstant(false);
    mu_qqZZ_1->setConstant(false);
    mu_qqZZ_2->setConstant(false);
    for (auto np : nps){
        ((RooRealVar*)np)->setConstant(false);
    }
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    // minim.setEps(0.00000000001);
    // minim.setMaxIterations(100000);
    std::cout << "Default Minimizer: " << ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str() << std::endl;
    int status = minim.minimize(ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str(),
				                ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str());
    std::cout << "Status from the Minimizer: " << status << std::endl;

    RooFit::OwningPtr<RooFitResult> theSaveMigrad = minim.save("test", "test");
    const RooArgList& myPulls = theSaveMigrad->floatParsFinal();
    myPulls.Print("v");

    // Assign my pulls to the POIs and NPs
    mu->setVal(((RooRealVar*)myPulls.find("mu"))->getVal());
    mu_qqZZ->setVal(((RooRealVar*)myPulls.find("mu_qqZZ"))->getVal());
    mu_qqZZ_1->setVal(((RooRealVar*)myPulls.find("mu_qqZZ_1"))->getVal());
    mu_qqZZ_2->setVal(((RooRealVar*)myPulls.find("mu_qqZZ_2"))->getVal());
    double myPullsConstSum = 0.0;
    for (auto np : nps){
       ((RooRealVar*)np)->setVal(((RooRealVar*)myPulls.find(np->GetName()))->getVal());
       myPullsConstSum += ((RooRealVar*)myPulls.find(np->GetName()))->getVal()*((RooRealVar*)myPulls.find(np->GetName()))->getVal();
    }
    myPullsConstSum /= 2;

    // Calculate the NLL from these values
    RooNLLVar* nllMyPulls = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllSRMyPulls = (RooNLLVar*)pdf_SR->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR0MyPulls = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR1MyPulls = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR2MyPulls = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR0MyPullsConstrained = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR1MyPullsConstrained = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR2MyPullsConstrained = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "nllMyPulls value: " << nllMyPulls->getVal() << std::endl;
    std::cout << "nllSRMyPulls value: " << 2*nllSRMyPulls->getVal() << std::endl;
    std::cout << "nllCR0MyPulls value: " << 2*nllCR0MyPulls->getVal() << std::endl;
    std::cout << "nllCR1MyPulls value: " << 2*nllCR1MyPulls->getVal() << std::endl;
    std::cout << "nllCR2MyPulls value: " << 2*nllCR2MyPulls->getVal() << std::endl;
    std::cout << "nllCR0MyPullsConstrained value: " << nllCR0MyPullsConstrained->getVal() << std::endl;
    std::cout << "nllCR1MyPullsConstrained value: " << nllCR1MyPullsConstrained->getVal() << std::endl;
    std::cout << "nllCR2MyPullsConstrained value: " << nllCR2MyPullsConstrained->getVal() << std::endl;
    std::cout << "All Individual NLLs added together: " << nllSRMyPulls->getVal() + nllCR0MyPulls->getVal() + nllCR1MyPulls->getVal() + nllCR2MyPulls->getVal() << std::endl;
    std::cout << "All Constrained NLLs added together: " << nllSRMyPulls->getVal() + nllCR0MyPullsConstrained->getVal() + nllCR1MyPullsConstrained->getVal() + nllCR2MyPullsConstrained->getVal() << std::endl;
    std::cout << "constNLLcombData NLL value: " << constNLLcombData->getVal() << std::endl;
    std::cout << "Difference between constNLLcombData value and for when all alphas are 0.0: " << constNLLcombData->getVal() - constValAlpha0p0 << std::endl;
    std::cout << "Hand-Calculated Constraint Sum: " << myPullsConstSum << std::endl;
    std::cout << "Number I need to reproduce: " << 2*(nllSRMyPulls->getVal() + nllCR0MyPulls->getVal() + nllCR1MyPulls->getVal() + nllCR2MyPulls->getVal() + constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "nllMyPulls value without the added constraint term: " << nllMyPulls->getVal() - constNLLcombData->getVal() << std::endl;

    // Here are Jay's Pulls; Assign them to the POIs and NPs
    std::vector<std::string> jaysNPlist = {"ATLAS_LUMI", "qq_PDF_var_0jet", "qq_PDF_var_1jet", "qq_PDF_var_2jet", "gg_PDF_var", "EW_PDF_var", 
            "HOQCD_0jet", "HOQCD_1jet", "HOQCD_2jet", "HOEW_QCD_syst_0jet", "HOEW_QCD_syst_1jet", "HOEW_QCD_syst_2jet", "HOEW_syst", 
            "0jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "1jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", "2jet_H4l_Shower_UEPS_Sherpa_HM_CKKW", 
            "H4l_Shower_UEPS_Sherpa_HM_QSF", "ggZZNLO_QCD_syst_shape", "ggZZNLO_QCD_syst_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_norm", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CKKW_shape", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_norm", "H4l_Shower_UEPS_Sherpa_ggHM_QSF_shape", 
            "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_norm", "H4l_Shower_UEPS_Sherpa_ggHM_CSSKIN_shape", "HOQCD_VBF", "H4l_Shower_UEPS_VBF_OffShell", 
            "JET_JER_DataVsMC_MC16", "JET_JER_EffectiveNP_10", "JET_JER_EffectiveNP_11", "JET_JER_EffectiveNP_1", "JET_JER_EffectiveNP_12restTerm", 
            "JET_JER_EffectiveNP_2", "JET_JER_EffectiveNP_3", "JET_JER_EffectiveNP_4", "JET_JER_EffectiveNP_5", "JET_JER_EffectiveNP_6", 
            "JET_JER_EffectiveNP_7", "JET_JER_EffectiveNP_8", "EG_RESOLUTION_ALL", "EG_SCALE_ALL", "EG_SCALE_AF2", "JET_BJES_Response", 
            "JET_EffectiveNP_Detector1", "JET_EffectiveNP_Detector2", "JET_EffectiveNP_Mixed1", "JET_EffectiveNP_Mixed2", "JET_EffectiveNP_Mixed3", 
            "JET_EffectiveNP_Modelling1", "JET_EffectiveNP_Modelling2", "JET_EffectiveNP_Modelling3", "JET_EffectiveNP_Modelling4", 
            "JET_EffectiveNP_Statistical1", "JET_EffectiveNP_Statistical2", "JET_EffectiveNP_Statistical3", "JET_EffectiveNP_Statistical4", 
            "JET_EffectiveNP_Statistical5", "JET_EffectiveNP_Statistical6", "JET_EtaIntercalibration_Modelling", 
            "JET_EtaIntercalibration_NonClosure_2018data", "JET_EtaIntercalibration_NonClosure_highE", "JET_EtaIntercalibration_NonClosure_negEta", 
            "JET_EtaIntercalibration_NonClosure_posEta", "JET_EtaIntercalibration_TotalStat", "JET_Flavor_Composition_ggF", "JET_Flavor_Composition_EW", 
            "JET_Flavor_Composition_qqZZ", "JET_Flavor_Response_ggF", "JET_Flavor_Response_EW", "JET_Flavor_Response_qqZZ", "JET_Pileup_OffsetMu", 
            "JET_Pileup_OffsetNPV", "JET_Pileup_PtTerm", "JET_Pileup_RhoTopology", "JET_PunchThrough_MC16", "JET_SingleParticle_HighPt", "MUON_ID", 
            "MUON_MS", "MUON_SAGITTA_RESBIAS", "MUON_SAGITTA_RHO", "MUON_SCALE", "EL_EFF_ID_CorrUncertaintyNP0", "EL_EFF_ID_CorrUncertaintyNP10", 
            "EL_EFF_ID_CorrUncertaintyNP11", "EL_EFF_ID_CorrUncertaintyNP12", "EL_EFF_ID_CorrUncertaintyNP13", "EL_EFF_ID_CorrUncertaintyNP14", 
            "EL_EFF_ID_CorrUncertaintyNP15", "EL_EFF_ID_CorrUncertaintyNP1", "EL_EFF_ID_CorrUncertaintyNP2", "EL_EFF_ID_CorrUncertaintyNP3", 
            "EL_EFF_ID_CorrUncertaintyNP4", "EL_EFF_ID_CorrUncertaintyNP5", "EL_EFF_ID_CorrUncertaintyNP6", "EL_EFF_ID_CorrUncertaintyNP7", 
            "EL_EFF_ID_CorrUncertaintyNP8", "EL_EFF_ID_CorrUncertaintyNP9", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7", 
            "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8", "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", 
            "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "MUON_EFF_ISO_STAT", "MUON_EFF_ISO_SYS", "MUON_EFF_RECO_STAT_LOWPT", "MUON_EFF_RECO_STAT", 
            "MUON_EFF_RECO_SYS_LOWPT", "MUON_EFF_RECO_SYS", "MUON_EFF_TTVA_STAT", "MUON_EFF_TTVA_SYS", "JET_fJvtEfficiency", "PRW_DATASF"};
    std::vector<double> jaysPullsNPs; 
    if (lumiVal == 139.0){
        jaysPullsNPs = {-0.028277317083615246, -0.04057689010533464, 0.10369602885304745, 0.11066106781601417, -0.01070038669720914, -0.05759139700218771, -0.039368980796976906, 0.5389750637805085, -0.35797809100039923, 0.20192714938577377, 0.3119750126374231, -0.34065397302735523, 0.08902291167197891, -0.12265626665304194, -0.09637690460720441, -0.039706380932586105, -0.09290529968612696, -0.03373526893304729, -0.049685334887108606, -0.050419760428098634, -0.04376676978020435, 0.22378012646824283, 0.03879232998630511, 0.10630390150558454, 0.10758326132262591, -0.10897959543960042, -0.0028379359100459164, 0.11431385823033342, 0.11815194087567107, -0.14739965013029085, 0.2131143413627042, 0.5267357087866459, -0.46411726235064027, -0.32531435023980587, -0.10630155772201742, 0.07115133352005606, -0.13758632952538594, 0.015360317395978899, 0.06082579953899187, -0.04408293558549051, -0.19138766492421166, -0.04998098758399088, -0.06315685339836277, 0.25272849285388566, -0.0524907911301553, -0.46935315685699613, -0.10993653983666227, 0.0005480517066326902, -0.027944171055505323, 0.23626789670107323, -0.2991882518229628, -0.036770028863391396, -0.17627080691598332, -0.2051338696569478, -0.05143889369986522, -0.12036324644115359, 0.13613692361996083, 0.11139336643690637, 0.1974181069234188, -0.213246497746094, 0.1418124931083198, 0.046164060236882305, -0.15928099244585206, 0.005703217414649191, 0.028022178843108078, 0.015073922432917513, -0.2605904835707358, 0.07593053677512569, 0.009739070129780637, 0.11047471668654774, -0.1238551556982504, -0.05522619601358493, 0.08736245932540211, -0.2694307611782417, -0.08734056945760392, 0.2988457055106014, 0.22255049091117932, -0.10373253982659435, 0.0589675100652219, -0.18324467387631735, -0.022963698143251113, 0.12542490156363834, -0.09595508290568312, 0.01008181564580793, -0.052299910697367336, 0.0023535365823551466, -0.1428966470871945, -0.036892745852325395, 0.08386201945388114, 0.046127586103968665, 0.04902717670386127, 0.168580145738976, -0.02210869712145925, -0.030064319886695345, 0.04529172404444309, -0.18009054292510787, 0.135460661068939, 0.06599254978164655, 0.09024491707773363, -0.0792675310028688, 0.029778883434420044, -0.10196046636167728, -0.07091799955244524, -0.07057386812122683, 0.024698138674941413, 0.17801628648266543, -0.10758632942503409, -0.15534825109186184, -0.04007972536525407, -0.1296035966883202, -0.08943470544397047, 0.2062304815009452, 0.030195454621761614, -0.09351374542662536, 0.13286347620739783, -0.03730729501766865, -0.009331009163767445, -0.00016678167807109137, 0.01912980400614542, -0.01963682651102139, 0.08770610034598421, 0.10323520334183814, -0.0757368855199293, -0.006919896675076195, -0.014742474890948013, 0.006149342356215567, -0.09089024860922228};
    } else if (lumiVal == 140.1){
        jaysPullsNPs = {-0.012304123211624683, -0.05676484099486917, 0.10939412502257628, 0.09638654928806716, -0.0058844252493451925, -0.06036371391323871, -0.03773664535473304, 0.5793434693040871, -0.4212014447990021, 0.21307168687755318, 0.3836150694261321, -0.39502421103420504, 0.0944781062049346, -0.1144102187662508, -0.09140647609202203, -0.025362269145717015, -0.1324008500628735, -0.03277737220725146, -0.05614153586944149, -0.050788955850813965, -0.04672728958667602, 0.2338628653997735, 0.05365856229758719, 0.09159777655918631, 0.11682165071070977, -0.113644163528267, -0.0035655684639608336, 0.14369059995211889, 0.15687110738196564, -0.15961590447182816, 0.2182628855898944, 0.5987569298109362, -0.5313331405754305, -0.4038120247234137, -0.14977323516550634, 0.07562245792050515, -0.14845436970025247, -0.009304755341245707, 0.058816132370854146, -0.021001200725458493, -0.182495970531472, -0.05329179807718245, -0.0717892265524447, 0.29797915399877106, -0.06653055504789945, -0.5288158190703922, -0.13966120138674182, -0.029037186337836716, -0.027018665723132315, 0.2507913141952367, -0.35337273407308145, -0.047177334812360056, -0.20920348095519858, -0.28011238051784765, -0.05924563762028058, -0.13650632777024027, 0.15768386558782635, 0.1272509299633453, 0.20547795351572848, -0.25356793466740873, 0.16108384642245754, 0.026054324889089906, -0.1425065072428116, -0.007767656727963554, 0.01846641808500754, 0.01468159423410477, -0.25602516497379013, 0.10952903594904492, 0.009830068386587098, 0.10821577323867902, -0.10431604772509426, -0.058840845017583104, 0.0992555238152754, -0.2740406663589481, -0.14069961136070505, 0.32728545460242153, 0.2297893680452763, -0.11148346923574244, 0.06234896055040964, -0.18047187761570105, -0.030666006444667514, 0.13473017969716183, -0.10020347619081518, -0.013779689663337223, -0.04750234474811328, -0.01926876231491791, -0.16647182416480644, -0.039757208238752284, 0.08454565173267604, 0.04379765117371479, 0.06987027817753091, 0.15217488354939762, -0.02752346419457425, -0.011212408571258553, 0.07759116726063546, -0.18010443158362216, 0.1628624960743229, 0.06821414471583617, 0.09421697291339483, -0.08632369610947666, 0.04975729702963196, -0.10382823337336287, -0.09670931458705488, -0.06707559613782098, 0.021844984822358214, 0.20116242063199496, -0.12606627919654068, -0.19185378723819724, -0.04131165860228641, -0.16703310286428247, -0.10316440863915259, 0.20326071626422054, 0.03519802767673355, -0.09667528025334116, 0.15645707245613, -0.03169324300338893, 0.012305829270434982, -0.004906894436337248, 0.013069144651165376, -0.029354931539325255, 0.07947198665695093, 0.13335639122965937, -0.08957011499742167, -0.008428209564661093, -0.03723189922003388, -0.007676995146901545, -0.13030715963188513};
        // jaysPullsNPs = {-0.012266679725209752, -0.05620464883995752, 0.10951681594989485, 0.09637625860494836, -0.00574596479753818, -0.060316522643128416, -0.038001846690458425, 0.5810602323342227, -0.4221872904409583, 0.21355968870163175, 0.38559191914769925, -0.39579755247507536, 0.09489948985407085, -0.11495597337065638, -0.0918242971535861, -0.025482598556318008, -0.13262317719132843, -0.032802744559745384, -0.05319004268593275, -0.052417659993300295, -0.0467717936338548, 0.23533324853268975, 0.053599836906627715, 0.09153801166948962, 0.11725069258289754, -0.113782692973636, -0.0035793079214979113, 0.14377489991562628, 0.1572673429035812, -0.1597558259640467, 0.21875993182378184, 0.6000462979450599, -0.5326528665851058, -0.4047070639390155, -0.1493976789315085, 0.07576105105996248, -0.14891034414948998, -0.009247488556733071, 0.05889514558900019, -0.02099807849199379, -0.18264693045136945, -0.053339542143398984, -0.07187859008812067, 0.2982642946160774, -0.06664621481723851, -0.5291869633060672, -0.13974458032818704, -0.029144933275137352, -0.027160928447725767, 0.2508955044983031, -0.3538046238432573, -0.046991457574500045, -0.2093116604356858, -0.2802655766028828, -0.05933405684972028, -0.13656922705058355, 0.15786099224164266, 0.12727532995744364, 0.2060409274054087, -0.2539374619352031, 0.16127424606414237, 0.026132967432421706, -0.14267402990040748, -0.007837595492436946, 0.01826674417315502, 0.014657422726827025, -0.25568671483393707, 0.11033989626142908, 0.009823945890147965, 0.10818393824506493, -0.10469980356254481, -0.059081024594873445, 0.09938175865429316, -0.2757327109003212, -0.14076122917959913, 0.32749372013879585, 0.23001689351670782, -0.11148121675349075, 0.0623950076909525, -0.18056263318375781, -0.0307763613106475, 0.13472209378407984, -0.10020326236709345, -0.013764916858625447, -0.047504480843216415, -0.019348996948560543, -0.16649189476306828, -0.039849219032122005, 0.08457189338054036, 0.04380256254584379, 0.06991233900110302, 0.152235435267893, -0.02747136741676288, -0.011260119675021414, 0.07763732236678912, -0.18016454509913066, 0.16278775431186424, 0.06821136663249444, 0.09426823738257899, -0.0863970937198693, 0.049726946141198866, -0.10380101048447991, -0.09678656955510666, -0.06712967272322268, 0.021846212119242706, 0.20125317271711954, -0.12607815420567525, -0.19191885276413134, -0.04133363073135307, -0.1670760343218263, -0.10319167211391388, 0.2033027739967348, 0.035115796280736376, -0.09677531806724596, 0.15659811738577245, -0.03168078489931818, 0.012318276392642132, -0.004913158294118274, 0.01306739630838989, -0.02940014066388898, 0.07953313147297787, 0.1333914823072507, -0.08958024209070417, -0.008449074496556354, -0.03726295257370988, -0.007736774031600877, -0.13058541761475687};
    }
    assert(jaysNPlist.size() == jaysPullsNPs.size());
    std::map<std::string, double> jayNPpullsMap;
    int i=0;
    for (auto npName : jaysNPlist){
        jayNPpullsMap[npName] = jaysPullsNPs[i];
        i++;
    }
    double jaysPullsConstSum = 0.0;
    for (const auto& [key, value] : jayNPpullsMap){
        std::string alphaName = "alpha_"+key;
        ((RooRealVar*)nps.find(alphaName.c_str()))->setVal(value);
        jaysPullsConstSum += value*value;
    }
    jaysPullsConstSum /= 2;
    std::vector<double> jaysPullsPOIs;
    if (lumiVal == 139.0){
        jaysPullsPOIs = {0.8970120806742395, 1.1400534849538946, 0.8502735118360424, 0.8978423011657017};
    } else if (lumiVal == 140.1){
        jaysPullsPOIs = {0.896231117323111, 1.1300810104535157, 0.8488535803412169, 0.9004627681528321};
        // jaysPullsPOIs = {0.895685447816521, 1.1300694452063647, 0.8488492806816584, 0.9005832258397211};
    }
    mu->setVal(jaysPullsPOIs[0]);
    mu_qqZZ->setVal(jaysPullsPOIs[1]);
    mu_qqZZ_1->setVal(jaysPullsPOIs[2]);
    mu_qqZZ_2->setVal(jaysPullsPOIs[3]);

    // Calculate the NLL from these values
    RooNLLVar* nllJaysPulls = (RooNLLVar*)simPdf->createNLL(*combinedData, RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllSRJaysPulls = (RooNLLVar*)pdf_SR->createNLL(*theDataSets.at("SR"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR0JaysPulls = (RooNLLVar*)theCRpdfs.at("CR0")->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR1JaysPulls = (RooNLLVar*)theCRpdfs.at("CR1")->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR2JaysPulls = (RooNLLVar*)theCRpdfs.at("CR2")->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR0JaysPullsConstrained = (RooNLLVar*)constrainedCR0->createNLL(*theDataSets.at("CR0"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR1JaysPullsConstrained = (RooNLLVar*)constrainedCR1->createNLL(*theDataSets.at("CR1"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    RooNLLVar* nllCR2JaysPullsConstrained = (RooNLLVar*)constrainedCR2->createNLL(*theDataSets.at("CR2"), RooFit::Constrain(SH.GetNuisanceParams()),
                                                RooFit::GlobalObservables(SH.GetGlobalObservables()));
    std::cout << "nllJaysPulls value: " << nllJaysPulls->getVal() << std::endl;
    std::cout << "nllSRJaysPulls value: " << 2*nllSRJaysPulls->getVal() << std::endl;
    std::cout << "nllCR0JaysPulls value: " << 2*nllCR0JaysPulls->getVal() << std::endl;
    std::cout << "nllCR1JaysPulls value: " << 2*nllCR1JaysPulls->getVal() << std::endl;
    std::cout << "nllCR2JaysPulls value: " << 2*nllCR2JaysPulls->getVal() << std::endl;
    std::cout << "nllCR0JaysPullsConstrained value: " << nllCR0JaysPullsConstrained->getVal() << std::endl;
    std::cout << "nllCR1JaysPullsConstrained value: " << nllCR1JaysPullsConstrained->getVal() << std::endl;
    std::cout << "nllCR2JaysPullsConstrained value: " << nllCR2JaysPullsConstrained->getVal() << std::endl;
    std::cout << "All Individual NLLs added together: " << nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() << std::endl;
    std::cout << "All Constrained NLLs added together: " << nllSRJaysPulls->getVal() + nllCR0JaysPullsConstrained->getVal() + nllCR1JaysPullsConstrained->getVal() + nllCR2JaysPullsConstrained->getVal() << std::endl;
    std::cout << "constNLLcombData NLL value: " << constNLLcombData->getVal() << std::endl;
    std::cout << "Difference between constNLLcombData value and for these alphas: " << 2*(constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "Hand-Calculated Constraint Sum: " << 2*jaysPullsConstSum << std::endl;
    std::cout << "Number I need to reproduce: " << 2*(nllSRJaysPulls->getVal() + nllCR0JaysPulls->getVal() + nllCR1JaysPulls->getVal() + nllCR2JaysPulls->getVal() + constNLLcombData->getVal() - constValAlpha0p0) << std::endl;
    std::cout << "nllJaysPulls value without the added constraint term: " << nllJaysPulls->getVal() - constNLLcombData->getVal() << std::endl;

    std::cout << "Done." << std::endl;
    return 0;
}