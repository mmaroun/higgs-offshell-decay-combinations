#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )

sys_filename=${data_dir}"/NSBI_HZZ_nominal_sys.root"
echo $sys_filename

# Show then run the command
the_cmd="testMergeDataSets ${sys_filename} >& testMerge.log & "
echo $the_cmd

sleep 1

testMergeDataSets ${sys_filename} >& testMerge.log & 

sleep 1