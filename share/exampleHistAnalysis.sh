#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )

# Show then run the command
the_cmd="exampleHistAnalysis >& exampleHist.log & "
echo $the_cmd

sleep 1

exampleHistAnalysis >& exampleHist.log & 

sleep 1