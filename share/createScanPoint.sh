#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/"
ws_name="WS_18_09_2024_data.root"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi

lumi=140.1
echo "Luminosity Set at ${lumi} ifb"

runData=data
echo "Running on ${runData}"

mu_val=3.0
echo "Creating Scan Point for mu = ${mu_val}"

if [ $lumi == 140.1 ]; then
    dataPlusYieldsPlusCRNobsFilename=${data_dir}"/NSBI_HZZ_nominal_sys_RealData_140ifb_N3LOkfac_UpdSysNames.root";
elif [ $lumi == 139.0 ]; then
    dataPlusYieldsPlusCRNobsFilename=${data_dir}"/NSBI_HZZ_nominal_sys_RealData.root";
fi
echo $dataPlusYieldsPlusCRNobsFilename

crYieldsVarsFilename=${data_dir}"/NSBI_HZZ_nominal_sys_Asimov_140ifb_N3LOkfac_UpdSysNames.root"
echo $crYieldsVarsFilename

outfile_name=${target_dir}${ws_name}
echo $outfile_name

graph_filename=${run_dir}"/globalPOIscan_strat_1_3p0.root"
echo $graph_filename

# Show then run the command
the_cmd="createScanPoint ${lumi} ${dataPlusYieldsPlusCRNobsFilename} ${crYieldsVarsFilename} ${outfile_name} ${graph_filename} ${runData} ${mu_val} >& testCreate3p0.log & "
echo $the_cmd

sleep 1

createScanPoint ${lumi} ${dataPlusYieldsPlusCRNobsFilename} ${crYieldsVarsFilename} ${outfile_name} ${graph_filename} ${runData} ${mu_val} >& testCreate3p0.log & 

sleep 1