#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/"
input_dir=${run_dir}${final_dir_add}

inputFile2l2nu=${input_dir}"split2l2nuWS.root"
inputFile4l=${input_dir}"split4lWS.root"

# Show then run the command
the_cmd="compareSplitWS ${inputFile2l2nu} ${inputFile4l} >& testCompare.log & "
echo $the_cmd

sleep 1

compareSplitWS  ${inputFile2l2nu} ${inputFile4l} >& testCompare.log & 

sleep 1