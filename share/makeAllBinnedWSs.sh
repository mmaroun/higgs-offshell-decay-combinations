#!/bin/bash

declare -a arr=("0_00" "0_05" "0_10" "0_15" "0_20" "0_25" "0_30" "0_35" "0_40" "0_45" "0_50" "0_55" "0_60" "0_65" "0_70" "0_75" "0_80" "0_85" 
                "0_90" "0_95" "1_00" "1_05" "1_10" "1_15" "1_20" "1_25" "1_30" "1_35" "1_40" "1_45" "1_50" "1_55" "1_60" "1_65" "1_70" "1_75" 
                "1_80" "1_85" "1_90" "1_95" "2_00" "2_05" "2_10" "2_15" "2_20" "2_25" "2_30" "2_35" "2_40" "2_45" "2_50" "2_55" "2_60" "2_65" 
                "2_70" "2_75" "2_80" "2_85" "2_90" "2_95" "3_00" "3_05")

for i in "${arr[@]}"
do 
    source ../source/higgs-offshell-decay-combinations/share/generateBinnedModelOneMu.sh "$i"
done

# source ../source/higgs-offshell-decay-combinations/share/makeBinnedScanMultipleWS.sh