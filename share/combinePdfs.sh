#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/"
ws_name="WS_12_10_22final.root"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi

sbi_infile_name=${data_dir}"/testReadRootFile.root"
echo $sbi_infile_name

pois_infile_name=${data_dir}"/testLoadPoissons.txt"
echo $pois_infile_name

outfile_name=${target_dir}${ws_name}
echo $outfile_name

# Show then run the command
the_cmd="combinePdfs ${sbi_infile_name} ${pois_infile_name} ${outfile_name} >& testExec.log & "
echo $the_cmd

sleep 1

combinePdfs ${sbi_infile_name} ${pois_infile_name} ${outfile_name} >& testExec.log & 

sleep 1