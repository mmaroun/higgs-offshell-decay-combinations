#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/binnedHist_WSs/"
input_dir_add="histogramsOOnew/"

mu_val=$1

ws_FileName="WS_HZZ_4l_12_11_2024_binnedAsimov_"${mu_val}".root"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi

fullWSFile=${target_dir}${ws_FileName}
echo $fullWSFile

inputFilePrefix=${data_dir}"/"${input_dir_add}"hist_mu_"${mu_val};
echo $inputFilePrefix

# Show then run the command
the_cmd="generateBinnedModelOneMu ${mu_val} ${inputFilePrefix} ${fullWSFile} >& testGenerateBinModel_mu_${mu_val}.log & "
echo $the_cmd

sleep 1

generateBinnedModelOneMu ${mu_val} ${inputFilePrefix} ${fullWSFile} >& testGenerateBinModel_mu_${mu_val}.log & 

sleep 1