#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
input_dir_add="/WS_final/binnedHist_WSs/"

inputDir=${run_dir}${input_dir_add}
echo $inputDir

outfile=${run_dir}"/graphBinnedAsimovPOIscan.root"

# Show then run the command
the_cmd="makeBinnedScanMultipleWS ${inputDir} ${outfile} >& testMakeBinnedScan.log & "
echo $the_cmd

sleep 1

makeBinnedScanMultipleWS ${inputDir} ${outfile} >& testMakeBinnedScan.log & 

sleep 1