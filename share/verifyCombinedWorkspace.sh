#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/"
ws_prefix="WS_HZZ_2l2nu_offshell_16_10_2024"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi

inputFile="/eos/user/m/mmaroun/workspaceCombiner/WS_HZZ_4l_2l2nu_realData_13_11_2024.root";

# Show then run the command
the_cmd="verifyCombinedWorkspace ${inputFile} >& testCombined.log & "
echo $the_cmd

sleep 1

verifyCombinedWorkspace  ${inputFile}  >& testCombined.log & 

sleep 1