#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/OOs_Add_cpp/"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi
echo $target_dir

inputReferenceFilename=${data_dir}"/NSBI_HZZ_perEvent_Reference_info_for_binning.root";
echo $inputReferenceFilename

# Show then run the command
the_cmd="binOOs ${inputReferenceFilename} ${target_dir} >& testBinOOs.log & "
echo $the_cmd

sleep 1

binOOs ${inputReferenceFilename} ${target_dir} >& testBinOOs.log & 

sleep 1