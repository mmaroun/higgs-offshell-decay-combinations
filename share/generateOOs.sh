#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/OOs_Add/"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi
echo $target_dir

outputFile=${target_dir}"testOOs.root"
echo $outputFile

inputReferenceFilename=${data_dir}"/NSBI_HZZ_nominal_sys_Reference_140ifb_N3LOkfac_UpdSysNames.root";
echo $inputReferenceFilename

# Show then run the command
the_cmd="generateOOs ${inputReferenceFilename} ${outputFile} >& testGenerateOOs.log & "
echo $the_cmd

sleep 1

generateOOs ${inputReferenceFilename} ${outputFile} >& testGenerateOOs.log & 

sleep 1