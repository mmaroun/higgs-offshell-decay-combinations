#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )

lumi=140.1
echo "Luminosity Set at ${lumi} ifb"

if [ $lumi == 140.1 ]; then
    sys_filename=${data_dir}"/NSBI_HZZ_nominal_sys_RealData_140ifb_N3LOkfac_UpdSysNames.root";
elif [ $lumi == 139.0 ]; then
    sys_filename=${data_dir}"/NSBI_HZZ_nominal_sys_RealData.root";
fi
echo $sys_filename

# Show then run the command
the_cmd="validateSystematics ${lumi} ${sys_filename} >& testValidate.log & "
echo $the_cmd

sleep 1

validateSystematics ${lumi} ${sys_filename} >& testValidate.log & 

sleep 1