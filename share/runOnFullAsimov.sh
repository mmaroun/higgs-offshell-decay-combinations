#!/bin/bash

# Access the script and where the workspace will be written out; make the folder for the WS if necessary
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../utils/" &> /dev/null && pwd )
data_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../data/" &> /dev/null && pwd )
run_dir=$(pwd)
final_dir_add="/WS_final/"
ws_name="WS_HZZ_4l_offshell_07_10_2024_asimov.root"

target_dir=${run_dir}${final_dir_add}
if [ ! -d ${target_dir} ]; then
    mkdir -p ${target_dir};
fi

lumi=140.1
echo "Luminosity Set at ${lumi} ifb"

runData=asimov
echo "Running on ${runData}"

if [ $lumi == 140.1 ]; then
    dataPlusYieldsPlusCRNobsFilename=${data_dir}"/NSBI_HZZ_nominal_sys_AsimovReduced_140ifb_N3LOkfact_UpdSysNames.root";
elif [ $lumi == 139.0 ]; then
    dataPlusYieldsPlusCRNobsFilename=${data_dir}"/NSBI_HZZ_nominal_sys_wCR.root";
fi
echo $dataPlusYieldsPlusCRNobsFilename

outfile_name=${target_dir}${ws_name}
echo $outfile_name

graph_filename=${run_dir}"/globalPOIscan.root"
echo $graph_filename

# Show then run the command
the_cmd="runOnData ${lumi} ${dataPlusYieldsPlusCRNobsFilename} ${dataPlusYieldsPlusCRNobsFilename} ${outfile_name} ${graph_filename} ${runData} >& testAsimovRun.log & "
echo $the_cmd

sleep 1

runOnData ${lumi} ${dataPlusYieldsPlusCRNobsFilename} ${dataPlusYieldsPlusCRNobsFilename} ${outfile_name} ${graph_filename} ${runData} >& testAsimovRun.log & 

sleep 1