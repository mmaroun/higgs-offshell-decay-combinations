import ROOT
import numpy as np
import os

ROOT.ROOT.EnableImplicitMT(16)

inputFileName = '/eos/user/m/mmaroun/offShellCombinations/source/higgs-offshell-decay-combinations/data/NSBI_HZZ_perEvent_Reference_info_for_binning.root'
treeNameOriginal = 'nominal_sys_var_per_event'
treeNameLLRs = 'llr_oo'

inFile = ROOT.TFile.Open(inputFileName)
originalTree = inFile.Get(treeNameOriginal)
llrTree = inFile.Get(treeNameLLRs)
originalTree.AddFriend(llrTree)

df = ROOT.RDataFrame(originalTree)
ROOT.RDF.Experimental.AddProgressBar(df)

processList = ['S', 'SBI', 'B', 'EWB', 'EWSBI', 'EWSBI10', 'qqZZ_0', 'qqZZ_1', 'qqZZ_2', 'ttV']
systematicsList = ['ATLAS_alpha_LUMI', 'ATLAS_EG_RESOLUTION_ALL', 'ATLAS_EG_SCALE_AF2', 'ATLAS_EG_SCALE_ALL', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP0', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP1', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP2', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP3', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP4', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP5', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP6', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP7', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP8', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP9', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP10', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP11', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP12', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP13', 'ATLAS_EL_EFF_ID_CorrUncertaintyNP14', 
    'ATLAS_EL_EFF_ID_CorrUncertaintyNP15', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15', 
    'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16', 'ATLAS_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17', 
    'ATLAS_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'ATLAS_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'ATLAS_MUON_EFF_ISO_STAT', 
    'ATLAS_MUON_EFF_ISO_SYS', 'ATLAS_MUON_EFF_RECO_STAT', 'ATLAS_MUON_EFF_RECO_STAT_LOWPT', 'ATLAS_MUON_EFF_RECO_SYS', 
    'ATLAS_MUON_EFF_RECO_SYS_LOWPT', 'ATLAS_MUON_EFF_TTVA_STAT', 'ATLAS_MUON_EFF_TTVA_SYS', 'ATLAS_MUON_ID', 'ATLAS_MUON_MS', 
    'ATLAS_MUON_SAGITTA_RESBIAS', 'ATLAS_MUON_SAGITTA_RHO', 'ATLAS_MUON_SCALE', 'ATLAS_JET_BJES_Response', 'ATLAS_JET_EffectiveNP_Detector1', 
    'ATLAS_JET_EffectiveNP_Detector2', 'ATLAS_JET_EffectiveNP_Mixed1', 'ATLAS_JET_EffectiveNP_Mixed2', 'ATLAS_JET_EffectiveNP_Mixed3', 
    'ATLAS_JET_EffectiveNP_Modelling1', 'ATLAS_JET_EffectiveNP_Modelling2', 'ATLAS_JET_EffectiveNP_Modelling3', 
    'ATLAS_JET_EffectiveNP_Modelling4', 'ATLAS_JET_EffectiveNP_Statistical1', 'ATLAS_JET_EffectiveNP_Statistical2', 
    'ATLAS_JET_EffectiveNP_Statistical3', 'ATLAS_JET_EffectiveNP_Statistical4', 'ATLAS_JET_EffectiveNP_Statistical5', 
    'ATLAS_JET_EffectiveNP_Statistical6', 'ATLAS_JET_EtaIntercalibration_Modelling', 'ATLAS_JET_EtaIntercalibration_NonClosure_2018data', 
    'ATLAS_JET_EtaIntercalibration_NonClosure_highE', 'ATLAS_JET_EtaIntercalibration_NonClosure_negEta', 
    'ATLAS_JET_EtaIntercalibration_NonClosure_posEta', 'ATLAS_JET_EtaIntercalibration_TotalStat', 'ATLAS_JET_Flavor_Composition_VBF', 
    'ATLAS_JET_Flavor_Composition_gg', 'ATLAS_JET_Flavor_Composition_qq', 'ATLAS_JET_Flavor_Response_VBF', 'ATLAS_JET_Flavor_Response_gg', 
    'ATLAS_JET_Flavor_Response_qq', 'ATLAS_JET_JER_DataVsMC_MC16', 'ATLAS_JET_JER_EffectiveNP_1', 'ATLAS_JET_JER_EffectiveNP_2', 
    'ATLAS_JET_JER_EffectiveNP_3', 'ATLAS_JET_JER_EffectiveNP_4', 'ATLAS_JET_JER_EffectiveNP_5', 'ATLAS_JET_JER_EffectiveNP_6', 
    'ATLAS_JET_JER_EffectiveNP_7', 'ATLAS_JET_JER_EffectiveNP_8', 'ATLAS_JET_JER_EffectiveNP_10', 'ATLAS_JET_JER_EffectiveNP_11', 
    'ATLAS_JET_JER_EffectiveNP_12restTerm', 'ATLAS_JET_Pileup_OffsetMu', 'ATLAS_JET_Pileup_OffsetNPV', 'ATLAS_JET_Pileup_PtTerm', 
    'ATLAS_JET_Pileup_RhoTopology', 'ATLAS_JET_PunchThrough_MC16', 'ATLAS_JET_SingleParticle_HighPt', 'ATLAS_JET_fJvtEfficiency', 
    'ATLAS_PRW_DATASF', 'ATLAS_PS_qqZZ_CKKW_0Jet_Shape', 'ATLAS_PS_qqZZ_CKKW_1Jet_Shape', 'ATLAS_PS_qqZZ_CKKW_2Jet_Shape', 
    'ATLAS_PS_qqZZ_QSF_Shape', 'ATLAS_PS_ggZZ_CKKW_Norm', 'ATLAS_PS_ggZZ_CKKW_Shape', 'ATLAS_PS_ggZZ_CSSKIN_Norm', 
    'ATLAS_PS_ggZZ_CSSKIN_Shape', 'ATLAS_PS_ggZZ_QSF_Norm', 'ATLAS_PS_ggZZ_QSF_Shape', 'ATLAS_H4l_Shower_UEPS_VBF_OffShell', 
    'ATLAS_HOEW_QCD_0Jet', 'ATLAS_HOEW_QCD_1Jet', 'ATLAS_HOEW_QCD_2Jet', 'ATLAS_HOEW', 'ATLAS_HOQCD_0Jet', 'ATLAS_HOQCD_1Jet', 
    'ATLAS_HOQCD_2Jet', 'ATLAS_HOQCD_VBF', 'ATLAS_VBF_PDF', 'ATLAS_QCD_ggZZk_Norm', 'ATLAS_QCD_ggZZk_Shape', 'ATLAS_gg_PDF', 
    'ATLAS_qq_PDF_0Jet', 'ATLAS_qq_PDF_1Jet', 'ATLAS_qq_PDF_2Jet']


yields = {'S': 37.147495, 'SBI': 362.12915, 'B': 371.32272, 'EWB': 29.984165, 'EWSBI': 25.751380, 'EWSBI10': 55.352203, 'qqZZ_0': 723.94799, 'qqZZ_1':559.26580 , 'qqZZ_2': 305.38488, 'ttV': 75.787437}

pone_over_pref = f'0.'
one_yield = f'0.'
for process in processList:
    pone_over_pref += f'+{yields[process]}*r_{process}'
    one_yield += f'+{yields[process]}'
df = df.Define('pone_over_pref', pone_over_pref)
df = df.Define('one_yield', one_yield)


muVal = 0.00
mu_list = []
for i in range(61):
    mu_list.append(muVal)
    muVal += 0.05

histograms = {}
for mu in mu_list:
    string_mu = "%.2f"%mu
    string_mu = string_mu.replace(".","_")
    print(string_mu)
    multipliers = {'S': mu - np.sqrt(mu), 
        'SBI': np.sqrt(mu), 
        'B': 1 - np.sqrt(mu), 
        'EWB': (1.0/(-10.0+np.sqrt(10.0)))*((1.0-np.sqrt(10.0))*mu+9.0*np.sqrt(mu)-10.0+np.sqrt(10.0)), 
        'EWSBI': (1.0/(-10.0+np.sqrt(10.0)))*(np.sqrt(10.0)*mu-10.0*np.sqrt(mu)), 
        'EWSBI10': (1.0/(-10.0+np.sqrt(10.0)))*(-(mu)+np.sqrt(mu)), 
        'qqZZ_0': 1., 
        'qqZZ_1': 1., 
        'qqZZ_2': 1., 
        'ttV': 1.}
    
    pmu_over_pref = f'{treeNameLLRs}.llr_mu_{string_mu}'
    
    df = df.Define(f'pmu_over_pref_{string_mu}', pmu_over_pref)
    df = df.Define(f'oo_{string_mu}', f'(pmu_over_pref_{string_mu})/((pmu_over_pref_{string_mu})+(pone_over_pref/one_yield))')
    df.Display({f'oo_{string_mu}'})

    for process in processList:
        nominal_weight = f'total_weight*r_{process}*{yields[process]}/{yields["S"]+yields["EWSBI10"]}'
        df = df.Define(f'nominal_weight_{process}_{string_mu}', nominal_weight)
        nom_histo_name = f'hist_mu_{string_mu}_{process}'
        histograms[nom_histo_name] = df.Histo1D((nom_histo_name, '', 200, 0., 1.), f'oo_{string_mu}', f'nominal_weight_{process}_{string_mu}')
        for sysName in systematicsList:
            for var in ['up', 'down']:
                var_weight = f'nominal_weight_{process}_{string_mu}*var_{sysName}_ratio_{process}_{var}'
                df = df.Define(f'{var}_weight_{process}_{sysName}_{string_mu}', var_weight)
                var_histo_name = f'hist_mu_{string_mu}_{process}_{sysName}_{var}'
                histograms[var_histo_name] = df.Histo1D((var_histo_name, '', 200, 0., 1.), f'oo_{string_mu}', f'{var}_weight_{process}_{sysName}_{string_mu}')
                
ROOT.RDF.RunGraphs(histograms[h] for h in histograms)
saveDir = '/eos/user/m/mmaroun/offShellCombinations/source/higgs-offshell-decay-combinations/data/histogramsOO/'
os.system("saveDir=\"/eos/user/m/mmaroun/offShellCombinations/source/higgs-offshell-decay-combinations/data/histogramsOO\"; if [ ! -d ${saveDir} ]; then mkdir -p ${saveDir}; fi")
print('Writing out Histograms to files now...')
for mu in mu_list:
    string_mu = "%.2f"%mu
    string_mu = string_mu.replace(".","_")
    for process in processList:
        histFileName = saveDir+'hist_mu_'+string_mu+'_'+process+'.root'
        outFile = ROOT.TFile(histFileName, 'recreate')
        nomHisto = 'hist_mu_'+string_mu+'_'+process
        histograms[nomHisto].GetValue().Write()
        for sysName in systematicsList:
            for var in ['up', 'down']:
                varHisto = 'hist_mu_'+string_mu+'_'+process+'_'+sysName+'_'+var
                histograms[varHisto].GetValue().Write()
        outFile.Close()
print('Done.')