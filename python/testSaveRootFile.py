import ROOT
import os, sys, importlib, glob
import numpy as np
import pandas as pd
import math
import pickle
pd.options.mode.chained_assignment = None
import importlib, sys
import h5py

'''
This script takes in an existing h5 datafile with ML-trained coefficients
and converts it to a ROOT file with fewer columns. The aim of this script
is to create a test Root file for storage of important parameter values
for the RooDensityRatio class to evaluate() with.

******IMPORTANT***** 
To run this code with a python enviroment with pandas available, please run
the foillowing command: 

source /cvmfs/sft.cern.ch/lcg/views/dev3/latest/x86_64-el9-gcc12-opt/setup.sh

in your terminal. Afterwards, when using the NSBI Analysis in RooFit, re-set-up 
AnalysisBase.
********************
'''

def run():

    pdFilePath = '/eos/user/j/jsandesa/forMatt/'
    pdFileName = 'dataset_wDR_mean_origIndex.h5'
    systFileName = 'sys_var_dict.pickle'
    if not os.path.isfile("test.h5"):
        print("File is currently not readable as table. Rewriting hdf5 as 'table' format for chunk processing")
        df = pd.read_hdf(pdFilePath+pdFileName)
        df.to_hdf('test.h5', format='table', key='test')

    # Yields need to be calculated first before r_* can be stored becaure of r_qqZZ_0/1/2
    print("Calculating nominal event yields...")
    opts = ROOT.RDF.RSnapshotOptions()
    opts.fMode="RECREATE"
    opts.fOverwriteIfExists = True
    outfilePath = '../data/'
    outfileName = 'testReadRootFile.root'
    root_file = ROOT.TFile(outfilePath+outfileName, 'recreate')
    n_SR_S         = 0.0
    n_SR_SBI       = 0.0
    n_SR_B         = 0.0
    n_SR_EWB       = 0.0
    n_SR_EWSBI     = 0.0
    n_SR_EWSBI10   = 0.0
    n_SR_qqZZ_0    = 0.0
    n_SR_qqZZ_1    = 0.0
    n_SR_qqZZ_2    = 0.0
    n_SR_qqZZ      = 0.0
    n_SR_ttV       = 0.0
    for chunk in pd.read_hdf('test.h5', key='test', chunksize=10000):
        n_SR_S += chunk[chunk['subType']=='S'].weight_kFact.sum()
        n_SR_SBI += chunk[chunk['subType']=='SBI'].weight_kFact.sum()
        n_SR_B += chunk[chunk['subType']=='B'].weight_kFact.sum()
        n_SR_EWB += chunk[chunk['subType']=='EWB'].weight_kFact.sum()
        n_SR_EWSBI += chunk[chunk['subType']=='EWSBI'].weight_kFact.sum()
        n_SR_EWSBI10 += chunk[chunk['subType']=='EWSBI10'].weight_kFact.sum()
        n_SR_qqZZ += chunk[chunk['subType']=='qqZZ'].weight_kFact.sum()
        n_SR_qqZZ_0 += chunk[(chunk['subType']=='qqZZ')&(chunk['n_jets']==0)].weight_kFact.sum()
        n_SR_qqZZ_1 += chunk[(chunk['subType']=='qqZZ')&(chunk['n_jets']==1)].weight_kFact.sum()
        n_SR_qqZZ_2 += chunk[(chunk['subType']=='qqZZ')&(chunk['n_jets']>=2)].weight_kFact.sum()
        n_SR_ttV += chunk[chunk['subType']=='ttV'].weight_kFact.sum()
    
    print("Writing nominal event yields to 'yields' tree...")
    all_yields_dict = {'n_SR_SBI':np.array([n_SR_SBI]), 'n_SR_B':np.array([n_SR_B]), 'n_SR_S':np.array([n_SR_S]), 
                       'n_SR_qqZZ':np.array([n_SR_qqZZ]), 'n_SR_ttV':np.array([n_SR_ttV]),
                       'n_SR_EWB':np.array([n_SR_EWB]), 'n_SR_EWSBI':np.array([n_SR_EWSBI]), 'n_SR_EWSBI10':np.array([n_SR_EWSBI10]),
                       'n_SR_qqZZ_0':np.array([n_SR_qqZZ_0]), 'n_SR_qqZZ_1':np.array([n_SR_qqZZ_1]), 'n_SR_qqZZ_2':np.array([n_SR_qqZZ_2])}
    rdfScalars = ROOT.RDF.FromNumpy(all_yields_dict)
    rdfScalars.Snapshot('yields', outfilePath+outfileName, options=opts)
    
    root_file.Close()
    print("Done. Populating nominal density ratio and weight branches...")

    # Now get the nominal ratios and weight
    root_file = ROOT.TFile(outfilePath+outfileName, 'update')

    r_S = np.array([0], dtype=np.float64)
    r_SBI = np.array([0], dtype=np.float64)
    r_B = np.array([0], dtype=np.float64)
    r_EWB = np.array([0], dtype=np.float64)
    r_EWSBI = np.array([0], dtype=np.float64)
    r_EWSBI10 = np.array([0], dtype=np.float64)
    r_qqZZ_0 = np.array([0], dtype=np.float64)
    r_qqZZ_1 = np.array([0], dtype=np.float64)
    r_qqZZ_2 = np.array([0], dtype=np.float64)
    r_ttV = np.array([0], dtype=np.float64)
    total_weight = np.array([0], dtype=np.float64)

    nominal_tree = ROOT.TTree("nominal", "nominal")
    nominal_tree.Branch("r_S",       r_S,       "r_S/D")
    nominal_tree.Branch("r_SBI",     r_SBI,     "r_SBI/D")
    nominal_tree.Branch("r_B",       r_B,       "r_B/D")
    nominal_tree.Branch("r_EWB",     r_EWB,     "r_EWB/D")
    nominal_tree.Branch("r_EWSBI",   r_EWSBI,   "r_EWSBI/D")
    nominal_tree.Branch("r_EWSBI10", r_EWSBI10, "r_EWSBI10/D")
    nominal_tree.Branch("r_qqZZ_0",  r_qqZZ_0,  "r_qqZZ_0/D")
    nominal_tree.Branch("r_qqZZ_1",  r_qqZZ_1,  "r_qqZZ_1/D")
    nominal_tree.Branch("r_qqZZ_2",  r_qqZZ_2,  "r_qqZZ_2/D")
    nominal_tree.Branch("r_ttV",     r_ttV,     "r_ttV/D")
    nominal_tree.Branch("total_weight", total_weight, "total_weight/D")

    for chunk in pd.read_hdf('test.h5', key='test', chunksize=10000):
        mask_SM = ((chunk['subType'] =='SBI')|(chunk['subType'] =='EWSBI')|(chunk['subType']=='ttV')|(chunk['subType']=='qqZZ'))
        chunk = chunk[mask_SM]
        for index, row in chunk.iterrows():
            r_S[0] = row['r_S']
            r_SBI[0] = row['r_SBI']
            r_B[0]= row['r_B']
            r_EWB[0] = row['r_EWB']
            r_EWSBI[0] = row['r_EWSBI']
            r_EWSBI10[0] = row['r_EWSBI10']
            r_ttV[0] = row['r_ttV']
            total_weight[0] = row['weight_kFact']
            zeroJet_arr = 1 if row['n_jets'] == 0 else 0 
            oneJet_arr = 1 if row['n_jets'] == 1 else 0 
            twoJet_arr = 1 if row['n_jets'] >= 2 else 0 
            r_qqZZ_0[0] = row['r_qqZZ'] * n_SR_qqZZ * zeroJet_arr / n_SR_qqZZ_0
            r_qqZZ_1[0] = row['r_qqZZ'] * n_SR_qqZZ * oneJet_arr / n_SR_qqZZ_1
            r_qqZZ_2[0] = row['r_qqZZ'] * n_SR_qqZZ * twoJet_arr / n_SR_qqZZ_2
            nominal_tree.Fill()

    print("Writing density ratios and weights branches to 'nominal' tree...")
    nominal_tree.Write()
    root_file.Close()

    ### Now Handle the Systematics Ratios and Yields ###
    # Likely Yields First, since r_qqZZ_0/1/2 (and I'm assuming g_qqZZ_0/1/2_SYS_UP/DOWN) depend on yields
    n_sys = 100
    process_names = np.array(['S', 'SBI', 'B', 'EWB', 'EWSBI', 'EWSBI10', 'qqZZ', 'ttV', 'qqZZ_0', 'qqZZ_1', 'qqZZ_2'], dtype=str)
    yields_sys = []
    for i in range(n_sys):
        yields_sys.append(np.array([0], dtype=np.float64))
        yield_string_sys_up = "n_SR_"+sys_names[i]+"_UP"
    



    print("Done.")

    return


if __name__ == '__main__':
    run()
