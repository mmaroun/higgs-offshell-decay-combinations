import numpy as np

''' 
A simple program to regenerate the 5000-event dataset from the larger SM asimov 
dataset used for NLL validation with systematics for the RooDensityRatio. The
user is not encouraged to run this script on its own.
'''

def run():

    textFilePath = '../data/NSBI_index_list.txt'

    index_list = []
    with open(textFilePath, 'r') as f:
        lines = f.readlines()
        lines = lines[1:] # Remove the header line from the text file
        for line in lines:
            index_list.append(int(line))
    index_array = np.asarray(index_list)
    print(index_array)

    npyFilePath = '../data/NSBI_index_array.npy'
    np.save(npyFilePath, index_array)

    return

if __name__ == '__main__':
    run()
